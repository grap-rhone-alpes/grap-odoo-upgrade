_MAX_WAIT_FOR_ODOO = 60
_MAX_TRY_QTY = 5

_INIT_STEP = {
    "name": "Initial Update All in 8.0",
    "technical_name": "step_1__update_all_08_0",
    "odoo_version": "8.0",
    "user": "odoo8",
    "role": "update",
    "local_path": "./src/grap-odoo-env_08.0",
    "git_url": "https://gitlab.com/grap-rhone-alpes/grap-odoo-env.git",
    "git_branch": "8.0",
}

_UPGRADE_STEPS = [
    {
        "name": "OpenUpgrade in 9.0",
        "technical_name": "step_2__openupgrade_09_0",
        "odoo_version": "9.0",
        "user": "odoo9",
        "role": "upgrade",
        "local_path": "./src/grap-odoo-env_09.0-open-upgrade",
        "git_url": "https://gitlab.com/grap-rhone-alpes/grap-odoo-env.git",
        "git_branch": "9.0-open-upgrade",
        "old_user": "odoo8",
    }, {
        "name": "OpenUpgrade in 10.0",
        "technical_name": "step_3__openupgrade_10_0",
        "odoo_version": "10.0",
        "user": "odoo10",
        "role": "upgrade",
        "local_path": "./src/grap-odoo-env_10.0-open-upgrade",
        "git_url": "https://gitlab.com/grap-rhone-alpes/grap-odoo-env.git",
        "git_branch": "10.0-open-upgrade",
        "old_user": "odoo9",
    }, {
        "name": "OpenUpgrade in 11.0",
        "technical_name": "step_4__openupgrade_11_0",
        "odoo_version": "11.0",
        "user": "odoo11",
        "role": "upgrade",
        "local_path": "./src/grap-odoo-env_11.0-open-upgrade",
        "git_url": "https://gitlab.com/grap-rhone-alpes/grap-odoo-env.git",
        "git_branch": "11.0-open-upgrade",
        "old_user": "odoo10",
    }, {
        "name": "OpenUpgrade in 12.0",
        "technical_name": "step_5__openupgrade_12_0",
        "odoo_version": "12.0",
        "user": "odoo12",
        "role": "upgrade",
        "local_path": "./src/grap-odoo-env_12.0",
        "git_url": "https://gitlab.com/grap-rhone-alpes/grap-odoo-env.git",
        "git_branch": "12.0",
        "old_user": "odoo11",
    },
]

_FINAL_STEP = {
    "name": "Final Update All in 12.0",
    "technical_name": "step_6__update_install_12_0",
    "odoo_version": "12.0",
    "user": "odoo12",
    "role": "update",
    "do_not_rebuild": True,
    "local_path": "./src/grap-odoo-env_12.0",
}

_ALL_STEPS = [_INIT_STEP] + _UPGRADE_STEPS + [_FINAL_STEP]
