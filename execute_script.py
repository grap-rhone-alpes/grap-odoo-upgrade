from abstract_script import AbstractScript


class ExecuteScript(AbstractScript):

    def main(self):
        if len(self.steps_to_execute) != 1:
            self.logger_error(
                "Invalid step argument. please provide a unique step.")
            exit()
        step = self.steps_to_execute[0]

        # Execute Post Python Script
        self._execute_python_files_post_migration(step)


if __name__ == "__main__":
    ExecuteScript().run()
