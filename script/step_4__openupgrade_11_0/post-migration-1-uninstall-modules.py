_UNINSTALL_MODULES = [
    # Odoo Core deleted modules
    # https://github.com/OCA/OpenUpgrade/blob/11.0/odoo/
    # openupgrade/doc/source/modules100-110.rst
    "account_accountant",
    "marketing_campaign",
]


def main(self, step):
    self._uninstall_modules(_UNINSTALL_MODULES)
