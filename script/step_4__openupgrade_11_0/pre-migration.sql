-- [stock]
-- change product consu to product stockable to avoid deletion of qwant
-- that take time.

update product_template set type = 'product' where type = 'consu';

-- [stock]
-- set create_uid to the admin user, if create_uid is not defined,
-- to allow the possibility to set a responsible_id to all the template
update product_template set create_uid = 1 where create_uid is null;


-- [sale_stock]
-- remove bad entry in ir_model_fields, that make failing creation of
-- the field sale_line_id in stock_move.
delete from ir_model_fields where model = 'stock.move' and name = 'sale_line_id';

-- TODO, improve population of activity_date_deadline
-- for each model that inherit from mail


-- ----------------------------------------------------------------------------
-- Always begin with the same company for the admin users
-- ----------------------------------------------------------------------------

update res_users set company_id = (
    select id from res_company where code ='DEL'
)
where id = 1;

update res_partner p set company_id = (
    select id from res_company where code ='DEL'
)
from res_users u
where u.id = 1 and u.partner_id = p.id;
