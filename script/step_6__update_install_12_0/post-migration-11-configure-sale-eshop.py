WITH_ESHOPS = ["PZI", "CHE"]


def main(self, step):
    companies = self.odoo_local.ResCompany.browse(
        self.odoo_local.ResCompany.search(
            [("code", "in", WITH_ESHOPS)]))
    companies.write({"has_eshop": True})
    companies = self.odoo_local.ResCompany.browse(
        self.odoo_local.ResCompany.search(
            [("code", "not in", WITH_ESHOPS)]))
    companies.write({"has_eshop": False})
