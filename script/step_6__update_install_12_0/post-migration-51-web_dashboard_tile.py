_DASHBOARD_TILES_MARGIN_PRODUCTS = [{
    "name": "Sans catégorie de marge",
    "background_color": "#990000",
    "domain": """[
        ('sale_ok', '=', True),
        ('cae_administrative_ok', '=', False),
        ('consignor_partner_id', '=', False),
        ('margin_classification_id', '=', False),
    ]"""}, {
    "name": "Marge négative",
    "background_color": "#5b0000",
    "domain": """[
        ('sale_ok', '=', True),
        ('cae_administrative_ok', '=', False),
        ('consignor_partner_id', '=', False),
        ('standard_margin_rate', '<', 0)
    ]"""}, {
    "name": "Marge nulle",
    "background_color": "#990000",
    "domain": """[
        ('sale_ok', '=', True),
        ('cae_administrative_ok', '=', False),
        ('consignor_partner_id', '=', False),
        ('standard_margin_rate', '=', 0)
    ]"""}, {
    "name": "Marge entre 0% et 10%",
    "domain": """[
        ('sale_ok', '=', True),
        ('cae_administrative_ok', '=', False),
        ('consignor_partner_id', '=', False),
        ('standard_margin_rate', '>', 0),
        ('standard_margin_rate', '<=', 10),
    ]"""}, {
    "name": "Marge entre 10% et 20%",
    "domain": """[
        ('sale_ok', '=', True),
        ('cae_administrative_ok', '=', False),
        ('consignor_partner_id', '=', False),
        ('standard_margin_rate', '>', 10),
        ('standard_margin_rate', '<=', 20),
    ]"""}, {
    "name": "Marge entre 20% et 30%",
    "domain": """[
        ('sale_ok', '=', True),
        ('cae_administrative_ok', '=', False),
        ('consignor_partner_id', '=', False),
        ('standard_margin_rate', '>', 20),
        ('standard_margin_rate', '<=', 30),
    ]"""}, {
    "name": "Marge entre 30% et 40%",
    "domain": """[
        ('sale_ok', '=', True),
        ('cae_administrative_ok', '=', False),
        ('consignor_partner_id', '=', False),
        ('standard_margin_rate', '>', 30),
        ('standard_margin_rate', '<=', 40),
    ]"""}, {
    "name": "Marge entre 40% et 50%",
    "domain": """[
        ('sale_ok', '=', True),
        ('cae_administrative_ok', '=', False),
        ('consignor_partner_id', '=', False),
        ('standard_margin_rate', '>', 40),
        ('standard_margin_rate', '<=', 50),
    ]"""}, {
    "name": "Marge entre 50% et 60%",
    "domain": """[
        ('sale_ok', '=', True),
        ('cae_administrative_ok', '=', False),
        ('consignor_partner_id', '=', False),
        ('standard_margin_rate', '>', 50),
        ('standard_margin_rate', '<=', 60),
    ]"""}, {
    "name": "Marge entre 60% et 70%",
    "domain": """[
        ('sale_ok', '=', True),
        ('cae_administrative_ok', '=', False),
        ('consignor_partner_id', '=', False),
        ('standard_margin_rate', '>', 60),
        ('standard_margin_rate', '<=', 70),
    ]"""}, {
    "name": "Marge entre 70% et 80%",
    "domain": """[
        ('sale_ok', '=', True),
        ('cae_administrative_ok', '=', False),
        ('consignor_partner_id', '=', False),
        ('standard_margin_rate', '>', 70),
        ('standard_margin_rate', '<=', 80),
    ]"""}, {
    "name": "Marge entre 80% et 90%",
    "domain": """[
        ('sale_ok', '=', True),
        ('cae_administrative_ok', '=', False),
        ('consignor_partner_id', '=', False),
        ('standard_margin_rate', '>', 80),
        ('standard_margin_rate', '<=', 90),
    ]"""}, {
    "name": "Marge entre 90% et 100%",
    "domain": """[
        ('sale_ok', '=', True),
        ('cae_administrative_ok', '=', False),
        ('consignor_partner_id', '=', False),
        ('standard_margin_rate', '>', 90),
        ('standard_margin_rate', '<=', 100),
    ]"""}
]

_DASHBOARD_TILES_OPEN_INVOICES = [{
    "name": "Facture Impayées (Novembre 2019)",
    "domain": """[
         ('date_invoice', '>=', '2019-11-01'),
         ('date_invoice', '<', '2019-12-01'),
         ('type', '=', 'out_invoice'),
         ('state', '=', 'open'),
         ('pos_pending_payment', '=', False),
         ('is_consignment_invoice', '=', False)
    ]"""}, {
    "name": "Facture Impayées (Décembre 2019)",
    "domain": """[
         ('date_invoice', '>=', '2019-12-01'),
         ('date_invoice', '<', '2020-01-01'),
         ('type', '=', 'out_invoice'),
         ('state', '=', 'open'),
         ('pos_pending_payment', '=', False),
         ('is_consignment_invoice', '=', False)
    ]"""}, {
    "name": "Facture Impayées (Janvier 2020)",
    "domain": """[
         ('date_invoice', '>=', '2020-01-01'),
         ('date_invoice', '<', '2020-02-01'),
         ('type', '=', 'out_invoice'),
         ('state', '=', 'open'),
         ('pos_pending_payment', '=', False),
         ('is_consignment_invoice', '=', False)
    ]"""}, {
    "name": "Facture Impayées (Fév. 2020)",
    "domain": """[
         ('date_invoice', '>=', '2020-02-01'),
         ('date_invoice', '<', '2020-03-01'),
         ('type', '=', 'out_invoice'),
         ('state', '=', 'open'),
         ('pos_pending_payment', '=', False),
         ('is_consignment_invoice', '=', False)
    ]"""}, {
    "name": "Facture Impayées (Mars 2020)",
    "domain": """[
         ('date_invoice', '>=', '2020-03-01'),
         ('date_invoice', '<', '2020-04-01'),
         ('type', '=', 'out_invoice'),
         ('state', '=', 'open'),
         ('pos_pending_payment', '=', False),
         ('is_consignment_invoice', '=', False)
    ]"""}, {
    "name": "Facture Impayées (Avril 2020)",
    "domain": """[
         ('date_invoice', '>=', '2020-01-01'),
         ('date_invoice', '<', '2020-02-01'),
         ('type', '=', 'out_invoice'),
         ('state', '=', 'open'),
         ('pos_pending_payment', '=', False),
         ('is_consignment_invoice', '=', False)
    ]"""}, {
    "name": "Facture Impayées (Mai 2020)",
    "domain": """[
         ('date_invoice', '>=', '2020-05-01'),
         ('date_invoice', '<', '2020-06-01'),
         ('type', '=', 'out_invoice'),
         ('state', '=', 'open'),
         ('pos_pending_payment', '=', False),
         ('is_consignment_invoice', '=', False)
    ]"""}, {
    "name": "Facture Impayées (Juin 2020)",
    "domain": """[
         ('date_invoice', '>=', '2020-06-01'),
         ('date_invoice', '<', '2020-07-01'),
         ('type', '=', 'out_invoice'),
         ('state', '=', 'open'),
         ('pos_pending_payment', '=', False),
         ('is_consignment_invoice', '=', False)
    ]"""}, {
    "name": "Facture Impayées (Juillet 2020)",
    "domain": """[
         ('date_invoice', '>=', '2020-07-01'),
         ('date_invoice', '<', '2020-08-01'),
         ('type', '=', 'out_invoice'),
         ('state', '=', 'open'),
         ('pos_pending_payment', '=', False),
         ('is_consignment_invoice', '=', False)
    ]"""}, {
    "name": "Facture Impayées (Août 2020)",
    "domain": """[
         ('date_invoice', '>=', '2020-08-01'),
         ('date_invoice', '<', '2020-09-01'),
         ('type', '=', 'out_invoice'),
         ('state', '=', 'open'),
         ('pos_pending_payment', '=', False),
         ('is_consignment_invoice', '=', False)
    ]"""}, {
    "name": "Facture Impayées (Septembre 2020)",
    "domain": """[
         ('date_invoice', '>=', '2020-09-01'),
         ('date_invoice', '<', '2020-10-01'),
         ('type', '=', 'out_invoice'),
         ('state', '=', 'open'),
         ('pos_pending_payment', '=', False),
         ('is_consignment_invoice', '=', False)
    ]"""}, {
    "name": "Facture Impayées (Octobre 2020)",
    "domain": """[
         ('date_invoice', '>=', '2020-10-01'),
         ('date_invoice', '<', '2020-11-01'),
         ('type', '=', 'out_invoice'),
         ('state', '=', 'open'),
         ('pos_pending_payment', '=', False),
         ('is_consignment_invoice', '=', False)
    ]"""},
]

_DASHBOARD_TILES_SETTINGS_PRODUCTS = [{
    "name": "Sans TVA",
    "domain": """[
        ('fiscal_classification_id', 'ilike', 'Pas de Taxes'),
    ]"""}, {
    "name": "TVA 2,1%",
    "domain": """[
        ('fiscal_classification_id', 'ilike', '%02,1%'),
    ]"""}, {
    "name": "TVA 5,5%",
    "domain": """[
        ('fiscal_classification_id', 'ilike', '%05,5%'),
    ]"""}, {
    "name": "TVA 10,0%",
    "domain": """[
        ('fiscal_classification_id', 'ilike', '%10,0%'),
    ]"""}, {
    "name": "TVA 20,0%",
    "domain": """[
        ('fiscal_classification_id', 'ilike', '%20,0%'),
    ]"""}, {
    "name": "Produits 'Matières Premières'",
    "domain": """[
        ('categ_id', 'ilike', 'Matières Premières /'),
    ]"""}, {
    "name": "Produits 'Transformés'",
    "domain": """[
        ('categ_id', 'ilike', 'Transformation /'),
    ]"""}, {
    "name": "Produits 'Revente'",
    "domain": """[
        ('categ_id', 'ilike', 'Revente /'),
    ]"""}, {
    "name": "Produits 'Spéciaux'",
    "domain": """[
        ('categ_id', '=', '187'),
    ]"""},
]

CATEGORY_DATAS = [{
    "name": "Paramétrage des produits",
    "default_model_name": "product.product",
    "default_action_ref": "product.product_normal_action",
    "sequence": 1,
    "tiles": _DASHBOARD_TILES_SETTINGS_PRODUCTS,
    }, {
    "name": "Marge des produits",
    "default_model_name": "product.product",
    "default_action_ref": "product.product_normal_action",
    "sequence": 4,
    "tiles": _DASHBOARD_TILES_MARGIN_PRODUCTS,
    }, {
    "name": "Facture impayées",
    "default_model_name": "account.invoice",
    "default_action_ref": "account.action_invoice_tree",
    "default_secondary_function": "sum",
    "default_secondary_field_ref": "account.field_account_invoice_residual",
    "default_secondary_format": "{:.2f} € (TTC)",
    "sequence": 5,
    "tiles": _DASHBOARD_TILES_OPEN_INVOICES,
    }
]


def main(self, step):
    TileCategory = self.odoo_local.TileCategory
    TileTile = self.odoo_local.TileTile
    tiles = TileTile.browse(TileTile.search([]))
    sequence = max([tile.sequence for tile in tiles] + [0])
    for CATEGORY_DATA in CATEGORY_DATAS:
        # Create category, if not exists
        existing_categories = TileCategory.browse(
            [('name', '=', CATEGORY_DATA['name'])])
        if not existing_categories:
            self.logger_info("Create new Tile Category %s" % (
                CATEGORY_DATA["name"]
            ))

            category = TileCategory.create({
                'name': CATEGORY_DATA['name'],
                'sequence': CATEGORY_DATA['sequence'],
            })
        else:
            category = existing_categories[0]
        for TILE_DATA in CATEGORY_DATA["tiles"]:
            if TileTile.search([
                ('name', '=', TILE_DATA['name']),
                ("category_id", "=", category.id),
            ]):
                continue
            sequence += 1
            self.logger_info("Create new Tile %s" % (
                TILE_DATA["name"]
            ))
            model_name =\
                TILE_DATA.get("model_name", False)\
                or CATEGORY_DATA["default_model_name"]
            model_id = self.odoo_local.IrModelData.browse([
                ('model', '=', 'ir.model'),
                ('name', '=', "model_%s" % model_name.replace(".", "_")),
            ])[0].res_id
            action_ref =\
                TILE_DATA.get("action_ref", False)\
                or CATEGORY_DATA["default_action_ref"]
            action_id = self.odoo_local.IrModelData.browse([
                ('module', '=', action_ref.split(".")[0]),
                ('name', '=', action_ref.split(".")[1]),
            ])[0].res_id
            tile_vals = {
                "name": TILE_DATA["name"],
                "sequence": sequence,
                "model_id": model_id,
                "category_id": category.id,
                "action_id": action_id,
                "domain": TILE_DATA["domain"],
                "hide_if_null": "hide_if_null" in TILE_DATA or True,
            }
            if "background_color" in TILE_DATA:
                tile_vals["background_color"] = TILE_DATA["background_color"]

            # Secondary function
            secondary_function = CATEGORY_DATA.get(
                "default_secondary_function", False)
            if secondary_function:
                field_ref = CATEGORY_DATA[
                    "default_secondary_field_ref"]

                field_id = self.odoo_local.IrModelData.browse([
                    ('module', '=', field_ref.split(".")[0]),
                    ('name', '=', field_ref.split(".")[1]),
                ])[0].res_id

                tile_vals.update({
                    "secondary_function": CATEGORY_DATA[
                        "default_secondary_function"],
                    "secondary_format": CATEGORY_DATA[
                        "default_secondary_format"],
                    "secondary_field_id": field_id
                    })
            TileTile.create(tile_vals)
