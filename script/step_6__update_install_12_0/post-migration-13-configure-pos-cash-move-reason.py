def main(self, step):
    companies = self.browse_by_search(
        self.odoo_local.ResCompany, [
            '|', ('active', '=', True), ('active', '=', False),
            ('name', 'not ilike', '%ZZZ%')
        ])
    for company in companies:
        self.logger_info(
            "Handling pos.cash.move.reason for company"
            " %s - %s" % (company.code, company.name))
        self.odoo_user.write({'company_id': company.id})

        sql_res = self._execute_sql_request_by_psycopg2("""
            SELECT id,
                grap_migration_is_income_reason,
                grap_migration_is_expense_reason
            FROM product_template
            WHERE (grap_migration_is_income_reason is true
            OR grap_migration_is_expense_reason is true)
            AND company_id = '%d' and active = true;
        """ % (company.id))

        for (template_id, income, expense) in sql_res:
            template = self.odoo_local.ProductTemplate.browse([template_id])[0]
            reasons = self.odoo_local.PosMoveReason.search([
                ('company_id', '=', company.id),
                ('name', '=', template.name)
                ])
            if reasons:
                self.logger_warning(
                    "%s - Pos move Reason '%s' still created" % (
                        company.code, template.name))
                continue

            # Vieux truc daubé, faire un search sur les account journal ne
            # fonctionne pas on passe donc salement par les pos config,
            # et on récupère la caisse qui a le plus de journaux, et là,
            # on devrait être pas trop mal.
            my_list = self.browse_by_search(
                self.odoo_local.PosConfig,
                [('company_id', '=', company.id)]
            ).journal_ids

            journals = my_list[0]
            for item in my_list:
                if len(item) > len(my_list):
                    journals = item

            if 'banque' in template.name.lower()\
                    or 'achat' in template.name.lower():
                # only for Cash Journal
                journal_ids = [x.id for x in journals if x.code == 'CS']
            else:
                journal_ids = [x.id for x in journals]

            if expense and not template.property_account_expense_id:
                self.logger_warning(
                    "The product %s is marked as expense"
                    " but it doesn't have any expense account defined."
                    " It will not be marked as expense in V12" % (
                        template.name))
                expense = False
            if income and not template.property_account_income_id:
                self.logger_warning(
                    "The product %s is marked as income"
                    " but it doesn't have any income account defined."
                    " It will not be marked as income in V12" % (
                        template.name))
                income = False

            if income or expense:
                self.logger_info(
                    "%s - Creating PoS move Reason '%s' for %d journals." % (
                        company.code, template.name, len(journal_ids)))
                self.odoo_local.PosMoveReason.create({
                    "name": template.name,
                    "company_id": company.id,
                    "active": True,
                    "journal_ids": journal_ids,
                    "is_income_reason": income,
                    "is_expense_reason": expense,
                    "income_account_id": income and template.property_account_income_id.id,
                    "expense_account_id": expense and template.property_account_expense_id.id,
                })
            else:
                self.logger_warning(
                    "The product %s is not defined as income neither expense"
                    " Creation skipped" % (template.name))

            # Unlinking obsolete product
            try:
                self.logger_info("Unlinking %s ..." % (template.name))
                template.unlink()
            except Exception:
                self.logger_info(
                    "Unable to unlink the product %s #%d. It will be disabled" % (
                        template.name, template.id))
                template.active = False

    for company in companies:
        self.odoo_user.write({'company_id': company.id})
        reasons = self.browse_by_search(
            self.odoo_local.PosMoveReason, [
                ('company_id', '=', company.id),
                ('name', 'ilike', '%rreur%'),
            ])
        if not reasons:
            continue

        configs = self.odoo_local.PosConfig.browse(
            self.odoo_local.PosConfig.search([
                '|', ('active', '=', True), ('active', '=', False)]))

        configs.write({"autosolve_pos_move_reason": reasons[0].id})
