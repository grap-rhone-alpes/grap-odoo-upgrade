_REQUIRED_RULES = [
    {
        "name": "Meal Voucher Payment",
        "type": "meal_voucher_payment",
        "encoding": "any",
        "pattern": "...........{NNNDD}........",
    }
]


def main(self, step):
    # Global nomenclatures
    nomenclatures = self.browse_by_search(
        self.odoo_local.BarcodeNomenclature,
        [('company_id', '=', False)],
    )
    if nomenclatures:
        self.logger_info(
            "Unlinking %d global nomenclatures ..." % (
                len(nomenclatures)
            ))
        nomenclatures.unlink()

    companies = self.odoo_local.ResCompany.browse(
        self.odoo_local.ResCompany.search([
            '|', ('active', '=', True), ('active', '=', False)]))
    for company in companies:
        self.odoo_user.write({'company_id': company.id})
        self.logger_info("Updating Barcode Nomenclatures for company %s" % (
            company.name))
        nomenclatures = self.browse_by_search(
            self.odoo_local.BarcodeNomenclature,
            [('company_id', '=', company.id)],
        )
        if not nomenclatures:
            continue

        # Get main nomenclature
        main_nomenclature = nomenclatures[0]
        main_nomenclature.write({
            "name": "%s - Nomenclature de code-barre" % (company.code),
        })
        main_patterns = main_nomenclature.mapped("rule_ids.pattern")
        if type(main_patterns) is not list:
            main_patterns = [main_patterns]

        if len(nomenclatures) > 1:
            other_nomenclatures = nomenclatures[1:]

            # Moving unexisting rules in main nomenclature
            # from other nomenclature
            for nomenclature in other_nomenclatures:
                for rule in nomenclature.rule_ids:
                    if rule.pattern not in main_patterns:
                        main_patterns.append(rule.pattern)
                        self.logger_info(
                            "Moving pattern '%s' of %s" % (
                                rule.pattern, nomenclature.name
                            ))
                        rule.write({
                            "barcode_nomenclature_id": main_nomenclature.id,
                        })
                self.logger_info("Unlinking obsolete nomenclature %s" % (
                    nomenclature.name))
                nomenclature.unlink()

        # Create Meal Voucher Pattern
        for required_rule in _REQUIRED_RULES:
            if required_rule["pattern"] not in main_patterns:
                vals = required_rule.copy()
                vals["barcode_nomenclature_id"] = main_nomenclature.id
                self.logger_info("Creating new rule %s (%s)" % (
                    required_rule["name"], required_rule["pattern"]))
                self.odoo_local.BarcodeRule.create(vals)

        # Set the nomenclature for all the PosConfig
        configs = self.odoo_local.PosConfig.browse(
            self.odoo_local.PosConfig.search([
                '|', ('active', '=', True), ('active', '=', False)]))
        self.logger_info(
            "Set the nomenclature %s for all the %d"
            " PoS Configs" % (main_nomenclature.name, len(configs)))
        configs.write({"barcode_nomenclature_id": main_nomenclature.id})
