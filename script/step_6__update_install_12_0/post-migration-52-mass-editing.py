def main(self, step):
    mass_editings = self.browse_by_search(self.odoo_local.MassEditing, [])
    for mass_editing in mass_editings:
        self.logger_info("Trying to fix mass editing %s" % (
            mass_editing.name
        ))
        new_name = "Edition en lot (%s)" % (
            mass_editing.name)
        mass_editing.action_name = new_name
        mass_editing.disable_mass_operation()
        mass_editing.enable_mass_operation()
