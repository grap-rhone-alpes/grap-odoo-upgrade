def main(self, step):
    self.logger_warning("Reloading french translation ...")
    wizard = self.odoo_local.BaseLanguageInstall.create({
        'lang': 'fr_FR',
        'overwrite': True,
    })
    wizard.lang_install()
