IT_emails = [
    "informatique@grap.coop",
    "sylvain.legal@grap.coop",
    "quentin.dupont@grap.coop",
    "sandie.favre@grap.coop",
]
IT_xml_groups = [
    # Base odoo Groups
    "stock.group_stock_multi_locations",
    "base.group_erp_manager",
    "purchase.group_purchase_manager",
    "base.group_hr_user",  # 'Fonctionnaire' ;-)
    "sales_team.group_sale_manager",
    "point_of_sale.group_pos_manager",

    # Extra Odoo Groups
    "analytic.group_analytic_tags",
    "stock.group_adv_location",
    "product.group_pricelist_item",

    # OCA Groups
    "queue_job.group_queue_job_manager",
    "base_technical_features.group_technical_features",

    # Custom Groups
    "product_label.group_label_manager",
    "product_food.group_certifier_manager",
    "product_food.group_allergen_manager",
    "fiscal_company_base.fiscal_company_manager",
    "product_to_scale_bizerba.group_manager",
]


accounting_emails = IT_emails + [
    "camille.estevez@grap.coop",
    "lucie.definod@grap.coop",
    "nina.collin@grap.coop",
    "sibylle.guillondeprince@grap.coop",
    "laure.hadorn@grap.coop",
]

accounting_xml_groups = [
    "intercompany_trade_base.intercompany_trade_manager",
    "account.group_account_manager",
    "analytic.group_analytic_accounting",
    "account_move_change_number.group_account_move_change_number",
]

pos_place_emails = IT_emails + [
    "contact@elodie-d.fr",
    "melimelo.loire@gmail.com",
    "compta@lachevreetlechou.fr",
    "fournil@lacledesole.fr",
    "contact@pan-chocolaterie.fr",
    "contact@fournildescometes.fr",
]

pos_place_xml_groups = [
    "pos_place.group_pos_place_manager",
]


all_xml_groups = [
    "sale_merge_draft_invoice.group_sale_merge_draft_invoice",
    "product.group_sale_pricelist",
    "base.group_partner_manager",
]

technical_shop_logins = [
    "che-eboutique",
    "pzi-eboutique",
]

technical_shop_xml_groups = [
    "sale_eshop.res_groups_is_eshop",
    "sale_eshop.res_groups_eshop_user",
]


scale_emails = IT_emails + [
    "cran@lelocal-epicerie.fr",             # LO1
    "meythet@lelocal-epicerie.fr",          # LO2
    "equipedechoc@lelocal-epicerie.fr",     # LOC
    "contact@lepiceriedeshalles.coop",      # HAL
]


scale_xml_groups = [
    "barcodes_generator_abstract.generate_barcode",
    "product_to_scale_bizerba.group_user",
]


eboutique_emails = IT_emails + [
    "compta@lachevreetlechou.fr",
    "lclcpanier@gmail.com",
    "contact@petitszestes.fr",
]

eboutique_xml_groups = [
    "sale_eshop.res_groups_eshop_manager",
    "stock_preparation_category.preparation_manager",
    "sale_recovery_moment.recovery_manager",
]


def main(self, step):
    # Change email of the new admin user.
    informatique_user = self._get_ref("base.user_admin")
    informatique_user.write({
        "email": "informatique@grap.coop",
        })
    self._restrict_group_to_users(
        IT_xml_groups, emails=IT_emails)

    self._restrict_group_to_users(
        accounting_xml_groups, emails=accounting_emails)

    self._restrict_group_to_users(
        technical_shop_xml_groups, logins=technical_shop_logins)

    self._restrict_group_to_users(
        eboutique_xml_groups, emails=eboutique_emails)

    self._restrict_group_to_users(
        pos_place_xml_groups, logins=pos_place_emails)

    self._restrict_group_to_users(
        scale_xml_groups, emails=scale_emails)

    self._restrict_group_to_users(all_xml_groups)
