_UNINSTALL_MODULES = [
    # Remove this module as it is an OCA/account-invoicing obsolete glue module
    "account_invoice_pricelist_stock_account",

    # Remove unused module crm
    "crm",

    # removed autoinstalled module, during openupgrade process
    "partner_autocomplete",

    # Remove this module, once all new modules are installed
    "sale_food",

    # Remove this module, once purchase_triple_discount is updated
    "product_supplierinfo_triple_discount",

    # Remove this module,
    # once account_product_fiscal_classification is updated
    "account_product_fiscal_classification_usage_group",

    # Remove this module, once account_invoice_margin is installed
    "invoice_margin",

    # Remove line_sequence modules that seem fucked in V12
    # keeping only stock_picking_line_sequence
    "purchase_order_line_sequence",
    "account_invoice_line_sequence",

    # Remove this module, once pos_place is installed
    "pos_street_market",

    # Remove this module, once grap_account_export_ebp is installed
    "account_export_ebp",

    # Remove this fucking module that was not possible
    # to uninstall in V8
    "module_uninstall_check",

    # Remove all the obsolete fiscal companies modules
    "base_fiscal_company",
    "account_fiscal_company",
    "product_fiscal_company",
    "sales_team_fiscal_company",
    "account_voucher_fiscal_company",
    "barcodes_fiscal_company",
    "calendar_fiscal_company",
    "crm_fiscal_company",
    "l10n_fr_fiscal_company",
    "pos_fiscal_company",
    "pos_pricelist_fiscal_company",
    "purchase_fiscal_company",
    "stock_account_fiscal_company",
    "stock_fiscal_company",

    # Remove all the obsolete intercompany-trade modules
    "intercompany_trade_purchase_account_invoice_pricelist",
]


def main(self, step):
    self._uninstall_modules(_UNINSTALL_MODULES)
