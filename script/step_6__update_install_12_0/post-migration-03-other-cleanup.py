def main(self, step):
    # 1) Purge two 'ir.actions.act_windows'
    model_list = self.odoo_local.IrModel.browse(
        [('id', '!=', 0)]).mapped('model')
    actWindows = self.odoo_local.irActionsAct_window.browse(
        [('res_model', 'not in', model_list)]
    )
    if actWindows:
        self.logger_info(
            "Unlinking %d obsolete ir.actions.act_windows" % len(actWindows))
        actWindows.unlink()

    # 2) reload traduction
    wizard = self.odoo_local.BaseLanguageInstall.create({
        "lang": "fr_FR",
        "overwrite": True,
    })
    wizard.lang_install()
