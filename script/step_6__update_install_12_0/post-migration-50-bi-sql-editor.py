bi_pos_activity_day_hour = {
    "name": "PdV - Activité par HEURE et JOUR de la semaine",
    "technical_name": "bi_pos_activity_day_hour",
    "view_name": "x_bi_sql_view_bi_pos_activity_day_hour",
    "is_materialized": True,
    "domain_force": "[('x_company_id', 'child_of', [user.company_id.id])]",
    "action_context": {
        "pivot_measures": [
            "x_amount_total_vat_incl",
            "x_order_qty",
            "__computed_average_amount_vat_incl"
        ],
        "pivot_computed_measures": [{
            "field1": "x_amount_total_vat_incl",
            "field2": "x_order_qty",
            "operation": "x_amount_total_vat_incl/x_order_qty",
            "name": "Panier Moyen TTC",
            "id": "__computed_average_amount_vat_incl",
            "format": "float"
        }]
    },
    "fields": {
        "x_company_id": ["Societe", {"is_index": True}],
        "x_user_id": ["Utilisateur", {}],
        "x_day_order": ["JOUR de la vente", {"graph_type": "col"}],
        "x_hour_order": ["HEURE de la vente", {"graph_type": "row"}],
        "x_date_order": ["Date de la vente", {}],
        "x_amount_total_vat_excl": ["Total HT", {}],
        "x_amount_total_vat_incl": ["Total TTC", {"graph_type": "measure"}],
        "x_order_qty": ["# Vente", {"graph_type": "measure"}],
    },
    "query": """
SELECT
    sub_req.company_id as x_company_id,
    sub_req.user_id as x_user_id,
    sub_req.date_order as x_date_order,
    sub_req.amount_total_vat_excl as x_amount_total_vat_excl,
    sub_req.amount_total as x_amount_total_vat_incl,
    sub_req.order_qty as x_order_qty,
    CASE EXTRACT(ISODOW FROM sub_req.french_date_order)
        WHEN 1 THEN '1 - Lundi'
        WHEN 2 THEN '2 - Mardi'
        WHEN 3 THEN '3 - Mercredi'
        WHEN 4 THEN '4 - Jeudi'
        WHEN 5 THEN '5 - Vendredi'
        WHEN 6 THEN '6 - Samedi'
        WHEN 7 THEN '7 - Dimanche'
    END as x_day_order,
            to_char(french_date_order, 'HH24') as x_hour_order
FROM (
    SELECT
        po.company_id ,
        po.user_id,
        date_trunc('hour', po.date_order) as date_order,
        date_trunc(
            'hour',
            (to_char(
                date_order::timestamp with time zone, 'YYYY-MM-DD HH24:MI:SS'
            )::timestamp AT TIME ZONE 'UTC')
            AT TIME ZONE 'Europe/Paris'
        ) as french_date_order,
        sum(po.amount_total - po.amount_tax) as amount_total_vat_excl,
        sum(po.amount_total) as amount_total,
        count(po.id) as order_qty
    FROM pos_order po
    GROUP BY
        po.company_id,
        po.user_id,
        date_trunc('hour', po.date_order),
        date_trunc(
            'hour',
            (to_char(
                date_order::timestamp with time zone, 'YYYY-MM-DD HH24:MI:SS'
            )::timestamp AT TIME ZONE 'UTC')
            AT TIME ZONE 'Europe/Paris'
        )
) as sub_req
    """,
}

bi_pos_payment_type = {
    "name": " PdV - Moyen de paiement",
    "technical_name": "bi_pos_payment_type",
    "view_name": "x_bi_sql_view_bi_pos_payment_type",
    "is_materialized": True,
    "domain_force": "[('x_company_id', 'child_of', [user.company_id.id])]",
    "fields": {
        "x_company_id": ["Societe", {"is_index": True}],
        "x_journal_id": ["Methode de paiement", {
            "many2one_model_id": "account.journal",
            "graph_type": "col",
        }],
        "x_date": ["Date du paiement", {"graph_type": "row"}],
        "x_total": ["Montant", {"graph_type": "measure"}],
        "x_quantity": ["# de paiements", {"graph_type": "measure"}],
    },
    "action_context": {
        "pivot_measures": [
            "x_total",
            "x_quantity",
            "__computed_average_amount",
        ],
        "pivot_computed_measures": [{
            "field1": "x_total",
            "field2": "x_quantity",
            "operation": "x_total/x_quantity",
            "name": "Montant Moyen",
            "id": "__computed_average_amount",
            "format": "float"
        }]
    },
    "query": """
SELECT
    absl.company_id as x_company_id,
    absl.journal_id as x_journal_id,
    DATE_TRUNC('day', absl.date::timestamp) AS x_date,
    SUM(absl.amount) as x_total,
    count(distinct(absl.pos_statement_id)) as x_quantity
FROM account_bank_statement_line AS absl
INNER JOIN account_journal AS aj
    ON aj.id = absl.journal_id
WHERE
    absl.pos_statement_id IS NOT NULL
    AND aj.type in ('cash', 'bank')
    AND absl.amount < 1000000
    AND absl.amount > -1000000
GROUP BY
    absl.company_id,
    absl.journal_id,
    DATE_TRUNC('day', absl.date::timestamp)
"""
}

bi_pos_activity_per_date = {
    "name": "PdV - Activité par date",
    "technical_name": "bi_pos_activity_per_date",
    "view_name": "x_bi_sql_view_bi_pos_activity_per_date",
    "is_materialized": True,
    "domain_force": "[('x_company_id', 'child_of', [user.company_id.id])]",
    "fields": {
        "x_company_id": ["Societe", {"is_index": True}],
        "x_date_order": ["Date de vente", {"graph_type": "row"}],
        "x_order_qty": ["# de vente", {"graph_type": "measure"}],
        "x_line_qty": ["# de ligne de vente", {"graph_type": "measure"}],
        "x_amount_total_vat_excl": ["Total HT", {"graph_type": "measure"}],
        "x_amount_total_vat_incl": ["Total TTC", {"graph_type": "measure"}],
    },
    "action_context": {
        "pivot_measures": [
            "x_order_qty",
            "x_line_qty",
            "x_amount_total_vat_excl",
            "x_amount_total_vat_incl",
            "__computed_average_vat_incl",
        ],
        "pivot_computed_measures": [{
            "field1": "x_amount_total_vat_incl",
            "field2": "x_order_qty",
            "operation": "x_amount_total_vat_incl/x_order_qty",
            "name": "Panier Moyen TTC",
            "id": "__computed_average_vat_incl",
            "format": "float"
        }]
    },
    "query": """
SELECT
      po.company_id as x_company_id,
      date_trunc('day',po.date_order) as x_date_order,
      count(po.id) as x_order_qty,
      sum(table_line_qty.line_qty) as x_line_qty,
      sum(po.amount_total - po.amount_tax) as x_amount_total_vat_excl,
      sum(po.amount_total) as x_amount_total_vat_incl
from pos_order po
INNER JOIN (
    SELECT
        po.id,
        count(*) as line_qty
    FROM pos_order po
    INNER JOIN pos_order_line pol
        ON pol.order_id = po.id
    GROUP BY po.id
    ) table_line_qty
    ON table_line_qty.id = po.id
GROUP BY
    po.company_id,
    date_trunc('day',po.date_order)

    """
}

bi_pos_cashier_error = {
    "name": "PdV - Erreur de caisse",
    "technical_name": "bi_pos_cashier_error",
    "view_name": "x_bi_sql_view_bi_pos_cashier_error",
    "is_materialized": True,
    "domain_force": "[('x_company_id', 'child_of', [user.company_id.id])]",
    "action_context": {},
    "fields": {
        "x_company_id": ["Societe", {"is_index": True}],
        "x_type": ["Type d'erreur", {"graph_type": "col"}],
        "x_date": ["Date de l'erreur", {"graph_type": "row"}],
        "x_amount": ["Montant", {"graph_type": "measure"}],
    },
    "query": """
SELECT
absl.company_id as x_company_id,
absl.amount as x_amount,
CASE
    WHEN aa.code ilike '6582%' THEN 'loss'
    WHEN aa.code ilike '7582%' THEN 'gain'
    ELSE 'unknown'
END as x_type,
date_trunc(
    'day', absl.date::timestamp with time zone AT TIME ZONE 'Europe/Paris'
    ) as x_date
FROM account_bank_statement_line absl
INNER JOIN account_account aa on absl.account_id = aa.id
WHERE aa.code ilike '7582%' or aa.code ilike '6582%'
"""
}

technical_account_income_product_category = {
    "name": "[TECH] #1-A - account_product_category",
    "technical_name": "technical_account_income_product_category",
    "view_name": "x_bi_sql_view_technical_account_income_product_category",
    "state": "model_valid",
    "is_materialized": True,
    "domain_force": "[('x_company_id', 'child_of', [user.company_id.id])]",
    "fields": {
        "x_company_id": ["Societe", {"is_index": True}],
        "x_product_categ_id": ["Categorie d'article", {"is_index": True}],
        "x_account_id": ["Compte comptable", {
            "many2one_model_id": "account.account"
        }],
        "x_account_code": ["Code comptable"],
    },
    "query": """
    SELECT
    view.company_id as x_company_id,
    view.product_categ_id as x_product_categ_id,
    aa.id as x_account_id,
    aa.code as x_account_code
FROM (
    SELECT
        company_id,
        cast (substring(res_id from 18) as int4) as product_categ_id,
        cast (substring(value_reference from 17) as int4) as account_id
    FROM ir_property
    WHERE res_id ilike 'product.category%'
    AND name = 'property_account_income_categ_id'
) as view
INNER JOIN account_account aa
on aa.id = view.account_id
    """
}

technical_account_income_product_template = {
    "name": "[TECH] #1-B - account_product_template",
    "technical_name": "technical_account_income_product_template",
    "view_name": "x_bi_sql_view_technical_account_income_product_template",
    "state": "model_valid",
    "is_materialized": True,
    "domain_force": "[('x_company_id', 'child_of', [user.company_id.id])]",
    "fields": {
        "x_company_id": ["Societe", {"is_index": True}],
        "x_product_tmpl_id": ["Modele d'article", {"is_index": True}],
        "x_account_id": ["Compte comptable", {
            "many2one_model_id": "account.account"
        }],
        "x_account_code": ["Code comptable"],
    },
    "query": """
SELECT
    view.company_id as x_company_id,
    view.product_tmpl_id as x_product_tmpl_id,
    aa.id as x_account_id,
    aa.code as x_account_code
FROM (
    SELECT
        company_id,
        cast (substring(res_id from 18) as int4) as product_tmpl_id,
        cast (substring(value_reference from 17) as int4) as account_id
    FROM ir_property
    WHERE res_id ilike 'product.template%'
    AND name = 'property_account_income_id'
) as view
INNER JOIN account_account aa
on aa.id = view.account_id
"""
}

technical_product_product_list = {
    "name": "[TECH] #2 - product_product_list",
    "technical_name": "technical_product_product_list",
    "view_name": "x_bi_sql_view_technical_product_product_list",
    "state": "model_valid",
    "is_materialized": True,
    "domain_force":
    "[('x_product_company_id', 'child_of', [user.company_id.id])]",
    "action_context": {},
    "fields": {
        "x_product_company_id": ["Societe", {
            "many2one_model_id": "res.company",
            "is_index": True}
        ],
        "x_product_id": ["Article", {"many2one_model_id": "product.product"}],
        "x_is_alimentary": ["Est un article Alimentaire"],
        "x_country_id": ["Pays"],
        "x_state_id": ["Region"],
        "x_department_id": ["Departement"],
        "x_maker_description": ["Marque"],
        "x_template_id": ["Modele d'article", {
            "many2one_model_id": "product.template"}
        ],
        "x_consignor_partner_id": ["Depot Vendeur"],
        "x_product_active": ["Article Actif"],
        "x_fiscal_classification_id": ["Categorie Fiscale"],
        "x_template_categ_id": ["Categorie d'article", {
            "many2one_model_id": "product.category"}
        ],
        "x_intermediate_categ_id": ["Categorie d'article intermediaire", {
            "many2one_model_id": "product.category"}
        ],
        "x_root_categ_id": ["Categorie d'article racine", {
            "many2one_model_id": "product.category"}
        ],
        "x_uom_category_id": ["Categorie d'UdM de l'article", {
            "many2one_model_id": "uom.category"}
        ],
        "x_main_partner_id": ["Fournisseur principal ACTUEL", {
            "many2one_model_id": "res.partner"}
        ],
        "x_category_account_id": ["Compte Comptable de la categorie", {
            "many2one_model_id": "account.account"}
        ],
        "x_category_account_code": ["Code Comptable de la categorie"],
        "x_template_account_id": ["Compte Comptable de l'article", {
            "many2one_model_id": "account.account"}
        ],
        "x_template_account_code": ["Code Comptable de l'article"],
        "x_account_type": ["Type comptable", {
            "ttype": "selection",
            "selection": """[
                ('consignment', 'Depot Vente'),
                ('consignment_commission', 'Commission sur depot vente'),
                ('sale_from_purchase', 'Vente de marchandises'),
                ('sale_from_make', 'Vente de produits finis'),
                ('sale_service', 'Vente de service'),
                ('deposit', 'Consigne'),
                ('voucher', "Bon d'achat"),
                ('discount_sale', 'Remise sur vente'),
                ('other', 'AUTRES'),
            ]"""}
        ]
    },
    "query": """
SELECT
    pt.company_id as x_product_company_id,
    pp.id as x_product_id,
    pp.is_alimentary as x_is_alimentary,
    pp.country_id as x_country_id,
    pp.state_id as x_state_id,
    pp.state_id as x_department_id,
    pp.maker_description as x_maker_description,
    pt.id as x_template_id,
    pt.consignor_partner_id as x_consignor_partner_id,
    pt.active as x_product_active,
    pt.fiscal_classification_id as x_fiscal_classification_id,
    pt.categ_id as x_template_categ_id,
    pc_2.id as x_intermediate_categ_id,
    pc_2.parent_id as x_root_categ_id,
    pu.category_id as x_uom_category_id,
    (select name from product_supplierinfo psi where psi.product_tmpl_id = pt.id limit 1) as x_main_partner_id,
    apc.x_account_id as x_category_account_id,
    apc.x_account_code as x_category_account_code,
    apt.x_account_id as x_template_account_id,
    apt.x_account_code as x_template_account_code,
    CASE
        WHEN pt.is_consignment                           THEN 'consignment'
        WHEN pt.is_consignment_commission      THEN 'consignment_commission'
        WHEN apc.x_account_code like '707%'            THEN 'sale_from_purchase'
        WHEN apc.x_account_code like '701%'            THEN 'sale_from_make'
        WHEN apc.x_account_code like '706%'         THEN 'sale_service'
        WHEN apt.x_account_code like '706%'          THEN 'sale_service'
        WHEN apc.x_account_code like '7082%'         THEN 'sale_service'
        WHEN apt.x_account_code like '7082%'          THEN 'sale_service'
        WHEN apc.x_account_code = '4096'          THEN 'deposit'
        WHEN apt.x_account_code = '4096'           THEN 'deposit'
        WHEN apc.x_account_code like '467%'      THEN 'voucher'
        WHEN apt.x_account_code like '467%'       THEN 'voucher'
        WHEN apc.x_account_code = '7091'      THEN 'discount_sale'
        WHEN apt.x_account_code = '7091'       THEN 'discount_sale'
        WHEN apc.x_account_code = '7097'      THEN 'discount_sale'
        WHEN apt.x_account_code = '7097'       THEN 'discount_sale'
        ELSE 'other'
    END as x_account_type

FROM product_product pp
INNER JOIN product_template pt
       ON  pt.id = pp.product_tmpl_id
INNER JOIN product_category pc_1 -- We assume that category has three level
    ON pc_1.id = pt.categ_id
INNER JOIN product_category pc_2 -- We assume that category has three level
    ON pc_2.id = pc_1.parent_id
INNER JOIN uom_uom pu
    ON  pu.id = pt.uom_id
LEFT JOIN x_bi_sql_view_technical_account_income_product_category apc
    ON pt.categ_id = apc.x_product_categ_id
    AND pt.company_id = apc.x_company_id
LEFT JOIN x_bi_sql_view_technical_account_income_product_template apt
    ON pt.id = apt.x_product_tmpl_id
    AND pt.company_id = apt.x_company_id
    """,
}

technical_sale_pos_order_line = {
    "name": "[TECH] #A-1 - sale_pos_order_line",
    "technical_name": "technical_sale_pos_order_line",
    "view_name": "x_bi_sql_view_technical_sale_pos_order_line",
    "state": "model_valid",
    "is_materialized": True,
    "domain_force": "[('x_company_id', 'child_of', [user.company_id.id])]",
    "fields": {
        "x_company_id": ["Societe", {"is_index": True}],
        "x_customer_id": ["Client", {
            "many2one_model_id": "res.partner"}
        ],
        "x_sale_type": ["Type de vente", {
            "ttype": "selection",
            "selection": """[
                ('point_of_sale_eshop', 'Vente Via eBoutique'),
                ('point_of_sale_regular', 'Vente Classique'),
            ]"""},
        ],
        "x_sale_date": ["Date de vente"],
        "x_product_id": ["Article", {
            "many2one_model_id": "product.product"}
        ],
        "x_user_id": ["Utilisateur"],
        "x_pricelist_id": ["Liste de prix"],
        "x_place_id": ["Lieu de vente", {
            "many2one_model_id": "pos.place"}
        ],
        "x_product_uom_qty": ["Quantite"],
        "x_price_total_vat_excl": ["Total HT"],
        "x_margin": ["Marge HT"],
    },
    "query": """
SELECT
    x_company_id,
    x_customer_id,
    x_sale_type,
    x_sale_date,
    x_product_id,
    x_user_id,
    x_pricelist_id,
    x_place_id,
    COALESCE(sum(x_product_uom_qty), 0) as x_product_uom_qty,
    COALESCE(sum(x_price_total_vat_excl), 0) as x_price_total_vat_excl,
    COALESCE(sum(x_margin), 0) as x_margin
FROM (
    SELECT
        po.company_id AS x_company_id,
        po.partner_id as x_customer_id,
        CASE
        WHEN po.id = pos_picking.final_pos_order_id
            THEN 'point_of_sale_eshop'
            ELSE 'point_of_sale_regular'
        END ::varchar AS x_sale_type,
        date_trunc('day', po.date_order::timestamp with time zone AT TIME ZONE 'Europe/Paris') as x_sale_date,
        pol.product_id as x_product_id,
        po.user_id as x_user_id,
        po.pricelist_id as x_pricelist_id,
        po.place_id as x_place_id,
        pol.qty as x_product_uom_qty,
        pol.price_subtotal as x_price_total_vat_excl,
        pol.margin as x_margin
    FROM pos_order_line pol
    INNER JOIN pos_order po
        ON pol.order_id = po.id
    LEFT OUTER JOIN (
            SELECT distinct(final_pos_order_id) as final_pos_order_id
            FROM stock_picking
            WHERE final_pos_order_id is not null
        ) as pos_picking
        ON po.id = pos_picking.final_pos_order_id
    WHERE
        po.state not IN ('draft')
        AND pol.qty != 0
) as tmp
GROUP BY
    x_company_id,
    x_customer_id,
    x_sale_type,
    x_sale_date,
    x_product_id,
    x_user_id,
    x_pricelist_id,
    x_place_id;

    """,

}

technical_sale_account_invoice_line = {
    "name": "[TECH] #A-2 - sale_account_invoice_line",
    "technical_name": "technical_sale_account_invoice_line",
    "view_name": "x_bi_sql_view_technical_sale_account_invoice_line",
    "state": "model_valid",
    "is_materialized": True,
    "domain_force": "[('x_company_id', 'child_of', [user.company_id.id])]",
    "fields": {
        "x_company_id": ["Societe", {"is_index": True}],
        "x_customer_id": ["Client", {
            "many2one_model_id": "res.partner"}
        ],
        "x_sale_type": ["Type de vente", {
            "ttype": "selection",
            "selection": """[
                ('invoice', 'Facture'),
            ]"""},
        ],
        "x_sale_date": ["Date de vente"],
        "x_product_id": ["Article", {
            "many2one_model_id": "product.product"}
        ],
        "x_user_id": ["Utilisateur"],
        "x_pricelist_id": ["Liste de prix"],
        "x_place_id": ["Lieu de vente", {
            "many2one_model_id": "pos.place"}
        ],
        "x_product_uom_qty": ["Quantite"],
        "x_price_total_vat_excl": ["Total HT"],
        "x_margin": ["Marge HT"],
    },
    "query": """
SELECT
    x_company_id,
    x_customer_id,
    x_sale_type,
    x_sale_date,
    x_product_id,
    x_user_id,
    x_pricelist_id,
    cast (false as int4) x_place_id,
    COALESCE(sum(x_product_uom_qty), 0) as x_product_uom_qty,
    COALESCE(sum(x_price_total_vat_excl), 0) as x_price_total_vat_excl,
    COALESCE(sum(x_margin), 0) as x_margin
FROM (
    SELECT
        ai.company_id AS x_company_id,
        ai.partner_id AS x_customer_id,
        'invoice'::varchar AS x_sale_type,
        date_trunc('day', ai.date_invoice::timestamp with time zone AT TIME ZONE 'Europe/Paris') as x_sale_date,
        ail.product_id as x_product_id,
        ai.user_id as x_user_id,
        ai.pricelist_id as x_pricelist_id,
        CASE WHEN ai.type = 'out_invoice' THEN
            ail.quantity / uom_ail.factor * uom_pt.factor
        ELSE
            - (ail.quantity / uom_ail.factor * uom_pt.factor)
        END::decimal(16, 3) as x_product_uom_qty,
        ail.price_subtotal_signed AS x_price_total_vat_excl,
        ail.margin_signed AS x_margin
    FROM account_invoice_line ail
    INNER JOIN account_invoice ai
        ON ail.invoice_id = ai.id
    INNER JOIN product_product pp
        ON ail.product_id = pp.id
    INNER JOIN product_template pt
        ON pp.product_tmpl_id = pt.id
    INNER JOIN uom_uom uom_ail
        ON uom_ail.id = ail.uom_id
    INNER JOIN uom_uom uom_pt
        ON uom_pt.id = pt.uom_id
    WHERE
        ai.state NOT IN ('draft', 'cancel')
        AND ai.type IN ('out_invoice', 'out_refund')
        AND ail.quantity != 0
        AND ai.id NOT IN (
            SELECT invoice_id
            FROM pos_order
            WHERE invoice_id IS NOT NULL)
) as tmp
GROUP BY
    x_company_id,
    x_customer_id,
    x_sale_type,
    x_sale_date,
    x_product_id,
    x_user_id,
    x_pricelist_id
    """,
}


technical_sale_pos_invoice_line = {
    "name": "[TECH] #B - sale_pos_order_line + sale_account_invoice_line",
    "technical_name": "technical_sale_pos_invoice_line",
    "view_name": "x_bi_sql_view_technical_sale_pos_invoice_line",
    "state": "model_valid",
    "is_materialized": True,
    "domain_force": "[('x_company_id', 'child_of', [user.company_id.id])]",
    "action_context": {},
    "fields": {
        "x_company_id": ["Societe", {"is_index": True}],
        "x_customer_id": ["Client", {
            "many2one_model_id": "res.partner"}
        ],
        "x_sale_type": ["Type de vente", {
            "ttype": "selection",
            "selection": """[
                ('point_of_sale_eshop', 'Vente Via eBoutique'),
                ('point_of_sale_regular', 'Vente Classique'),
                ('invoice', 'Facture'),
            ]"""},
        ],
        "x_sale_date": ["Date de vente"],
        "x_product_id": ["Article", {
            "many2one_model_id": "product.product"}
        ],
        "x_user_id": ["Utilisateur"],
        "x_pricelist_id": ["Liste de prix"],
        "x_place_id": ["Lieu de vente", {
            "many2one_model_id": "pos.place"}
        ],
        "x_product_uom_qty": ["Quantite"],
        "x_price_total_vat_excl": ["Total HT"],
        "x_margin": ["Marge HT"],
    },
    "query":
    """
SELECT
    x_company_id,
    x_customer_id,
    x_product_id,
    x_user_id,
    x_pricelist_id,
    x_place_id,
    x_sale_type,
    x_sale_date,
    COALESCE(sum(x_product_uom_qty), 0) AS x_product_uom_qty,
    COALESCE(sum(x_price_total_vat_excl), 0) AS x_price_total_vat_excl,
    COALESCE(sum(x_margin), 0) AS x_margin
FROM   ( select * from x_bi_sql_view_technical_sale_account_invoice_line
UNION ALL  select * from x_bi_sql_view_technical_sale_pos_order_line) as temp
GROUP BY
    x_company_id,
    x_customer_id,
    x_sale_type,
    x_sale_date,
    x_product_id,
    x_user_id,
    x_pricelist_id,
    x_place_id
    """,
}


bi_sale_product_main = {
    "name": "Activité - Vente de produits",
    "technical_name": "bi_sale_product_main",
    "view_name": "x_bi_sql_view_bi_sale_product_main",
    "is_materialized": True,
    "domain_force": "[('x_company_id', 'child_of', [user.company_id.id])]",
    "action_context": {
        "pivot_measures": [
            "x_price_total_vat_excl",
            "x_margin",
            "__computed_margin_rate"
        ],
        "pivot_computed_measures": [{
            "field1": "x_margin",
            "field2": "x_price_total_vat_excl",
            "operation": "x_margin/x_price_total_vat_excl",
            "name": "Taux de marque",
            "id": "__computed_margin_rate",
            "format": "percentage"
        }]
    },
    "fields": {
        "x_company_id": ["Societe", {"is_index": True}],
        "x_company_category_id": ["Categorie de societe", {
            "many2one_model_id": "res.company.category"}
        ],
        "x_customer_id": ["Client", {
            "many2one_model_id": "res.partner"}
        ],
        "x_sale_type": ["Type de vente", {
            "ttype": "selection",
            "selection": """[
                ('point_of_sale_eshop', 'Vente Via eBoutique'),
                ('point_of_sale_regular', 'Vente Classique'),
                ('invoice', 'Facture'),
            ]"""},
        ],
        "x_sale_date": ["Date de vente", {
            "is_index": True,
            "graph_type": "row",
        }],
        "x_product_id": ["Article", {
            "many2one_model_id": "product.product"}
        ],
        "x_user_id": ["Utilisateur"],
        "x_pricelist_id": ["Liste de prix"],
        "x_place_id": ["Lieu de vente", {
            "many2one_model_id": "pos.place"}
        ],
        "x_product_uom_qty": ["Quantite"],
        "x_price_total_vat_excl": ["Total HT", {
            "graph_type": "measure",
        }],
        "x_margin": ["Marge HT", {
            "graph_type": "measure",
        }],
        "x_is_alimentary": ["Est un article Alimentaire"],
        "x_country_id": ["Pays"],
        "x_state_id": ["Region"],
        "x_department_id": ["Departement"],
        "x_maker_description": ["Marque"],
        "x_template_id": ["Modele d'article", {
            "many2one_model_id": "product.template"}
        ],
        "x_consignor_partner_id": ["Depot Vendeur"],
        "x_product_active": ["Article Actif"],
        "x_fiscal_classification_id": ["Categorie Fiscale"],
        "x_template_categ_id": ["Categorie d'article", {
            "many2one_model_id": "product.category"}
        ],
        "x_intermediate_categ_id": ["Categorie d'article intermediaire", {
            "many2one_model_id": "product.category"}
        ],
        "x_root_categ_id": ["Categorie d'article racine", {
            "many2one_model_id": "product.category"}
        ],
        "x_uom_category_id": ["Categorie d'UdM de l'article", {
            "many2one_model_id": "uom.category"}
        ],
        "x_main_partner_id": ["Fournisseur principal ACTUEL", {
            "many2one_model_id": "res.partner"}
        ],
        "x_account_type": ["Type comptable", {
            "is_index": True,
            "graph_type": "col",
            "ttype": "selection",
            "selection": """[
                ('consignment', 'Depot Vente'),
                ('consignment_commission', 'Commission sur depot vente'),
                ('sale_from_purchase', 'Vente de marchandises'),
                ('sale_from_make', 'Vente de produits finis'),
                ('sale_service', 'Vente de service'),
            ]"""}
        ]
    },
    "query":
    """
SELECT
    sale_data.x_company_id,
    sale_data.x_customer_id,
    sale_data.x_sale_type,
    sale_data.x_sale_date,
    sale_data.x_user_id,
    sale_data.x_pricelist_id,
    sale_data.x_place_id,
    sale_data.x_product_uom_qty,
    sale_data.x_margin,
    sale_data.x_price_total_vat_excl,
    product_data.x_product_id,
    product_data.x_is_alimentary,
    product_data.x_country_id,
    product_data.x_state_id,
    product_data.x_department_id,
    product_data.x_maker_description,
    product_data.x_consignor_partner_id,
    product_data.x_product_active,
    product_data.x_template_categ_id,
    product_data.x_intermediate_categ_id,
    product_data.x_root_categ_id,
    product_data.x_uom_category_id,
    product_data.x_main_partner_id,
    product_data.x_account_type,
    rc.category_id as x_company_category_id
FROM  x_bi_sql_view_technical_sale_pos_invoice_line sale_data
INNER JOIN x_bi_sql_view_technical_product_product_list product_data
    ON sale_data.x_product_id = product_data.x_product_id
INNER JOIN res_company rc
    ON sale_data.x_company_id = rc.id
WHERE product_data.x_account_type in (
    'consignment',
    'consignment_commission',
    'sale_from_purchase',
    'sale_from_make',
    'sale_service'
)
    """,
}


bi_sale_product_other = {
    "name": "Activité - Vente de produits (Activité connexe)",
    "technical_name": "bi_sale_product_other",
    "view_name": "x_bi_sql_view_bi_sale_product_other",
    "is_materialized": True,
    "domain_force": "[('x_company_id', 'child_of', [user.company_id.id])]",
    "action_context": {
        "pivot_measures": [
            "x_price_total_vat_excl",
            "x_margin",
            "__computed_margin_rate"
        ],
        "pivot_computed_measures": [{
            "field1": "x_margin",
            "field2": "x_price_total_vat_excl",
            "operation": "x_margin/x_price_total_vat_excl",
            "name": "Taux de marque",
            "id": "__computed_margin_rate",
            "format": "percentage"
        }]
    },
    "fields": {
        "x_company_id": ["Societe", {"is_index": True}],
        "x_company_category_id": ["Categorie de societe", {
            "many2one_model_id": "res.company.category"}
        ],
        "x_customer_id": ["Client", {
            "many2one_model_id": "res.partner"}
        ],
        "x_sale_type": ["Type de vente", {
            "ttype": "selection",
            "selection": """[
                ('point_of_sale_eshop', 'Vente Via eBoutique'),
                ('point_of_sale_regular', 'Vente Classique'),
                ('invoice', 'Facture'),
            ]"""},
        ],
        "x_sale_date": ["Date de vente", {
            "is_index": True,
            "graph_type": "row",
        }],
        "x_product_id": ["Article", {
            "many2one_model_id": "product.product"}
        ],
        "x_user_id": ["Utilisateur"],
        "x_pricelist_id": ["Liste de prix"],
        "x_place_id": ["Lieu de vente", {
            "many2one_model_id": "pos.place"}
        ],
        "x_product_uom_qty": ["Quantite"],
        "x_price_total_vat_excl": ["Total HT", {
            "graph_type": "measure",
        }],
        "x_margin": ["Marge HT", {
            "graph_type": "measure",
        }],
        "x_is_alimentary": ["Est un article Alimentaire"],
        "x_country_id": ["Pays"],
        "x_state_id": ["Region"],
        "x_department_id": ["Departement"],
        "x_maker_description": ["Marque"],
        "x_template_id": ["Modele d'article", {
            "many2one_model_id": "product.template"}
        ],
        "x_consignor_partner_id": ["Depot Vendeur"],
        "x_product_active": ["Article Actif"],
        "x_fiscal_classification_id": ["Categorie Fiscale"],
        "x_template_categ_id": ["Categorie d'article", {
            "many2one_model_id": "product.category"}
        ],
        "x_intermediate_categ_id": ["Categorie d'article intermediaire", {
            "many2one_model_id": "product.category"}
        ],
        "x_root_categ_id": ["Categorie d'article racine", {
            "many2one_model_id": "product.category"}
        ],
        "x_uom_category_id": ["Categorie d'UdM de l'article", {
            "many2one_model_id": "uom.category"}
        ],
        "x_main_partner_id": ["Fournisseur principal ACTUEL", {
            "many2one_model_id": "res.partner"}
        ],
        "x_account_type": ["Type comptable", {
            "is_index": True,
            "graph_type": "col",
            "ttype": "selection",
            "selection": """[
                ('deposit', 'Consigne'),
                ('voucher', "Bon d'achat"),
                ('discount_sale', 'Remise sur vente'),
                ('other', 'AUTRES'),
            ]"""}
        ]
    },
    "query":
    """
SELECT
    sale_data.x_company_id,
    sale_data.x_customer_id,
    sale_data.x_sale_type,
    sale_data.x_sale_date,
    sale_data.x_user_id,
    sale_data.x_pricelist_id,
    sale_data.x_place_id,
    sale_data.x_product_uom_qty,
    sale_data.x_margin,
    sale_data.x_price_total_vat_excl,
    product_data.x_product_id,
    product_data.x_is_alimentary,
    product_data.x_country_id,
    product_data.x_state_id,
    product_data.x_department_id,
    product_data.x_maker_description,
    product_data.x_consignor_partner_id,
    product_data.x_product_active,
    product_data.x_template_categ_id,
    product_data.x_intermediate_categ_id,
    product_data.x_root_categ_id,
    product_data.x_uom_category_id,
    product_data.x_main_partner_id,
    product_data.x_account_type,
    rc.category_id as x_company_category_id
FROM  x_bi_sql_view_technical_sale_pos_invoice_line sale_data
INNER JOIN x_bi_sql_view_technical_product_product_list product_data
    ON sale_data.x_product_id = product_data.x_product_id
INNER JOIN res_company rc
    ON sale_data.x_company_id = rc.id
WHERE product_data.x_account_type in (
    'deposit',
    'voucher',
    'discount_sale',
    'other'
)
    """,
}

technical_purchase_order_line = {
    "name": "[TECH] #10 - purchase_invoice_line",
    "technical_name": "technical_purchase_order_line",
    "view_name": "x_bi_sql_view_technical_purchase_order_line",
    "state": "model_valid",
    "is_materialized": True,
    "domain_force": "[('x_company_id', 'child_of', [user.company_id.id])]",
    "action_context": {},
    "fields": {
        "x_company_id": ["Societe", {"is_index": True}],
        "x_supplier_name": ["Nom du fournisseur"],
        "x_purchase_date": ["Date de la vente"],
        "x_product_id": ["Article"],
        "x_product_uom_qty": ["Quantite"],
        "x_price_total_vat_excl": ["Total HT"],
    },
    "query": """
SELECT
    x_company_id,
    x_supplier_name,
    x_purchase_date,
    x_product_id,
    sum(x_product_uom_qty) as x_product_uom_qty,
    sum(x_price_total_vat_excl) as x_price_total_vat_excl
FROM (
    SELECT
        ai.company_id AS x_company_id,
            INITCAP(TRIM(rp.name)) AS x_supplier_name,
        date_trunc(
            'day',
            ai.date_invoice::timestamp with time zone
            AT TIME ZONE 'Europe/Paris'
        ) as x_purchase_date,
        ail.product_id as x_product_id,
        CASE WHEN ai.type = 'in_invoice' THEN
            ail.quantity / uom_ail.factor * uom_pt.factor
        ELSE
            - (ail.quantity / uom_ail.factor * uom_pt.factor)
        END::decimal(16, 3) as x_product_uom_qty,
            ail.price_subtotal_signed AS x_price_total_vat_excl
    FROM account_invoice_line ail
    INNER JOIN account_invoice ai
        ON ail.invoice_id = ai.id
    INNER JOIN product_product pp
        ON ail.product_id = pp.id
    INNER JOIN product_template pt
        ON pp.product_tmpl_id = pt.id
    INNER JOIN uom_uom uom_ail
        ON uom_ail.id = ail.uom_id
    INNER JOIN uom_uom uom_pt
        ON uom_pt.id = pt.uom_id
    INNER JOIN res_partner rp
        ON rp.id = ai.partner_id
    WHERE
        ai.state NOT IN ('draft', 'cancel', 'verified')
        AND ai.type IN ('in_invoice', 'in_refund')
        AND ail.quantity != 0
) as tmp
GROUP BY
    x_company_id,
    x_supplier_name,
    x_purchase_date,
    x_product_id
    """
}

bi_purchase_product = {
    "name": "Activité - Achat de produits",
    "technical_name": "technical_bi_purchase_product",
    "view_name": "x_bi_sql_view_bi_purchase_product",
    "is_materialized": True,
    "domain_force": "[('x_company_id', 'child_of', [user.company_id.id])]",
    "action_context": {
        "pivot_measures": [
            "x_price_total_vat_excl",
        ],
    },
    "fields": {
        "x_company_id": ["Societe", {"is_index": True}],
        "x_supplier_name": ["Nom du fournisseur"],
        "x_purchase_date": ["Date de la vente", {"graph_type": "row"}],
        "x_product_id": ["Article"],
        "x_product_uom_qty": ["Quantite"],
        "x_price_total_vat_excl": ["Total HT", {"graph_type": "measure"}],
        "x_is_alimentary": ["Est article alimentaire"],
        "x_country_id": ["Pays"],
        "x_state_id": ["Région"],
        "x_department_id": ["Departement"],
        "x_template_categ_id": ["Categorie d'article", {
            "many2one_model_id": "product.category"}
        ],
        "x_intermediate_categ_id": ["Categorie d'article intermediaire", {
            "many2one_model_id": "product.category"}
        ],
        "x_root_categ_id": ["Categorie d'article racine", {
            "many2one_model_id": "product.category"}
        ],
        "x_uom_category_id": ["Categorie d'UdM de l'article", {
            "many2one_model_id": "uom.category"}
        ],
        "x_fiscal_classification_id": ["Categorie Fiscale"],
    },
    "query": """
SELECT
    purchase_data.x_company_id,
    purchase_data.x_supplier_name,
    purchase_data.x_purchase_date,
    purchase_data.x_product_uom_qty,
    purchase_data.x_price_total_vat_excl,
    product_data.x_product_id,
    product_data.x_is_alimentary,
    product_data.x_country_id,
    product_data.x_state_id,
    product_data.x_department_id,
    product_data.x_template_categ_id,
    product_data.x_intermediate_categ_id,
    product_data.x_root_categ_id,
    product_data.x_uom_category_id,
    product_data.x_fiscal_classification_id
FROM x_bi_sql_view_technical_purchase_order_line as purchase_data
INNER JOIN x_bi_sql_view_technical_product_product_list product_data
    ON purchase_data.x_product_id = product_data.x_product_id
    """
}

bi_recurring_consignement = {
    "name": "Activité - Dépôt Vente",
    "technical_name": "bi_recurring_consignement",
    "view_name": "x_bi_sql_view_bi_recurring_consignement",
    "is_materialized": True,
    "domain_force": "[('x_company_id', 'child_of', [user.company_id.id])]",
    "action_context": {
        "pivot_measures": [
            "x_total_received",
            "x_total_return",
        ],
    },
    "fields": {
        "x_company_id": ["Societe", {"is_index": True}],
        "x_partner_id": ["Depot Vendeur", {"graph_type": "col"}],
        "x_invoice_date": ["Date de commissionnement", {"graph_type": "row"}],
        "x_total_received": ["Total recu", {"graph_type": "measure"}],
        "x_total_return": ["Total a reverser", {"graph_type": "measure"}],
    },
    "query": """
SELECT
    ai.company_id as x_company_id,
    ai.partner_id as x_partner_id,
    ai.date_invoice as x_invoice_date,
    sum(aml.credit - aml.debit) as x_total_received,
    sum(aml.credit - aml.debit) - ai.amount_total as x_total_return
FROM account_invoice ai
INNER JOIN account_move_line aml on aml.consignment_invoice_id = ai.id
where ai.is_consignment_invoice = true
and ai.state not in ('draft', 'cancel')
group by ai.company_id, ai.partner_id, ai.date_invoice, ai.amount_total
    """
}

bi_stock_picking_prepare = {
    "name": "Stock - Préparation de commande",
    "technical_name": "bi_stock_picking_prepare",
    "view_name": "x_bi_sql_view_bi_stock_picking_prepare",
    "is_materialized": True,
    "domain_force": "[('x_company_id', 'child_of', [user.company_id.id])]",
    "action_context": {
        "pivot_measures": [
            "x_qty",
        ],
    },
    "fields": {
        "x_company_id": ["Societe", {"is_index": True}],
        "x_date": ["Date"],
        "x_customer_id": ["Client", {
            "many2one_model_id": "res.partner"}
        ],
        "x_recovery_moment_group_id": ["Groupe de recuperation"],
        "x_recovery_moment_id": ["Moment de recuperation", {
            "graph_type": "col",
        }],
        "x_place_id": ["Lieu de recuperation"],
        "x_supplier_id": ["Fournisseur principal"],
        "x_template_categ_id": ["Categorie d'article", {
            "many2one_model_id": "product.category",
            "graph_type": "row"
        }],
        "x_intermediate_categ_id": ["Categorie d'article intermediaire", {
            "many2one_model_id": "product.category"}
        ],
        "x_root_categ_id": ["Categorie d'article racine", {
            "many2one_model_id": "product.category"}
        ],
        "x_uom_id": ["Unite de mesure"],
        "x_product_id": ["Article", {
            "many2one_model_id": "product.product",
            "graph_type": "row"
        }],
        "x_qty": ["Quantite", {"graph_type": "measure"}],
    },
    "query": """
SELECT
    sp.company_id as x_company_id,
    sp.min_date as x_date,
    sp.partner_id as x_customer_id,
    sp.recovery_group_id as x_recovery_moment_group_id,
    sp.recovery_moment_id as x_recovery_moment_id,
    srm.place_id as x_place_id,
    product_data.x_main_partner_id as x_supplier_id,
    product_data.x_template_categ_id as x_template_categ_id,
    product_data.x_intermediate_categ_id as x_intermediate_categ_id,
    product_data.x_root_categ_id as x_root_categ_id,
    sm.product_uom as x_uom_id,
    sm.product_id as x_product_id,
    sm.product_uom_qty as x_qty
FROM stock_picking sp
INNER JOIN stock_move sm
    ON sp.id = sm.picking_id
INNER JOIN uom_uom pu
    ON sm.product_uom = pu.id
INNER JOIN  x_bi_sql_view_technical_product_product_list product_data
    ON product_data.x_product_id = sm.product_id
INNER JOIN sale_recovery_moment srm
    ON sp.recovery_moment_id = srm.id
LEFT OUTER JOIN sale_recovery_moment_group srmg
    ON sp.recovery_group_id = srmg.id
WHERE sp.recovery_moment_id is not null
    AND sp.state not in ('draft', 'cancel')
    AND sp.min_date >= current_timestamp - interval '10 hour'
    """
}


bi_internal_use = {
    "name": "Stock - Utilisations internes",
    "technical_name": "bi_internal_use",
    "view_name": "x_bi_sql_view_bi_internal_use",
    "is_materialized": True,
    "domain_force": "[('x_company_id', 'child_of', [user.company_id.id])]",
    "action_context":  {
        "pivot_measures": [
            "x_amount",
        ],
    },
    "fields": {
        "x_company_id": ["Societe", {"is_index": True}],
        "x_date_done": ["Date", {"graph_type": "row"}],
        "x_internal_use_case_id": ["Categorie d'utilisation interne", {
            "graph_type": "col",
        }],
        "x_product_id": ["Article", {"many2one_model_id": "product.product"}],
        "x_product_qty": ["Quantite"],
        "x_product_uom_id": ["Unite de mesure"],
        "x_amount": ["Valeur HT", {"graph_type": "measure"}],
        "x_template_categ_id": ["Categorie d'article", {
            "many2one_model_id": "product.category"}
        ],
        "x_intermediate_categ_id": ["Categorie d'article intermediaire", {
            "many2one_model_id": "product.category"}
        ],
        "x_root_categ_id": ["Categorie d'article racine", {
            "many2one_model_id": "product.category"}
        ],
        "x_main_partner_id": ["Fournisseur principal ACTUEL"],
    },
    "query": """
SELECT
    iu.company_id as x_company_id,
    iu.date_done as x_date_done,
    iu.internal_use_case_id as x_internal_use_case_id,
    iul.product_id as x_product_id,
    iul.product_qty as x_product_qty,
    iul.product_uom_id as x_product_uom_id,
    iul.amount as x_amount,
    product_data.x_template_categ_id,
    product_data.x_intermediate_categ_id,
    product_data.x_root_categ_id,
    product_data.x_main_partner_id
FROM internal_use iu
INNER JOIN internal_use_line iul
    ON iul.internal_use_id = iu.id
INNER JOIN x_bi_sql_view_technical_product_product_list as product_data
    ON product_data.x_product_id = iul.product_id
where iu.state in ('confirmed', 'done')
    """
}

bi_product_loss = {
    "name": "Stock - Pertes de produits",
    "technical_name": "bi_product_loss",
    "view_name": "x_bi_sql_view_bi_product_loss",
    "is_materialized": True,
    "domain_force": "[('x_company_id', 'child_of', [user.company_id.id])]",
    "action_context": {
        "pivot_measures": [
            "x_amount_total",
        ],
    },
    "fields": {
        "x_company_id": ["Societe", {"is_index": True}],
        "x_loss_date": ["date de la perte", {"graph_type": "row"}],
        "x_data_type": ["Type de perte", {
            "ttype": "selection",
            "selection": """[
                ('inventory', 'Demarque inconnue'),
                ('internal_use', 'Perte identifiee'),
            ]""",
            "graph_type": "col",
            }],
        "x_product_id": ["Article", {"many2one_model_id": "product.product"}],
        "x_product_qty": ["Quantite"],
        "x_amount_total": ["Total HT", {"graph_type": "measure"}],
        "x_template_categ_id": ["Categorie d'article", {
            "many2one_model_id": "product.category"}
        ],
        "x_intermediate_categ_id": ["Categorie d'article intermediaire", {
            "many2one_model_id": "product.category"}
        ],
        "x_root_categ_id": ["Categorie d'article racine", {
            "many2one_model_id": "product.category"}
        ],
        "x_main_partner_id": ["Fournisseur principal ACTUEL", {
            "many2one_model_id": "res.partner"}
        ],
    },
    "query": """
SELECT
    loss_data.company_id as x_company_id,
    loss_data.loss_date as x_loss_date,
    loss_data.data_type as x_data_type,
    loss_data.product_id as x_product_id,
    loss_data.product_qty as x_product_qty,
    loss_data.amount_total as x_amount_total,
    product_data.x_template_categ_id,
    product_data.x_intermediate_categ_id,
    product_data.x_root_categ_id,
    product_data.x_main_partner_id
FROM (
    SELECT
        company_id,
        'inventory' as data_type,
        loss_date,
        product_id,
        product_uom_qty as product_qty,
        product_uom_qty * price_unit as amount_total
    FROM (
        SELECT
            sm.id,
            si.company_id,
            si.date as loss_date,
            sm.product_id,
            CASE
            WHEN sl.usage = 'internal' THEN sm.product_uom_qty
            ELSE - sm.product_uom_qty
            END  as product_uom_qty,
            max(sil.price_unit) as price_unit
        FROM stock_inventory_line sil
        INNER JOIN stock_move sm
            ON  sm.inventory_id = sil.inventory_id
            AND sm.product_id = sil.product_id
        INNER JOIN stock_inventory si
            ON sil.inventory_id = si.id
        INNER JOIN stock_location sl
            ON sm.location_id = sl.id
        WHERE si.date >='2018-04-09'
        GROUP BY sm.id,
            sl.usage,
            si.company_id,
            si.date,
            sm.product_id,
            sm.product_uom_qty
        ) as tmp
     UNION SELECT
        iu.company_id,
        'internal_use' as data_type,
        date_trunc(
            'hour',
            iu.date_done::timestamp AT TIME ZONE 'UTC'
            AT TIME ZONE 'Europe/Paris'
        ) as loss_date,
        iul.product_id,
        iul.product_qty,
        iul.amount as amount_total
    FROM internal_use_line iul
    INNER JOIN internal_use iu on iu.id = iul.internal_use_id
    INNER JOIN internal_use_case iuc on iuc.id = iu.internal_use_case_id
    WHERE
         iuc.account_id is null
         AND iu.date_done >='2018-04-09'
    ) as loss_data
INNER JOIN x_bi_sql_view_technical_product_product_list as product_data
    ON product_data.x_product_id = loss_data.product_id
    """
}

_SQL_DATAS = [
    bi_pos_activity_day_hour,
    bi_pos_payment_type,
    bi_pos_activity_per_date,
    bi_pos_cashier_error,
    technical_account_income_product_category,
    technical_account_income_product_template,
    technical_product_product_list,
    technical_sale_pos_order_line,
    technical_sale_account_invoice_line,
    technical_sale_pos_invoice_line,
    bi_sale_product_main,
    bi_sale_product_other,
    technical_purchase_order_line,
    bi_purchase_product,
    bi_recurring_consignement,
    bi_stock_picking_prepare,
    bi_internal_use,
    bi_product_loss,
]


def main(self, step):
    if not self._is_model_present("bi.sql.view"):
        return

    BiSqlView = self.odoo_local.BiSqlView

    i = 0
    for _sql_data in _SQL_DATAS:
        i += 1
        prefix = str(i) + "/" + str(len(_SQL_DATAS))

        # TODO, check if exists, to make the script idempotent
        sql_views = BiSqlView.browse([
            ("technical_name", "=", _sql_data["technical_name"])
        ])
        if sql_views:
            if sql_views[0].state == 'draft':
                self.logger_info(
                    "%s - dropping the BiSqlView '%s' that is in the draft"
                    " state to recreate it" % (
                        prefix, _sql_data["technical_name"]))
                sql_views.unlink()
            else:
                self.logger_warning(
                    "%s - The BiSqlView '%s' is still present."
                    " Skipping the creation." % (
                        prefix, _sql_data["technical_name"]))
                continue

        # Create SQL View
        self.logger_info("%s - Creating a new Bi Sql View '%s' ..." % (
            prefix, _sql_data["technical_name"]))
        try:
            sql_vals = {}
            for x, y in _sql_data.items():
                if x in ["fields", "state"]:
                    continue
                sql_vals[x] = y
            state = _sql_data.get("state", "ui_valid")
            sql_view = BiSqlView.create(sql_vals)
            sql_view.button_validate_sql_expression()

            for field in sql_view.bi_sql_view_field_ids:
                line_data = _sql_data["fields"].get(field.name, False)
                if not line_data:
                    self.logger_warning(
                        "No description provided for the field %s"
                        " of the bi.sql.view %s" % (
                            field.name, sql_view.technical_name
                        ))
                line_vals = {
                    "field_description": line_data[0],
                }
                if len(line_data) > 1:
                    line_vals.update(line_data[1])
                if "many2one_model_id" in line_vals:
                    line_vals["many2one_model_id"] =\
                        self.odoo_local.IrModel.search([
                            ("model", "=", line_vals["many2one_model_id"])
                        ])[0]
                field.write(line_vals)

            if state in ["model_valid", "ui_valid"]:
                sql_view.button_create_sql_view_and_model()

            if state in ["ui_valid"]:
                sql_view.button_create_ui()
        except Exception as e:
            self.logger_error("Unable to create the bi.sql.view. %s" % e)
