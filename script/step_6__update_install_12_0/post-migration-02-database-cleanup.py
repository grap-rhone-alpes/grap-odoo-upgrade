def main(self, step):
    # TODO, remove this lines
    self.logger_warning("WARNING ! DATABASE CLEANUP PARTIALLY DISABLED")

    if not self._is_model_present("cleanup.purge.wizard.module"):
        return

    purge_concepts = [{
        "logger_text": "Purging Obsolete Modules ...",
        "wizard_model": self.odoo_local.CleanupPurgeWizardModule,
        "line_model": self.odoo_local.CleanupPurgeLineModule,
        "logger_line_text": lambda line: line.name,
    },
        {
        "logger_text": "Purging Obsolete Models ...",
        "wizard_model": self.odoo_local.CleanupPurgeWizardModel,
        "line_model": self.odoo_local.CleanupPurgeLineModel,
        "logger_line_text": lambda line: line.name,
        "except": lambda line: line.name == "procurement.order",
    },
        # {
        #     "logger_text": "Purging Obsolete Columns ...",
        #     "wizard_model": self.odoo_local.CleanupPurgeWizardColumn,
        #     "line_model": self.odoo_local.CleanupPurgeLineColumn,
        #     "logger_line_text": lambda line: "%s - %s" % (
        #         line.model_id.model, line.name),
        # },
        #     {
        #     "logger_text": "Purging Obsolete Tables ...",
        #     "wizard_model": self.odoo_local.CleanupPurgeWizardTable,
        #     "line_model": self.odoo_local.CleanupPurgeLineTable,
        #     "logger_line_text": lambda line: line.name,
        # },
        {
        "logger_text": "Purging Obsolete Menus ...",
        "wizard_model": self.odoo_local.CleanupPurgeWizardMenu,
        "line_model": self.odoo_local.CleanupPurgeLineMenu,
        "logger_line_text": lambda line: line.name,
    }]

    something_purged = True
    purge_count = 0
    while something_purged:
        purge_count += 1
        self.logger_info("Launcing Purge #%d" % purge_count)
        something_purged = False
        for purge_concept in purge_concepts:
            self.logger_info(purge_concept["logger_text"])
            try:
                lines = purge_concept["wizard_model"].find()
            except Exception:
                self.logger_info("> Nothing to purge !")
                continue
            i = 0
            count = len(lines)
            self.logger_info("> Found %d items to purge !" % count)
            for line in lines:
                i += 1
                if type(line) == list:
                    line_vals = line[2]
                elif type(line) == dict:
                    line_vals = line
                purge_line = purge_concept["line_model"].create(line_vals)
                if purge_concept.get("except", False):
                    if purge_concept["except"](purge_line):
                        self.logger_warning("> %d / %d : SKIPPING '%s' ... " % (
                            i, count,
                            purge_concept["logger_line_text"](purge_line)))
                        continue
                try:
                    self.logger_info("> %d / %d : Purging '%s' ... " % (
                        i, count, purge_concept["logger_line_text"](purge_line)
                    ))
                    purge_line.purge()
                    something_purged = True
                except Exception:
                    self.logger_error(
                        "Purging '%s' failed." % (purge_line.name))
    else:
        self.logger_info(
            "Nothing has been purged, in the last purge try."
            " Exiting. Purge Try: %d" % purge_count)
