-------------------------------------------
-- [base]
-------------------------------------------

-- drop obsolete ir.actions.client
delete
    from ir_act_client
    where id not in (
        select res_id
        from ir_model_data
        where model = 'ir.actions.client'
    );

-- set range_y to main sequence
UPDATE ir_sequence
SET prefix = substring(prefix, 0, position('/YEAR/' in prefix)+1) || '%(range_y)s/'
WHERE prefix like '%/YEAR/%';


-- disable obsolete subsequence
UPDATE ir_sequence
    SET active = false
    WHERE id IN (SELECT sequence_id FROM account_sequence_fiscalyear)
    AND active = true;


-- delete all action_id in res_users
update res_users set action_id = null where action_id is not null;

-- drop bad ir.default
delete from ir_default where field_id in (select id from ir_model_fields where name='action_id' and model='res.users');

-------------------------------------------
-- [account]
-------------------------------------------

-- clean transient model to avoid error when dropping account_account_template
delete from wizard_multi_charts_accounts;


update res_company set account_subsequence_method = 'fiscal_year_setting';


update account_tax
    set type_tax_use = 'sale'
    where id in (
        select tax_id from fiscal_classification_sale_tax_rel)
    and type_tax_use != 'sale';

update account_tax
    set type_tax_use = 'purchase'
    where id in (
        select tax_id from fiscal_classification_purchase_tax_rel)
    and type_tax_use != 'purchase';


-- Set new tax group to taxes for regular taxes. (with l10n_fr)
UPDATE account_tax
SET tax_group_id = (select res_id from ir_model_data where module='l10n_fr' and name = 'tax_group_tva_0')
WHERE amount = 0;
UPDATE account_tax
SET tax_group_id = (select res_id from ir_model_data where module='l10n_fr' and name = 'tax_group_tva_20')
WHERE amount = 20;
UPDATE account_tax
SET tax_group_id = (select res_id from ir_model_data where module='l10n_fr' and name = 'tax_group_tva_85')
WHERE amount = 8.5;
UPDATE account_tax
SET tax_group_id = (select res_id from ir_model_data where module='l10n_fr' and name = 'tax_group_tva_55')
WHERE amount = 5.5;
UPDATE account_tax
SET tax_group_id = (select res_id from ir_model_data where module='l10n_fr' and name = 'tax_group_tva_10')
WHERE amount = 10;
UPDATE account_tax
SET tax_group_id = (select res_id from ir_model_data where module='l10n_fr' and name = 'tax_group_tva_21')
WHERE amount = 2.1;
UPDATE account_tax
SET tax_group_id = (select res_id from ir_model_data where module='l10n_fr' and name = 'tax_group_intra_20')
WHERE amount = -20;
UPDATE account_tax
SET tax_group_id = (select res_id from ir_model_data where module='l10n_fr' and name = 'tax_group_intra_85')
WHERE amount = -8.5;
UPDATE account_tax
SET tax_group_id = (select res_id from ir_model_data where module='l10n_fr' and name = 'tax_group_intra_55')
WHERE amount = -5.5;
UPDATE account_tax
SET tax_group_id = (select res_id from ir_model_data where module='l10n_fr' and name = 'tax_group_intra_10')
WHERE amount = -10;
UPDATE account_tax
SET tax_group_id = (select res_id from ir_model_data where module='l10n_fr' and name = 'tax_group_intra_21')
WHERE amount = -2.1;

-- Set default tax_group for all the bad / obsolete taxes
UPDATE account_tax
SET tax_group_id = (select res_id from ir_model_data where module='account' and name = 'tax_group_taxes')
WHERE tax_group_id not in (select res_id from ir_model_data where module='l10n_fr' and model='account.tax.group');


-- -- Delete all other tax groups (take to much time)
update account_tax_group
    set active = false
WHERE id not IN (
    SELECT res_id
    FROM ir_model_data WHERE model='account.tax.group');

-- Fix account tax description
UPDATE account_tax
    SET description = to_char(amount, '99D9') || '%'
    WHERE amount != 0;

UPDATE account_tax
    SET description = '0%'
    WHERE amount = 0;

-- Delete useless account account tag (if Alexis is OK...)
DELETE FROM account_account_tag
WHERE id not IN (
    SELECT res_id
    FROM ir_model_data WHERE model='account.account.tag');

-- finish the 9.0 migration
-- TODO, we will have to delete tranquilelouly this items
update account_account set deprecated = true where name ilike '%DISABLED BY OpenUpgrade%';


update account_journal
    set post_at_bank_rec = true
    where code not in (
        'AC', 'ACAV', 'OD', 'STJ', 'CH', 'VT', 'VTAV',
        'CS', 'CB', 'TR', 'ACTI', 'CA', 'GO', 'SAJ', 'VTTI', 'AN'
    );

-------------------------------------------
-- [product]
-------------------------------------------

-- delete template without product
delete from product_template where id not in (select product_tmpl_id from product_product);

-- delete product price history of the deleted template
delete from product_price_history where product_id is null;

-- delete obsolete property based on unexisting product category
DELETE FROM ir_property
    WHERE res_id ilike 'product.category%'
    AND cast (substring(res_id FROM 18) AS int4) NOT IN (
        SELECT id
        FROM product_category
    );

-------------------------------------------
-- [product_margin_classification]
-------------------------------------------

-- Fix some products with incorrect margin_state value. (bug between 8, 10 and 12 version)
UPDATE product_product pp
SET margin_state = 'correct'
WHERE margin_state = 'ok';

-------------------------------------------
-- [uom]
-------------------------------------------

-- Renable some units, disabled during the migration
update uom_uom uom
    set active=true
    where uom.id in (
        select distinct(pt.uom_po_id)
        from product_template pt
        where pt.active)
    and uom_type != 'reference'
    and active is false;

-- Disable Volume Unit
update uom_uom
    set active = false
    where category_id in (3, 7)
    and active is true;

-- Disable distance Unit
update uom_uom
    set active = false
    where category_id = 6
    and active is true;

-------------------------------------------
-- [stock]
-------------------------------------------
-- delete two packages
delete from product_packaging;

-- make delete stock_rule quicker
ALTER TABLE stock_move DROP constraint stock_move_rule_id_fkey;

-- QD request
delete from stock_rule r where r.id not in (
    select distinct(rule_id) from stock_move where rule_id is not null
);

-- delete obsolete stock rule that block sql contrainst
delete from stock_rule
    where active = false and picking_type_id is null;

-- fixing picking of FEN and STR
UPDATE stock_picking
SET location_dest_id = 8
WHERE picking_type_id in (394, 418) AND location_dest_id is NULL;

-- set scheduled_date if not set, based on date_done, or create_date
-- ????

-- set backorder strategy
update stock_picking_type set backorder_strategy = 'manual';

-- set show_operations to true to all incoming picking.type
update  stock_picking_type set show_operations = true where code in ('incoming', 'outgoing');

-------------------------------------------
-- [stock_move_line_auto_fill]
-------------------------------------------
update stock_picking_type set auto_fill_operation = true;

-------------------------------------------
-- [point_of_sale]
-------------------------------------------

-- fix no updatable data ir_sequence for pos session.
UPDATE ir_sequence
SET prefix = 'POS/%(day)s/%(month)s/%(year)s/',
company_id = null,
code = 'pos.session',
padding = 5
WHERE id = 250;

-- Fix set amount value to 0.0 if null
update pos_order set amount_tax=0.0 where amount_tax is null;
update pos_order set amount_return=0.0 where amount_return is null;
update pos_order set amount_total=0.0 where amount_total is null;
update pos_order set amount_paid=0.0 where amount_paid is null;

-- fix start_at null values for pos.session, if closed
update pos_session
    set start_at = create_date
    where start_at is null
    and state = 'closed';

-- fix stop_at null values for pos.session, if closed
update pos_session
    set stop_at = write_date
    where stop_at is null
    and state = 'closed';

-- set all pricelist available for all companies
insert into pos_config_product_pricelist_rel
    (pos_config_id, product_pricelist_id) (
        select pc.id, ppl.id
        from pos_config pc
        inner join product_pricelist ppl
        on ppl.company_id = pc.company_id
        and ppl.active = true
    );

-- Migrate market_place into pos_place
insert into pos_place
    (id, code, name, active, company_id, create_uid, create_date, write_uid, write_date) (
        select
        id, code, name, active, company_id, create_uid, create_date, write_uid, write_date
        from market_place);

update pos_order
    set place_id = market_place_id
    where market_place_id is not null;

-- configure pos_config
update pos_config set iface_gross_weight_method = 'manual';
update pos_config set iface_tare_method = 'manual';
update pos_config set is_posbox = true;
update pos_config set use_pricelist = true;
update pos_config set is_header_or_footer = true;
update pos_config set cash_control = true;
UPDATE pos_config SET iface_print_auto = false;
UPDATE pos_config SET iface_create_draft_sale_order = true;
update pos_config SET iface_display_categ_images = true;
update pos_config set iface_precompute_cash = true;



-- pos_picking_delayed: delete obsolete cron (now by queue job)
delete from ir_cron
where id in (
    select res_id
    from ir_model_data
    where module = 'pos_picking_delayed'
    and name = 'cron_create_delayed_pos_picking'
);

-------------------------------------------
-- [pos_meal_voucher]
-------------------------------------------

-- initialize products depending on the product_category settings
-- previously loaded from csv file, in step 5
update product_template set meal_voucher_ok = false;

update product_template pt
    set meal_voucher_ok = true
from product_category pc
    where pc.id = pt.categ_id
    and pc.meal_voucher_ok is true;

-- initialize account journal
update account_journal set meal_voucher_type = 'paper' where code = 'TR';

-- initialize pos.config
update pos_config set max_meal_voucher_amount = 19;

update pos_config set max_meal_voucher_amount = 38
    where id in (
        select id from res_company
        where category_id in (
            select id from res_company_category
            where name in ('Epicerie / bar / restaurant', 'Restauration')
        )
    );

-------------------------------------------
-- [pos_multiple_control]
-------------------------------------------

-- configure correctly account.journal
update account_journal
    set pos_control = true where type in ('bank', 'cash') and journal_user is true;

-------------------------------------------
-- [web]
-------------------------------------------

-- disable obsolete ir_ui_view to make the regeneration of assets OK.
update ir_ui_view
    set active = false
    where id not in (
        select res_id
        from ir_model_data
        where model = 'ir.ui.view'
    )
    and active = true;

-- [purchase]
-- set a global sequence for purchase.order

update ir_sequence
    set company_id = null
    where id in (
        select res_id
        from ir_model_data
        where module = 'purchase'
        and name='seq_purchase_order'
    );

-------------------------------------------
-- [sale]
-------------------------------------------

-- set a global sequence for sale.order

update ir_sequence
    set company_id = null
    where id in (
        select res_id
        from ir_model_data
        where module = 'sale'
        and name='seq_sale_order'
    );

-------------------------------------------
-- [internal_use_of_products]
-------------------------------------------

-- fix sequence. previously, company was ignored, but it's not the case anymore

update ir_sequence set company_id = null where code = 'internal.use';


-------------------------------------------
-- [multi_company_barcodes]
-------------------------------------------

-- set correct company to barcodes
update barcode_nomenclature bn
    set company_id = pc.company_id
from pos_config pc
where pc.barcode_nomenclature_id = bn.id;


delete from barcode_nomenclature where company_id is null;

-- ----------------------------------------------------------------------------
-- Always begin with the same company for the admin users
-- ----------------------------------------------------------------------------

update res_users set company_id = (
    select id from res_company where code ='DEL'
)
where id = 1;

update res_partner p set company_id = (
    select id from res_company where code ='DEL'
)
from res_users u
where u.id = 1 and u.partner_id = p.id;
