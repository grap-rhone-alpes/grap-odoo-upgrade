def main(self, step):
    # multi_search_product + multi_search_partner
    self.logger_info(
        "Configuring modules multi_search_product and multi_search_partner")
    configSetting = self.odoo_local.ResConfigSettings.create({})
    configSetting.write({
        "multi_search_partner_separator": "*",
        "multi_search_partner_separator_changed": True,
        "multi_search_product_separator": "*",
        "multi_search_product_separator_changed": True,
    })
    configSetting.execute()

    # web_dialog_size
    self.logger_info(
        "Configuring module web_dialog_size")
    if not self.odoo_local.IrConfig_parameter.browse(
            [('key', '=', 'web_dialog_size.default_maximize')]):
        self.odoo_local.IrConfig_parameter.create({
            "key": "web_dialog_size.default_maximize",
            "value": True
        })
