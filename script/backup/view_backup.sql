-- [sale] clean VAT database
-- TODO : to the same for account.tax)
UPDATE sale_order_line
    SET tax_ids_description='Prod001 - 05.5 - Pain des montagnes'
    WHERE tax_ids_description = 'Prod001 - 5.5 - Pain des montagnes';
UPDATE sale_order_line
    SET tax_ids_description='Prod002 - 05.5 - Ghislaine GUERRAZ'
    WHERE tax_ids_description = 'Prod002 - 5.5 - Ghislaine GUERRAZ';
UPDATE sale_order_line
    SET tax_ids_description='Prod003 - 05.5 - GAEC La ferme du parquet'
    WHERE tax_ids_description = 'Prod003 - 5.5 - GAEC La ferme du parquet';
UPDATE sale_order_line
    SET tax_ids_description='Prod003 - 05.5 NON ASSUJETTI - Les véganiciennes'
    WHERE tax_ids_description = 'Prod003 - 5.5 - NON ASSUJETTI - Les véganiciennes';
UPDATE sale_order_line
    SET tax_ids_description='Prod004 - 05.5 - Avec Viuz sur Jardin'
    WHERE tax_ids_description = 'Prod004 - 5.5 - Avec Viuz sur Jardin';
UPDATE sale_order_line
    SET tax_ids_description='Prod005 - 05.5 NON ASSUJETTI - Thomas CHARBONNIER'
    WHERE tax_ids_description = 'Prod005 - 5.5 NON ASSUJETTI - Thomas CHARBONNIER';
UPDATE sale_order_line
    SET tax_ids_description='Prod005 - 20.0 - GAEC de Chambourlas'
    WHERE tax_ids_description = 'Prod005 - 20.0 GAEC de Chambourlas';
UPDATE sale_order_line
    SET tax_ids_description='Prod005 - 05.5 - GAEC de Chambourlas'
    WHERE tax_ids_description = 'Prod005 - 05.5 GAEC de Chambourlas';
UPDATE sale_order_line
    SET tax_ids_description='Prod006 - 05.5 - La ferme du blaizac'
    WHERE tax_ids_description = 'Prod006 - 05.5 La ferme du blaizac';
UPDATE sale_order_line
    SET tax_ids_description='Prod006 - 05.5 NON ASSUJETTI - Les oréades des Bauges'
    WHERE tax_ids_description = 'Prod006 - 5.5 - NON ASSUJETTI - Les oréades des Bauges';
UPDATE sale_order_line
    SET tax_ids_description='Prod007 - 05.5 - GAEC L escargot de la grange de Dime'
    WHERE tax_ids_description = 'Prod007 - 5.5 - GAEC L escargot de la grange de Dime';
UPDATE sale_order_line
    SET tax_ids_description='Prod008 - 05.5 - Domaine des anges au coeur rouge'
    WHERE tax_ids_description = 'Prod008 - 5.5 - Domaine des anges au coeur rouge';
UPDATE sale_order_line
    SET tax_ids_description='Prod011 - 05.5 - Les Trésors d Abeille'
    WHERE tax_ids_description = 'Prod011 - 5.5 - Les Trésors d Abeille'
    OR tax_ids_description = 'Prod011 - 5.5 - Les Trésors d''Abeille';
UPDATE sale_order_line
    SET tax_ids_description='Prod012 - 05.5  NON ASSUJETTI - Nadège BOGEY'
    WHERE tax_ids_description = 'Prod012 - 5.5  NON ASSUJETTI - Nadège BOGEY';
UPDATE sale_order_line
    SET tax_ids_description='Prod015 - 05.5 NON ASSUJETTI - La ferme des cyclamens'
    WHERE tax_ids_description = 'Prod015 - 5.5 - NON ASSUJETTI - La ferme des cyclamens';
UPDATE sale_order_line
    SET tax_ids_description='Prod016 - 05.5 - La ferme des Suavets'
    WHERE tax_ids_description = 'Prod016 - 5.5 - La ferme des Suavets';
UPDATE sale_order_line
    SET tax_ids_description='Prod017 - 05.5 - Pascal Gay'
    WHERE tax_ids_description = 'Prod017 - 5.5 - Pascal Gay';
UPDATE sale_order_line
    SET tax_ids_description='Prod018 - 05.5 - La ferme du Coteau'
    WHERE tax_ids_description = 'Prod018 - 5.5 - La ferme du Coteau';
UPDATE sale_order_line
    SET tax_ids_description='Prod019 - 05.5 - Les vergers de Raucaz'
    WHERE tax_ids_description = 'Prod019 - 5.5 - Les vergers de Raucaz';
UPDATE sale_order_line
    SET tax_ids_description='Prod020 - 05.5 - GAEC Aux Sources du pain'
    WHERE tax_ids_description = 'Prod020 - 5.5 - GAEC Aux Sources du pain';
UPDATE sale_order_line
    SET tax_ids_description='Prod020 - 05.5 NON ASSUJETTI - Ferme La Charrette'
    WHERE tax_ids_description = 'Prod020 - 5.5 NON ASSUJETTI Ferme La Charrette';
UPDATE sale_order_line
    SET tax_ids_description='Prod020 - 20.0 NON ASSUJETTI - Ferme La Charrette'
    WHERE tax_ids_description = 'Prod020 - 20.0 NON ASSUJETTI Ferme La Charrette';
UPDATE sale_order_line
    SET tax_ids_description='Prod021 - 05.5 NON ASSUJETTI - Benoît BONNETON'
    WHERE tax_ids_description = 'Prod021 - 5.5 - NON ASSUJETTI - Benoît BONNETON';
UPDATE sale_order_line
    SET tax_ids_description='Prod022 - 05.5 - La ferme de Cote Chaude'
    WHERE tax_ids_description = 'Prod022 - 5.5 - La ferme de Cote Chaude';
UPDATE sale_order_line
    SET tax_ids_description='Prod025 - 05.5 - Le sanglier philosophe'
    WHERE tax_ids_description = 'Prod025 - 5.5 - Le sanglier philosophe';
UPDATE sale_order_line
    SET tax_ids_description='Prod026 - 05.5 - Asinerie du Cul du Bois'
    WHERE tax_ids_description = 'Prod026 - 5.5 - Asinerie du Cul du Bois';
UPDATE sale_order_line
    SET tax_ids_description='Prod027 - 05.5 - Céline Vuitton "Simplement Paysanne"'
    WHERE tax_ids_description = 'Prod027 - 05,5% - Céline Vuitton "Simplement Paysanne"';
UPDATE sale_order_line
    SET tax_ids_description='Prod027 - 05.5 NON ASSUJETTI - A l heure du thé'
    WHERE tax_ids_description = 'Prod027 - 5.5 - NON ASSUJETTI - A l heure du thé';
UPDATE sale_order_line
    SET tax_ids_description='Prod028 - 05.5 - Arlette Courtial'
    WHERE tax_ids_description = 'Prod028 - 05.5 Arlette Courtial';
UPDATE sale_order_line
    SET tax_ids_description='Prod028 - 05.5 - Fabien BEGNIS'
    WHERE tax_ids_description = 'Prod028 - 5.5 - Fabien BEGNIS';
UPDATE sale_order_line
    SET tax_ids_description='Prod029 - 05.5 - Le poulailler de la Chambotte'
    WHERE tax_ids_description = 'Prod029 - 5.5 - Le poulailler de la Chambotte';
UPDATE sale_order_line
    SET tax_ids_description='Prod030 - 02.1 NON ASSUJETTI - Radio Alto'
    WHERE tax_ids_description = 'Prod030 - 2.1 - NON ASSUJETTI - Radio Alto';
UPDATE sale_order_line
    SET tax_ids_description='Prod031 - 05.5 -  A l aunée des bois'
    WHERE tax_ids_description = 'Prod031 - 5.5 -  A l aunée des bois';
UPDATE sale_order_line
    SET tax_ids_description='Prod031 - 05.5 NON ASSUJETTI - A l aunée des bois'
    WHERE tax_ids_description = 'Prod031 - 5.5 - NON ASSUJETTI - A l aunée des bois'
    OR tax_ids_description = 'Prod031 - 5.5 - NON ASSUJETTI - A l''aunée des bois';
UPDATE sale_order_line
    SET tax_ids_description='Prod033 - 05.5 - L abeille Verte '
    WHERE tax_ids_description = 'Prod033 - 5.5 - L abeille Verte '
    OR tax_ids_description = 'Prod033 - 5.5 - L''abeille Verte';
UPDATE sale_order_line
    SET tax_ids_description='Prod034 - 05.5 NON ASSUJETTI - Le Jardin de Potiroux'
    WHERE tax_ids_description = 'Prod034 - 5.5 NON ASSUJETTI - Le Jardin de Potiroux';
UPDATE sale_order_line
    SET tax_ids_description='Prod035 - 05.5 - La ferme du chêne'
    WHERE tax_ids_description = 'Prod035 - 5.5 - La ferme du chêne';
UPDATE sale_order_line
    SET tax_ids_description='Prod036 - 05.5 - SCEA Spiruline des Bauges'
    WHERE tax_ids_description = 'Prod036 - 5.5 - SCEA Spiruline des Bauges';
UPDATE sale_order_line
    SET tax_ids_description='Prod037 - 05.5 - Les jardins de la cour'
    WHERE tax_ids_description = 'Prod037 - 5.5 - Les jardins de la cour';
UPDATE sale_order_line
    SET tax_ids_description='Prod038 - 05.5 - La ferme des granges'
    WHERE tax_ids_description = 'Prod038 - 5.5 - La ferme des granges';
UPDATE sale_order_line
    SET tax_ids_description='Prod039 - 05.5 - les douceurs des Bauges'
    WHERE tax_ids_description = 'Prod039 - 5.5 - les douceurs des Bauges';
UPDATE sale_order_line
    SET tax_ids_description='Prod040 - 05.5 - les bau''jardins'
    WHERE tax_ids_description = 'Prod040 - 5.5 - les bau''jardins';
UPDATE sale_order_line
    SET tax_ids_description='Prod041 - 05.5 - GAEC La marmotte en Bauges'
    WHERE tax_ids_description = 'Prod041 - 5.5 - GAEC La marmotte en Bauges';
UPDATE sale_order_line
    SET tax_ids_description='Prod043 - 05.5 - GAEC du Val Gelon'
    WHERE tax_ids_description = 'Prod043 - 5.5% - GAEC du Val Gelon ';
UPDATE sale_order_line
    SET tax_ids_description='Prod045 - 05.5 - Corbier SAS'
    WHERE tax_ids_description = 'Prod045 - 5.5 - Corbier SAS';
UPDATE sale_order_line
    SET tax_ids_description='Prod046 - 05.5 - GAEC la ferme du Caban'
    WHERE tax_ids_description = ' Prod046 - 5.5 - GAEC la ferme du Caban';
UPDATE sale_order_line
    SET tax_ids_description='Prod047 - 05.5 - La panacée des bauges '
    WHERE tax_ids_description = 'Prod047 - 5,5% - La panacée des bauges ';
UPDATE sale_order_line
    SET tax_ids_description='Prod001 - 05.5 - Claude Buffet'
    WHERE tax_ids_description = 'PZI - Prod001 - 5.5 - Claude Buffet';
UPDATE sale_order_line
    SET tax_ids_description='Prod002 - 05.5 - Frédéric Bozon'
    WHERE tax_ids_description = 'PZI - Prod002 - 5.5 - Frédéric Bozon';
UPDATE sale_order_line
    SET tax_ids_description='Prod003 - 20.0 NON ASSUJETTI - Herba Augustus'
    WHERE tax_ids_description = 'PZI - Prod003 - 20,0% - NON ASSUJETTI - Herba Augustus';
UPDATE sale_order_line
    SET tax_ids_description='Prod003 - 05.5 NON ASSUJETTI - Herba Augustus'
    WHERE tax_ids_description = 'PZI - Prod003 - 5,5% - NON ASSUJETTI - Herba Augustus';
UPDATE sale_order_line
    SET tax_ids_description='Prod004 - 20.0 NON ASSUJETTI - Frédérique LEGENDRE'
    WHERE tax_ids_description = 'PZI - Prod004 - 20,0% - NON ASSUJETTI - Frédérique LEGENDRE';
UPDATE sale_order_line
    SET tax_ids_description='Prod004 - 05.5 NON ASSUJETTI -  Frédérique LEGENDRE'
    WHERE tax_ids_description = 'PZI - Prod004 - 5,5% - NON ASSUJETTI -  Frédérique LEGENDRE';
UPDATE sale_order_line
    SET tax_ids_description='Prod007 - 05.5 - Caracoles de suc'
    WHERE tax_ids_description = 'Prod007 - 05.5 Caracoles de suc';
UPDATE sale_order_line
    SET tax_ids_description='Prod010 - 05.5 - La ferme des cailloux'
    WHERE tax_ids_description = 'Prod010 - 05.5 La ferme des cailloux';
UPDATE sale_order_line
    SET tax_ids_description='Prod012 - 05.5 NON ASSUJETTI - Nadège BOGEY'
    WHERE tax_ids_description = 'Prod012 - 05.5  NON ASSUJETTI - Nadège BOGEY';
UPDATE sale_order_line
    SET tax_ids_description='Prod015 - 20.0 - Trescol ferrier e bouit'
    WHERE tax_ids_description = 'Prod015 - 20.0 Trescol ferrier e bouit';
UPDATE sale_order_line
    SET tax_ids_description='Prod021 - 05.5 - Ferme de Pisse Renard'
    WHERE tax_ids_description = 'Prod021 - 05.5 -  Ferme de Pisse Renard';
UPDATE sale_order_line
    SET tax_ids_description='Prod022 - 05.5 - Les Bazins'
    WHERE tax_ids_description = 'Prod022 - 05.5 -  Les Bazins';
UPDATE sale_order_line
    SET tax_ids_description='Prod027 - 05.5 NON ASSUJETTI - A l heure du thé'
    WHERE tax_ids_description = 'Prod027 - 05.5 NON ASSUJETTI - A l heure du thé';
UPDATE sale_order_line
    SET tax_ids_description='Prod030 - 05.5 - BLASIUS Stefanie'
    WHERE tax_ids_description = 'Prod030 - 05.5 -  BLASIUS Stefanie';
UPDATE sale_order_line
    SET tax_ids_description='Prod031 - 05.5 - A l aunée des bois'
    WHERE tax_ids_description = 'Prod031 - 05.5 -  A l aunée des bois';
UPDATE sale_order_line
    SET tax_ids_description='Prod031 - 05.5 NON ASSUJETTI - A l aunée des bois'
    WHERE tax_ids_description = 'Prod031 - 05.5 NON ASSUJETTI - A l aunée des bois';
UPDATE sale_order_line
    SET tax_ids_description='Prod033 - 05.5 - L abeille Verte'
    WHERE tax_ids_description = 'Prod033 - 5.5 - L abeille Verte';
UPDATE sale_order_line
    SET tax_ids_description='Prod043 - 05.5 - GAEC du Val Gelon'
    WHERE tax_ids_description = 'Prod043 - 5.5% - GAEC du Val Gelon';
UPDATE sale_order_line
    SET tax_ids_description='Prod047 - 05.5 - La panacée des bauges'
    WHERE tax_ids_description = 'Prod047 - 5,5% - La panacée des bauges';
UPDATE sale_order_line
    SET tax_ids_description='Prod009 - 05.5 - La Ferme de peyrorelles'
    WHERE tax_ids_description = 'Prod009 - 05.5 La Ferme de peyrorelles';
UPDATE sale_order_line
    SET tax_ids_description='Prod011 - 20.0 NON ASSUJETTI - La poterie du passe velou'
    WHERE tax_ids_description = 'Prod011 - 20.0 NON ASSUJETTI La poterie du passe velou';
UPDATE sale_order_line
    SET tax_ids_description='Prod012 - 20.0 NON ASSUJETTI - L''arbre ô essences'
    WHERE tax_ids_description = 'Prod012 - 20.0 NON ASSUJETTI L''arbre ô essences';
UPDATE sale_order_line
    SET tax_ids_description='Prod014 - 05.5 NON ASSUJETTI - La ferme de veyrassac'
    WHERE tax_ids_description = 'Prod014 - 05.5 NON ASSUJETTI La ferme de veyrassac';
UPDATE sale_order_line
    SET tax_ids_description='Prod001 - 05.5 - Ghee Artisanal'
    WHERE tax_ids_description = 'TVA 05,5% - Ghee Artisanal';
UPDATE sale_order_line
    SET tax_ids_description='Prod032 - 20.0 NON ASSUJETTI - Karene et ses poteries'
    WHERE tax_ids_description = 'Prod032 - 20.0 - NON ASSUJETTI - Karene et ses poteries';
UPDATE sale_order_line
    SET tax_ids_description='TVA-VT-10.0-TTC'
    WHERE tax_ids_description = 'VT-10.0-TTC ';
UPDATE sale_order_line
    SET tax_ids_description='TVA-VT-05.5-TTC'
    WHERE tax_ids_description = 'VT-5.5-TTC ';
UPDATE sale_order_line
    SET tax_ids_description='TVA-VT-05.5-TTC'
    WHERE tax_ids_description = 'VT-5.5-TTC';
UPDATE sale_order_line
    SET tax_ids_description='TVA-VT-05.5-HT'
    WHERE tax_ids_description = 'VT-5.5-HT';
UPDATE sale_order_line
    SET tax_ids_description='TVA-VT-02.1-TTC'
    WHERE tax_ids_description = 'VT-2.1-TTC';
UPDATE sale_order_line
    SET tax_ids_description='TVA-VT-02.1-HT'
    WHERE tax_ids_description = 'TVA-VT-2.1-HT';
UPDATE sale_order_line
    SET tax_ids_description='TVA-VT-02.1-TTC'
    WHERE tax_ids_description = 'TVA-VT-2.1-TTC';
UPDATE sale_order_line
    SET tax_ids_description='TVA-VT-05.5-HT'
    WHERE tax_ids_description = 'TVA-VT-5.5-HT';
UPDATE sale_order_line
    SET tax_ids_description='TVA-VT-05.5-TTC'
    WHERE tax_ids_description = 'TVA-VT-5.5-TTC';
UPDATE sale_order_line
    SET tax_ids_description='TVA-VT-10.0-HT'
    WHERE tax_ids_description = 'VT-10.0-HT';
UPDATE sale_order_line
    SET tax_ids_description='TVA-VT-10.0-TTC'
    WHERE tax_ids_description = 'VT-10.0-TTC';
UPDATE sale_order_line
    SET tax_ids_description='TVA-VT-20.0-HT'
    WHERE tax_ids_description = 'VT-20.0-HT';
UPDATE sale_order_line
    SET tax_ids_description='TVA-VT-20.0-TTC'
    WHERE tax_ids_description = 'VT-20.0-TTC';






update sale_order_line
    set price_total = round(price_unit * product_uom_qty, 2),
    price_subtotal = round(price_unit * product_uom_qty, 2)
    where tax_ids_description ilike '%NON ASSUJETTI%' or
    tax_ids_description = 'TVA-VT-UE-0' or
    tax_ids_description is null;

update sale_order_line
    set price_total = round(price_unit * product_uom_qty, 2),
    price_subtotal = round(
        (price_unit * product_uom_qty) / 1.021
    , 2)
    WHERE tax_ids_description = 'TVA-VT-02.1-TTC';

update sale_order_line
    set price_total = round(price_unit * product_uom_qty, 2),
    price_subtotal = round(
        (price_unit * product_uom_qty) / 1.055
    , 2)
    WHERE tax_ids_description = 'TVA-VT-05.5-TTC'
    or tax_ids_description ilike '%- 05.5 -%';

update sale_order_line
    set price_total = round(price_unit * product_uom_qty, 2),
    price_subtotal = round(
        (price_unit * product_uom_qty) / 1.10
    , 2)
    WHERE tax_ids_description = 'TVA-VT-10.0-TTC';

update sale_order_line
    set price_total = round(price_unit * product_uom_qty, 2),
    price_subtotal = round(
        (price_unit * product_uom_qty) / 1.196
    , 2)
    WHERE tax_ids_description = 'TVA-VT-19.6-TTC'
    or tax_ids_description ilike '%- 19.6 -%';

update sale_order_line
    set price_total = round(price_unit * product_uom_qty, 2),
    price_subtotal = round(
        (price_unit * product_uom_qty) / 1.2
    , 2)
    WHERE tax_ids_description = 'TVA-VT-20.0-TTC'
    or tax_ids_description ilike '%- 20.0 -%';


update sale_order_line
    set price_total = round(
        price_unit * product_uom_qty * 1.021
    , 2),
    price_subtotal = round(price_unit * product_uom_qty, 2)
    WHERE tax_ids_description = 'TVA-VT-02.1-HT';

update sale_order_line
    set price_total = round(
        price_unit * product_uom_qty * 1.055
    , 2),
    price_subtotal = round(price_unit * product_uom_qty, 2)
    WHERE tax_ids_description = 'TVA-VT-05.5-HT';

update sale_order_line
    set price_total = round(
        price_unit * product_uom_qty * 1.10
    , 2),
    price_subtotal = round(price_unit * product_uom_qty, 2)
    WHERE tax_ids_description = 'TVA-VT-10.0-HT';

update sale_order_line
    set price_total = round(
        price_unit * product_uom_qty * 1.196
    , 2),
    price_subtotal = round(price_unit * product_uom_qty, 2)
    WHERE tax_ids_description = 'TVA-VT-19.6-HT';

update sale_order_line
    set price_total = round(
        price_unit * product_uom_qty * 1.20
    , 2),
    price_subtotal = round(price_unit * product_uom_qty, 2)
    WHERE tax_ids_description = 'TVA-VT-20.0-HT';
