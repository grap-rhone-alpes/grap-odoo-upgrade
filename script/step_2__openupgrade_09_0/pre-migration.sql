-- ----------------------------------------------------------------------------
-- BASE
-- ----------------------------------------------------------------------------

-- [PREVENT UNINSTALLATION]
-- prevent that the uninstallation of report_webkit in V10 remove recurring_consignment
DELETE
FROM ir_module_module_dependency
    WHERE
    name = 'report_webkit'
    AND module_id = (select id from ir_module_module where name = 'recurring_consignment');

-- prevent the unistallation of grap_change_views and grap_qweb_report, that depends on
-- a lot of modules that will be uninstalled during the migration process.
DELETE
FROM ir_module_module_dependency
    WHERE name != 'account'
    AND module_id in (select id from ir_module_module where name in ('grap_change_views', 'grap_qweb_report'));

-- prevent the unistallation of sale_eshop, for mystic reasons.
DELETE
FROM ir_module_module_dependency
    WHERE name != 'sale'
    AND module_id in (select id from ir_module_module where name in ('sale_eshop'));

DELETE
FROM ir_module_module_dependency
    WHERE name = 'account_accountant'
    AND module_id in (select id from ir_module_module where name in ('account_export_ebp'));


    -- UPDATE ir_module_module_dependency
    -- SET name = 'sale'
    -- WHERE name = 'sale_order_line_price_subtotal_gross'
    -- AND module_id = (
    --     SELECT id
    --     FROM ir_module_module
    --     WHERE name = 'sale_eshop'
    -- );


-- [General]
-- Drop the uninstalled module product_barcode_generator, because
-- openupgrade try to rename it into barcodes_generator_product
-- that has been backported by GRAP in v8.
DELETE
FROM ir_module_module
WHERE name = 'product_barcode_generator';

DELETE
FROM ir_model_data
WHERE name = 'module_product_barcode_generator';

DELETE from ir_filters where model_id ilike 'x_bi_sql_view%';

-- ----------------------------------------------------------------------------
-- PRODUCT
-- ----------------------------------------------------------------------------

-- [product]
-- Drop obsolete constraint on product_supplierinfo created on
-- antiguous version, and not dropped.
ALTER TABLE product_supplierinfo
DROP CONSTRAINT product_supplierinfo_psi_product_name_uniq;

-- ----------------------------------------------------------------------------
-- Account
-- ----------------------------------------------------------------------------
-- [account_invoice_line_sequence]
-- prepopulate sequence2
-- prepopulate max_line_sequence
-- Save 20 minutes !
ALTER TABLE account_invoice_line add column sequence2 integer;

UPDATE account_invoice_line
SET sequence2=sequence;

ALTER TABLE account_invoice add column max_line_sequence integer;

UPDATE account_invoice ai
SET max_line_sequence = tmp.max_sequence
FROM (
SELECT invoice_id, max(sequence) + 1 as max_sequence
FROM account_invoice_line
GROUP BY invoice_id) as tmp
WHERE ai.id = tmp.invoice_id;

UPDATE account_invoice ai
SET max_line_sequence = 1
WHERE ai.id not in (select distinct(invoice_id) from account_invoice_line);

-- ----------------------------------------------------------------------------
-- SALE
-- ----------------------------------------------------------------------------

-- [sale] clean bad uos_id on account.invoice.line (incorrect category)
UPDATE account_invoice_line ail
SET uos_id = sol.product_uom
FROM sale_order_line_invoice_rel rel,
    sale_order_line sol,
    product_uom pu_sol,
    product_uom pu_ail
WHERE sol.id = rel.order_line_id
AND ail.id = rel.invoice_id
AND pu_sol.id = sol.product_uom
AND pu_ail.id = ail.uos_id
AND pu_sol.category_id != pu_ail.category_id;

-- [sale] clean bad uos_id on account.invoice.line (no uos on invoice.line)
UPDATE account_invoice_line ail
SET uos_id = sol.product_uom
FROM sale_order_line_invoice_rel rel,
    sale_order_line sol
WHERE sol.id = rel.order_line_id
AND ail.id = rel.invoice_id
AND ail.uos_id is null;

-- [sale_order_dates] missing column on sale_order
alter table sale_order add column effective_date date;

update sale_order so
    set effective_date = tmp.min_date
from (
select so.id, min(sp.date) as min_date from sale_order so
inner join procurement_group pg on pg.id = so.procurement_group_id
inner join stock_picking sp on pg.id = sp.group_id
group by so.id) tmp
where so.id = tmp.id;


-- ----------------------------------------------------------------------------
-- PURCHASE
-- ----------------------------------------------------------------------------

-- [purchase] fix very weird data
UPDATE stock_move SET product_id = 474 WHERE id = 18026;

-- [purchase] Fix bad product_id on purchase.order.line (inconsistency with stock move)
UPDATE purchase_order_line pol
    SET product_id = sm.product_id
FROM stock_move sm
WHERE sm.purchase_line_id = pol.id
AND pol.product_id != sm.product_id;

-- [purchase] fix incorrect product_uom on purchase_order_line (regarding product setting)
UPDATE purchase_order_line pol
    SET product_uom = pt.uom_id
FROM product_product pp, product_template pt,
    product_uom pt_uom, product_uom pol_uom
WHERE pp.id = pol.product_id
AND pt.id = pp.product_tmpl_id
AND pt_uom.id = pt.uom_id
AND pol_uom.id = pol.product_uom
AND pt_uom.category_id != pol_uom.category_id;


-- [purchase] clean bad uos_id on account.invoice.line (incorrect category)
UPDATE account_invoice_line ail
SET uos_id = pol.product_uom
FROM purchase_order_line_invoice_rel rel,
    purchase_order_line pol,
    product_uom pu_pol,
    product_uom pu_ail
WHERE pol.id = rel.order_line_id
AND ail.id = rel.invoice_id
AND pu_pol.id = pol.product_uom
AND pu_ail.id = ail.uos_id
AND pu_pol.category_id != pu_ail.category_id;

-- [purchase] clean bad uos_id on account.invoice.line (no uos on invoice.line)
UPDATE account_invoice_line ail
SET uos_id = pol.product_uom
FROM purchase_order_line_invoice_rel rel,
    purchase_order_line pol
WHERE pol.id = rel.order_line_id
AND ail.id = rel.invoice_id
AND ail.uos_id is null;

-- [purchase] some invoices lines are inconsistent with the
-- linke purchase order lines.
-- deleting the link, because it is generating errors in the
-- first request of the function set_po_line_computed_rest()
UPDATE account_invoice_line ail
SET purchase_line_id = null
FROM purchase_order_line pol
WHERE pol.id = ail.purchase_line_id
AND ail.product_id != pol.product_id;


-- ----------------------------------------------------------------------------
-- VARIOUS RENAME, DUE TO REFACTORING BETWEEEN V8.0 AND V12.0
-- ----------------------------------------------------------------------------

-- [product_fiscal_company] product_template.administrative_ok --> product_template.cae_administrative_ok
ALTER TABLE product_template
RENAME COLUMN administrative_ok TO cae_administrative_ok;

-- [sale_food] product_product.is_food --> product_product.is_alimentary
ALTER TABLE product_product
RENAME COLUMN is_food TO is_alimentary;


-- ----------------------------------------------------------------------------
-- Always begin with the same company for the admin users
-- ----------------------------------------------------------------------------

update res_users set company_id = (
    select id from res_company where code ='DEL'
)
where id = 1;

update res_partner p set company_id = (
    select id from res_company where code ='DEL'
)
from res_users u
where u.id = 1 and u.partner_id = p.id;
