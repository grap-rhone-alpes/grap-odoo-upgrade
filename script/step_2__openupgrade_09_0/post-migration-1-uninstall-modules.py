_UNINSTALL_MODULES = [
    # Odoo Core deleted modules
    # Ref : https://github.com/OCA/OpenUpgrade/blob/9.0/openerp/
    # openupgrade/doc/source/modules80-90.rst
    "edi",
    "l10n_fr_rib",
    "stock_invoice_directly",
]


def main(self, step):
    self._uninstall_modules(_UNINSTALL_MODULES)
