_UNINSTALL_MODULES = [
    # Odoo Core deleted modules
    # Ref : https://github.com/OCA/OpenUpgrade/blob/10.0/odoo/
    # openupgrade/doc/source/modules90-100.rst
    "report_webkit",
    "warning",
]


def main(self, step):
    self._uninstall_modules(_UNINSTALL_MODULES)
