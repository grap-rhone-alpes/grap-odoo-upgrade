-- [product]
-- fix pricelist.item where base is standard_price_tax_included
UPDATE product_pricelist_item ppi
SET base = ppt.field
FROM product_price_type AS ppt
WHERE ppt.id = openupgrade_legacy_9_0_base
AND ppt.field = 'standard_price_tax_included';

-- [stock]
-- prepopulate stock_move.scrapped
-- TODO, Share in OCA

alter table stock_move add column scrapped boolean default false;

update stock_move sm
    set scrapped = sl.scrap_location
from stock_location sl
where sl.id = sm.location_dest_id
and sl.scrap_location = true;

-- [stock_picking_line_sequence]
-- prepopulate sequence2
alter table stock_move add column sequence2 integer;

update stock_move
    set sequence2=sequence;

-- [ir.rule]
-- changing manually domain of a the rule stock.stock_picking_type_rule
-- previous domain doesn't work anymore. (based on disappeared field 'company_id')
-- Note : could be great to make a PR against OpenUpgrade
update ir_rule set domain_force='["|", ("warehouse_id", "=", False), "|",("warehouse_id.company_id","=",False),("warehouse_id.company_id","child_of",[user.company_id.id])]'
where id in (select res_id from ir_model_data where module = 'stock' and name='stock_picking_type_rule');


-- ----------------------------------------------------------------------------
-- Always begin with the same company for the admin users
-- ----------------------------------------------------------------------------

update res_users set company_id = (
    select id from res_company where code ='DEL'
)
where id = 1;

update res_partner p set company_id = (
    select id from res_company where code ='DEL'
)
from res_users u
where u.id = 1 and u.partner_id = p.id;
