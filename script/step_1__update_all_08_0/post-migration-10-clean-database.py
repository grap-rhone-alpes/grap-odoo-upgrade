def main(self, step):

    # Fix standard sequence
    if self._is_model_present("bi.sql.view"):
        journals = self.odoo_local.AccountJournal.browse(
            self.odoo_local.AccountJournal.search())
        sequence_list = []
        for journal in journals:
            # Add main sequence
            if journal.sequence_id.implementation == 'standard':
                sequence_list.append(journal.sequence_id)
            # Add subsequence
            for sequence_fiscalyear in journal.sequence_id.fiscal_ids:
                if sequence_fiscalyear.sequence_id.implementation == 'standard':
                    sequence_list.append(sequence_fiscalyear.sequence_id)

        for sequence in sequence_list:
            number_next = sequence.number_next_actual
            self.logger_info(
                "Sequence %s (prefix:  %s) set to 'no_gap'."
                " Next number will be %d." % (
                    sequence.name, sequence.name, number_next))
            sequence.implementation = 'no_gap'
            sequence.number_next_actual = number_next

    # Clean Bi sql editor
    if self._is_model_present("bi.sql.view"):
        view_ids = self.odoo_local.BiSqlView.search(
            [("state", "!=", "draft")], order='id desc')
        for view in self.odoo_local.BiSqlView.browse(view_ids):
            try:
                self.logger_info(
                    "Set bi.sql.view '%s' (%s) to draft" % (
                        view.name, view.model_name))
                view.button_set_draft()
            except Exception:
                self.logger_error(
                    "Unable to set to draft the bi.sql.view '%s (%s)" % (
                        view.name, view.model_name))

        model_names = [
            "x_bi_sql_view.technical_product_product",
            "x_bi_sql_view.bi_regular_sale_product",
            "x_bi_sql_view.technical_purchase_order_line",
            "x_bi_sql_view.technical_sale_pos_invoice_line",
            "x_bi_sql_view.bi_regular_sale_product_all",
        ]
        models = self.odoo_local.IrModel.browse(
            [("model", "in", model_names)])
        for model in models:
            self.logger_info(
                "Unlinking extra obsolete models %s" % (model.model))
            model.unlink()

    # clean product template, removing template without variants
    # to avoid bad product.price.history generation in V12
    if self._is_model_present("product.template"):

        result = self.odoo_local.ProductProduct.search_read(
            ['|', ('active', '=', True), ('active', '=', False)],
            ['product_tmpl_id']
        )
        correct_template_ids = [x["product_tmpl_id"][0] for x in result]
        incorrect_template_ids = self.odoo_local.ProductTemplate.search(
            [('id', 'not in', correct_template_ids)]
        )
        count = 0
        templates = self.odoo_local.ProductTemplate.browse(incorrect_template_ids)
        templates.write({"available_in_pos": False})

        for template in templates:
            count += 1
            self.logger_info(
                "%d / %d: Unlinking template '%s' without variant."
                " (company %s)" % (
                    count, len(incorrect_template_ids),
                    template.name, template.company_id.name,
                ))
            template.unlink()

    # Disable Mass Linkings
    if self._is_model_present("mass.linking"):
        mass_linkings_ids = self.odoo_local.MassLinking.search(
            [("action_id", "!=", False)])

        mass_linkings = self.odoo_local.MassLinking.browse(mass_linkings_ids)
        for mass_linking in mass_linkings:
            self.logger_info(
                "Unlinking mass.linking %s" % (mass_linking.name)
            )
            mass_linking.disable_mass_operation()

    # Disable Mass actions
    if self._is_model_present("mass.action"):
        mass_action_ids = self.odoo_local.MassAction.search(
            [("action_id", "!=", False)])

        mass_actions = self.odoo_local.MassAction.browse(mass_action_ids)
        for mass_action in mass_actions:
            self.logger_info(
                "Unlinking mass.action %s" % (mass_action.name)
            )
            mass_action.disable_mass_operation()

    # # Disable Mass Sort
    if self._is_model_present("mass.sort.config"):
        mass_sort_ids = self.odoo_local.MassSortConfig.search(
            [("action_id", "!=", False)])

        mass_sorts = self.odoo_local.MassSortConfig.browse(mass_sort_ids)
        for mass_sort in mass_sorts:
            self.logger_info(
                "Unlinking mass.sort.config %s" % (mass_sort.name)
            )
            mass_sort.unlink()
