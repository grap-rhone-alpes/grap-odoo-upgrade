_INSTALL_MODULES = [
    # We install warning module because, otherwise,
    # the migration from 9.0 to 10.0 will fail because
    # warning introduce res.partner fields, that move then
    # in account module
    "warning",

    # We install triple_discount module to avoid
    # to crash, in demo database, if the modules are not installed
    # due to a dirty patch done in the migration of product module
    # in V9, to recover discount, discount2, discount3 fields
    # in product.supplierinfo
    "product_supplierinfo_triple_discount",
]


def main(self, step):
    self._install_modules(_INSTALL_MODULES)
