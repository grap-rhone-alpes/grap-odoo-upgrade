_UNINSTALL_MODULES_TO_REINSTALL = [
    # Reinstall in V12, because the migration fail with this module installed
    "server_environment",
    # Reinstall in V12, because the migration fail with this module installed
    "bi_sql_editor",
    # Reinstall in V12, because the migration fail with this module installed
    "web_dashboard_tile",
    # Reinstall in V12, because the migration fail with this module installed
    "account_move_change_number",
    # We reinstall in 12.0 web_switch_context_warning
    "web_switch_company_warning",
    # We reinstall in 12.0 pos_order_mgmt
    "pos_done_order_load",
    # We reinstall in V12 pos_default_empty_image
    "pos_product_without_image",
    # We reinstall in V12 mail_send_copy
    "mail_send_bcc",
    # We reinstall in V12 server_action_navigate
    "mass_linking",
    # We reinstinall in V12 server_action_sort
    "mass_sorting",
    # We reinstall in V12 pos_payment_change
    "pos_change_payment",
    # We reinstall in V12 grap_custom_account_invoice_workflow
    "invoice_verified_state",
    # We reinstall in v12 mobile_kiosk_purchase
    "mobile_app_purchase",
    # We reinstall in v12 mobile_kiosk_inventory
    "mobile_app_inventory",
    # We uninstall pos_picking_delayed to avoid
    # to install, by dependency, in V11 step, the module queue_job
    # that is not designed as the V12 model.
    # we will reinstall it in the V12.
    "pos_picking_delayed",
    # We reinstall in V12 technical_partner_access
    "users_partners_access",
]

_UNINSTALL_MODULES_DEFINIVE = [
    # OCA/account-invoicing
    "account_invoice_merge",
    "account_invoice_merge_purchase",
    "sale_order_line_price_subtotal_gross",

    # OCA / pos
    "pos_order_pricelist_change",

    # OCA/product-attribute
    "product_supplierinfo_tree_price_info",

    # OCA/purchase-workflow
    "purchase_supplier_rounding_method",
    "purchase_supplier_rounding_method_triple_discount",

    # OCA / server-tools

    # 24/06/2020 : j'ai commenté la ligne suivante. pas sûr de comprendre
    # pourquoi on devait désinstaller ce module.
    # en tout cas, ca shoote account_invoice_pricelist
    # "base_view_inheritance_extension",

    # TODO, FIXME unable to uninstall this fucking module
    # "module_uninstall_check",

    # OCA/stock-logistic-workflow
    "stock_disable_barcode_interface",

    # OCA/reporting-engine
    "report_custom_filename",

    # OCA/web
    "web_ckeditor4",
    "web_clean_navbar",
    "web_easy_switch_company",
    "web_hide_db_manager_link",
    "web_invalid_tab",
    "web_graph_sort",
    "web_shortcuts",
    "web_group_expand",
    "web_offline_warning",
    "web_graph_improved",

    # OCA / webkit-tools
    "base_headers_webkit",

    # odoo-addons-intercompany-trade
    "intercompany_trade_purchase",

    # grap-odoo-incubator
    "mass_action",                          # useless with server.actions model
    "mass_merging_content",                 # Unused in V8.0
    "mass_operation_abstract",
    "product_categ_search_complete_name",
    "product_supplierinfo_quick_edit",
    "product_ean_duplicates",
    "product_ean13_image",
    "pos_confirm_window_close",
    "pos_copy_order_opened_session",
    "pos_limit_unknown_barcode",
    "pos_optional_printing",
    "pos_payment_limit",
    "pos_payment_usability",
    "stock_easy_valuation",
    "pos_draft_move",

    # grap-odoo-business
    "sale_line_change_custom",
    "stock_picking_type_image",
    "stock_picking_mass_change",
    "stock_picking_quick_edit",

    # grap-odoo-custom
    "grap_product_handle_ean8",

    # grap-odoo-backport
    "sale_order_mass_action",

    # odoo-addons-grap
    "account_statement_reconciliation",
]

_UNINSTALL_MODULE_TEMPORARY = [
    # #####################
    # grap-odoo-custom
    # #####################
    "grap_change_ir_values",

    # #####################
    # Others
    # #####################
    "pos_picking_load_partner_name",

]


def main(self, step):
    # prevent the uninstallation of sale_eshop, grap_change_views,
    # grap_qweb_report
    request = """
    DELETE
    FROM ir_module_module_dependency
        WHERE
        name != 'account'
        AND module_id in (
            SELECT id
            FROM ir_module_module
            WHERE name in ('grap_change_views', 'grap_qweb_report')
        );

    UPDATE ir_module_module_dependency
    SET name = 'sale'
    WHERE name = 'sale_order_line_price_subtotal_gross'
    AND module_id in (
        SELECT id
        FROM ir_module_module
        WHERE name in ('sale_eshop')
    );
    """

    self._uninstall_modules(
        _UNINSTALL_MODULES_TO_REINSTALL,
        pre_sql_request=request)
    self._uninstall_modules(
        _UNINSTALL_MODULES_DEFINIVE,
        pre_sql_request=request)
    self._uninstall_modules(
        _UNINSTALL_MODULE_TEMPORARY,
        pre_sql_request=request)
