def main(self, step):
    return

    for company in self.browse_by_search(self.odoo_local.ResCompany):
        if company.fiscal_type == "fiscal_child":
            continue
        self.odoo_user.company_id = company.id

        fiscal_years = self.browse_by_search(self.odoo_local.AccountFiscalyear)
        for fiscal_year in fiscal_years:
            period_ids = self.odoo_local.AccountPeriod.search([
                ("fiscalyear_id", "=", fiscal_year.id)])
            moves = self.browse_by_search(self.odoo_local.AccountMove, [
                ("company_id", "=", company.id),
                ("period_id", "in", period_ids),
                ("ebp_export_id", "=", False)
            ])
            if moves:
                self.logger_info(
                    "%s - Handling %d moves"
                    " for fiscal year %s" % (
                        company.code, len(moves), fiscal_year.name))

                try:
                    self.odoo_local.context = {
                        "active_ids": [x.id for x in moves]
                    }
                    wizard = self.odoo_local.WizardEbpExport.create({
                        "fiscalyear_id": fiscal_year.id,
                    })
                    wizard.button_export()
                except Exception:
                    self.logger_warning("An error occured !")
