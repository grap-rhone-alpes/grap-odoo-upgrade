-- [base]
-- disable all ir_cron
UPDATE ir_cron set active = false;

-- Delete obsolete ir_act_window
DELETE FROM ir_act_window
WHERE res_model in (
    'document.page',
    'pos.cash.controls',
    'pos.receipt'
);

-- Fix res_partner_bank without partner_id
UPDATE res_partner_bank rpb
    SET partner_id = rc.partner_id
FROM res_company rc
WHERE rc.id = rpb.company_id
    AND rpb.partner_id is NULL;

-- Delete bad res.bank
delete from res_bank where id = 9;


-- Delete 3PP res_partner_category
DELETE from res_partner_category;

-- Delete custom partner title
DELETE from res_partner_title
where id not in (
    select res_id
    from ir_model_data
    where model = 'res.partner.title' and module = 'base'
);

-- [product]
update product_product set ean13 = null where ean13 = '';

-- clean weights values, before the deletion of weight net that will be placed in weight field
update product_template set weight = 0.0000;
-- select weight_net, count(*) from product_template group by weight_net order by weight_net;

-- [product_to_scale_bizerba]
-- weight_net field will be dropped. we move scale system lines from weight net to weight
update product_scale_system_product_line
    set field_id = (
        select id
        from ir_model_fields
        where model = 'product.product' and name = 'weight'
    )
    where field_id = (
        select id
        from ir_model_fields
        where model = 'product.template' and name = 'weight_net'
    );

-- [mail]
-- delete obsolete mail.message for deleted models
DELETE FROM mail_message WHERE model not IN (SELECT model FROM ir_model);

-- [bi_sql_editor]
-- Drop tile that depends on bi_sql_editor views, because the module will be uninstalled in the 080_post.py
delete from tile_tile
WHERE model_id in (
    SELECT id from ir_model WHERE model ilike 'x_bi_sql_view%'
);

--  [account] fix bad account_invoice_line
-- Check request
-- select count(*), ail.account_id, aa.code from account_invoice ai inner join account_invoice_line ail on ail.invoice_id = ai.id inner join account_account aa on aa.id = ail.account_id and aa.type = 'view' group by ail.account_id, aa.code;

update account_invoice_line set account_id = 2576 where account_id = 371;
update account_invoice_line set account_id = 2603 where account_id = 613;
update account_invoice_line set account_id = 627 where account_id = 621;
update account_invoice_line set account_id = 641 where account_id = 639;
update account_invoice_line set account_id = 756 where account_id = 755;
update account_invoice_line set account_id = 2611 where account_id = 766;
update account_invoice_line set account_id = 874 where account_id = 868;
update account_invoice_line set account_id = 2604 where account_id = 1039;


-- [account] fix bad account_invoice_tax
-- Check Request
-- select count(*) from account_invoice_tax where account_id in (select id from account_account where type = 'view');
delete from account_invoice_tax
    where account_id in (
        select id from account_account
        where type = 'view')
    and amount = 0.0;

-- [account] fix bad acount_tax
update account_tax set account_paid_id = null
    where account_paid_id in (
        select id from account_account where type = 'view');
update account_tax set account_collected_id = null
    where account_collected_id in (
        select id from account_account where type = 'view');

-- [sale] Delete a bad value for internal trade VEH -> CDA
DELETE from sale_order_tax where order_line_id = 30091;
update sale_order_line
    set tax_ids_description = null
    where tax_ids_description = 'TVA 0%';

-- [point_of_sale] clean bad values in pos_categ_id
update product_template
    set pos_categ_id = null
    where pos_categ_id not in (select id from pos_category);

-- [point_of_sale] affect a pos.order of EDC to a pos.session
update pos_order set session_id = 1251 where id = 141465;

-- [point_of_sale] / [pos_cash_move_reason] backup product settings
ALTER TABLE product_template add column grap_migration_is_income_reason boolean;
ALTER TABLE product_template add column grap_migration_is_expense_reason boolean;
UPDATE product_template
    SET grap_migration_is_income_reason = income_pdt,
    grap_migration_is_expense_reason = expense_pdt;

-- [translation] (grap_change_translation)
-- fix now untranslatable field account.product.fiscal.classification,name
update  account_product_fiscal_classification apfc
    set name = it.value
from ir_translation it
WHERE it.res_id = apfc.id
AND it.name='account.product.fiscal.classification,name';

delete from ir_translation it where it.name='account.product.fiscal.classification,name';

-- ---------------------------------------
-- [recurring_consignment]
-- ---------------------------------------

-- prefill consignment_product_id field that moved from account_tax_code to account_tax

alter table account_tax
ADD column consignment_product_id integer;

update account_tax at
    set consignment_product_id = atc.consignment_product_id
    from account_tax_code atc
    where atc.id = at.base_code_id
    and atc.consignment_product_id is not null;



-- ---------------------------------------
-- [stock]
-- ---------------------------------------

-- add a column to see which stock_rule (previously procurement_rule)
-- are useless in V12.
ALTER TABLE procurement_rule add column come_from_v8 boolean;
UPDATE procurement_rule SET come_from_v8 = true;





-- ---------------------------------------
-- [account_export_ebp -> grap_account_export_ebp]
-- ---------------------------------------

ALTER TABLE account_account add column ebp_export_tax boolean;
UPDATE account_account
    set ebp_export_tax = ebp_export_tax_code;

ALTER TABLE account_tax add column ebp_suffix varchar;

update account_tax at
    set ebp_suffix = atc.ebp_suffix
    from account_tax_code atc
    where atc.id = at.base_code_id
    and atc.ebp_suffix is not null;

ALTER TABLE ebp_export add column fiscal_year_id integer;

UPDATE ebp_export
    set fiscal_year_id = fiscalyear_id;

-- ----------------------------------------------------------------------------
-- Always begin with the same company for the admin users
-- ----------------------------------------------------------------------------

update res_users set company_id = (
    select id from res_company where code ='DEL'
)
where id = 1;

update res_partner p set company_id = (
    select id from res_company where code ='DEL'
)
from res_users u
where u.id = 1 and u.partner_id = p.id;
