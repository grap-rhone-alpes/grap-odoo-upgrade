_TRANSCODING_MAPPING = [
    {       # A (nom)
        'name': 'name', 'type': 'char',
    }, {    # A (Alimentaire)
        'name': 'is_alimentary', 'type': 'boolean',
    }, {     # A (Titre resturant)
        'name': 'meal_voucher_ok', 'type': 'boolean',
    },
]


def main(self, step):
    product_category_generator = self._generator_from_csv(
        step, "product_category.csv", _TRANSCODING_MAPPING, 2)
    count = 0
    for category_obj in product_category_generator:
        count += 1
        categories = self.odoo_local.ProductCategory.browse(
            [("complete_name", "=", category_obj.name)]
        )
        if len(categories) == 0:
            self.logger_warning(
                "Product Category %s not found in the database."
                " Skipping." % (category_obj.name))
        else:
            self.logger_info(
                "Product Category %s set. (alimentary /"
                " meal voucher)" % (category_obj.name))
            categories.write({
                "is_alimentary": category_obj.is_alimentary,
                "meal_voucher_ok": category_obj.meal_voucher_ok,
            })
