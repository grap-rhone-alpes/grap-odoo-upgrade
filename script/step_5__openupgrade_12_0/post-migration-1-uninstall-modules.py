_UNINSTALL_MODULES = [
    # Odoo Core deleted modules
    # https://github.com/OCA/OpenUpgrade/blob/12.0/odoo/
    # openupgrade/doc/source/modules110-120.rst
    "warning",

    # Uninstall pos_pricelist, as it's now native
    "pos_pricelist",

    # Uninstall glue modules, installed after the migration
    "product_analytic_pos",
    "product_analytic_purchase",
    "account_invoice_merge_attachment",
    "account_financial_report_date_range",
    "account_financial_report_qweb",
]


def main(self, step):
    self._uninstall_modules(_UNINSTALL_MODULES)
