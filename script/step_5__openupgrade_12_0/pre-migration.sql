-- ---------------------------------------
-- [queue_job]
-- ---------------------------------------
delete
    from ir_cron
    where id in (
        select res_id
        from ir_model_data
        where name='ir_cron_queue_job_garbage_collector'
        and module='queue_job'
    );

delete
    from ir_model_data
    where name='ir_cron_queue_job_garbage_collector'
    and module='queue_job';


-- FIX drop two obsolete views (inheriting logging page)
delete from ir_ui_view
where id in (
    select res_id from ir_model_data
    where model='ir.ui.view' and (
        (module='grap_change_views' and name='login_layout') or
        (module='web_clean_navbar' and name='clean_navbar') or
        (module='web_hide_db_manager_link' and name='login_layout_no_db')
    )
);

-- ---------------------------------------
-- [product]
-- ---------------------------------------
-- Clean null barcode on product.product
 update product_product set barcode = null where barcode = '';

-- ---------------------------------------
-- [uom_uom]
-- ---------------------------------------
-- we have two distance product_uom_category in our database
-- deleting the one without xml_data defined
update product_uom
    set category_id = 6
    where category_id = 4;

delete from product_uom_categ
    where id = 4;

-- We have three reference uom for Unit category, disabling
-- the two one that dont' have xml_data defined
update product_uom
    set active = false
    where category_id = 1 and id != 1 and active = true;


-- ---------------------------------------
-- [product_notation]
-- ---------------------------------------
-- drop empty spider chart, to avoid to create
-- useless attachment
update product_product
set spider_chart_image = null where
local_notation = '0'
and social_notation = '0'
and social_notation = '0'
and packaging_notation = '0'
and organic_notation = '0'
and spider_chart_image is not null;

-- ---------------------------------------
-- [account_invoice_margin]
-- ---------------------------------------
-- Add margin_signed on account_invoice_line, and populate
ALTER TABLE account_invoice_line add column margin_signed numeric;

UPDATE account_invoice_line ail
SET margin_signed =
    CASE
        WHEN ai.type = 'out_invoice'    THEN    ail.margin
        WHEN ai.type = 'out_refund'     THEN    - ail.margin
        ELSE -9999999
    END
FROM account_invoice ai
WHERE ai.id = ail.invoice_id
AND ai.type in ('out_refund', 'out_invoice')
AND ail.margin != 0;

-- Add margin_signed on account_invoice, and populate
ALTER TABLE account_invoice add column margin_signed numeric;

UPDATE account_invoice ai
SET margin_signed = tmp.sum_margin_signed
FROM (
    SELECT ail.invoice_id, sum(margin_signed) as sum_margin_signed
    from account_invoice_line as ail
    group by ail.invoice_id
) as tmp
WHERE tmp.invoice_id = ai.id;

-- ---------------------------------------
-- [fiscal_company_xxx]
-- ---------------------------------------

ALTER TABLE res_company
RENAME COLUMN fiscal_company TO fiscal_company_id;

-- change fiscal_company >> fiscal_company_id
UPDATE ir_rule
set domain_force = substring(domain_force for position('.fiscal_company.' in domain_force))
        || 'fiscal_company_id'
        || substring(domain_force from 15 + position('.fiscal_company.' in domain_force))
where domain_force ilike '%.fiscal_company.%';

-- change fiscal_childs >> fiscal_child_ids
UPDATE ir_rule
set domain_force = substring(domain_force for position('.fiscal_childs]' in domain_force))
        || 'fiscal_child_ids'
        || substring(domain_force from 14 + position('.fiscal_childs]' in domain_force))
where domain_force ilike '%.fiscal_childs]%';

-- ---------------------------------------
-- [sale]
-- ---------------------------------------

ALTER TABLE sale_order ADD COLUMN currency_rate numeric;
UPDATE sale_order set currency_rate = 1.0;

-- ---------------------------------------
-- [point_of_sale]
-- ---------------------------------------

ALTER TABLE pos_order ADD COLUMN currency_rate numeric;
UPDATE pos_order set currency_rate = 1.0;


-- ---------------------------------------
-- [pos_payment_terminal]
-- ---------------------------------------

ALTER TABLE account_journal
    RENAME column payment_mode TO pos_terminal_payment_mode;

-- ---------------------------------------
-- [sale_recovery_moment -> stock_preparation_category]
-- ---------------------------------------

ALTER TABLE product_prepare_category
    RENAME TO stock_preparation_category;

alter sequence product_prepare_category_id_seq
    rename to stock_preparation_category_id_seq;

ALTER TABLE stock_move
RENAME COLUMN prepare_categ_id TO preparation_categ_id;

ALTER TABLE product_product
RENAME COLUMN prepare_categ_id TO preparation_categ_id;

-- ---------------------------------------
-- [base_view_inheritance_extension]
-- ---------------------------------------

-- request usefull only in a demo mode.
-- demo view of 'base_view_inheritance_extension' V8 / V9 are breaking
-- regular update in V12.
delete from ir_model_data
    where model='ir.ui.view'
    and module='base_view_inheritance_extension'
    and name in ('view_partner_form', 'view_partner_form_demo');



-- ---------------------------------------
-- [grap_qweb_report] -> product_print_category_food_report]
-- ---------------------------------------

update ir_model_data
    set module='product_print_category_food_report', name='category_pricetag_normal'
    where module='grap_qweb_report' and name='category_label_normal' and model='product.print.category';

update ir_model_data
    set module='product_print_category_food_report', name='category_pricetag_bulk_selling'
    where module='grap_qweb_report' and name='category_label_bulk_selling' and model='product.print.category';

update ir_model_data
    set module='product_print_category_food_report', name='category_pricetag_bulk_selling_applimage'
    where module='grap_qweb_report' and name='category_label_bulk_selling_applimage' and model='product.print.category';

update ir_model_data
    set module='product_print_category_food_report', name='category_pricetag_middle_square'
    where module='grap_qweb_report' and name='category_label_middle_square' and model='product.print.category';

update ir_model_data
    set module='product_print_category_food_report', name='category_pricetag_counter'
    where module='grap_qweb_report' and name='category_label_counter' and model='product.print.category';

-- ---------------------------------------
-- [grap_l10n_fr] (and l10n_fr)
-- ---------------------------------------
-- prevent error on V12, when grap_l10n_fr is reloaded, because some data
-- are conflicting with l10n_fr module. (grap_l10n_fr doesn't create new items,
-- but are now overloading existing ones)
update account_tax_template
    set name = name || ' (openupgrade)'
    where id in (
        select res_id from ir_model_data
        where module = 'grap_l10n_fr'
        and model='account.tax.template'
    )
    and name not ilike '%openupgrade%';


-- ----------------------------------------------------------------------------
-- Always begin with the same company for the admin users
-- ----------------------------------------------------------------------------

update res_users set company_id = (
    select id from res_company where code ='DEL'
)
where id = 1;

update res_partner p set company_id = (
    select id from res_company where code ='DEL'
)
from res_users u
where u.id = 1 and u.partner_id = p.id;
