_INSTALL_MODULES = [
    # OCA / account-financial-tools
    "account_accountant_oca",
    "account_balance_line",
    "account_check_deposit",
    "account_coa_menu",
    "account_group_menu",
    "account_financial_report",
    "account_move_fiscal_year",
    "account_menu",
    "account_netting",
    "account_subsequence_fiscal_year",
    "account_tag_menu",
    "account_type_menu",

    # OCA / account-fiscal-rule
    "account_fiscal_position_usage_group",

    # OCA / account-invoicing
    "account_invoice_fiscal_position_update",
    "account_invoice_merge",
    "account_invoice_search_by_reference",
    "account_invoice_refund_link",
    "account_invoice_supplier_ref_unique",
    "account_invoice_view_payment",

    # OCA / margin-analysis
    "account_invoice_margin",
    "account_invoice_margin_sale",
    "sale_order_margin_percent",

    # OCA / pos
    "pos_place",                    # replace pos_street_market
    "pos_payment_change",           # replace pos_change_payment
    "pos_default_empty_image",      # replace pos_product_without_image
    "pos_order_mgmt",               # replace pos_done_order_load
    "pos_report_session_summary",
    "pos_meal_voucher",
    "pos_disable_change_cashier",
    "pos_picking_delayed",
    "pos_hide_banknote_button",

    # OCA / product-attribute
    "product_pricelist_direct_print",
    "product_restricted_type",

    # OCA / purchase-workflow
    "product_form_purchase_link",
    "purchase_force_invoiced",
    "purchase_propagate_qty",
    # Disabled, ça fait un peu ramer
    # "purchase_order_line_stock_available",
    # Doesn't seem usefull, only based on sale, not PoS...
    # "purchase_order_product_recommendation",
    "purchase_picking_state",
    "purchase_quick",

    # OCA / reporting-engine
    "bi_sql_editor",                # reinstalled, to avoid bugs
    "bi_sql_editor_aggregate",
    "report_wkhtmltopdf_param",

    # OCA / sale-workflow
    # TODO, FIXME this module fail during the instlalation
    # "product_form_sale_link",
    "sale_disable_inventory_check",
    "sale_discount_display_amount",
    "sale_force_invoiced",
    "sale_invoice_group_method",
    "sale_invoice_policy",
    "sale_merge_draft_invoice",
    "sale_order_price_recalculation",

    # OCA / server-auth
    "base_user_show_email",

    # OCA / server-backend
    "base_user_role",
    "server_action_navigate",       # Replace mass_linking
    "server_action_sort",           # replace mass_sorting

    # OCA / server-env
    "server_environment",
    "server_environment_ir_config_parameter",


    # OCA / server-tools
    # TODO, FIXME this module fail during the instlalation
    # "module_analysis",

    # OCA / server-ux
    "base_export_manager",
    "base_technical_features",
    "date_range",

    # OCA / server-brand
    "remove_odoo_enterprise",

    # OCA / social
    "mail_send_copy",               # replace mail_send_bcc

    # OCA / stock-logistics-workflow
    "stock_move_line_auto_fill",
    "stock_picking_back2draft",
    "stock_picking_invoice_link",
    "stock_picking_purchase_order_link",
    "stock_picking_quick",
    "stock_picking_sale_order_link",
    "stock_picking_send_by_mail",
    "stock_picking_show_backorder",
    "stock_picking_show_return",

    # OCA / web
    "web_advanced_search",
    "web_decimal_numpad_dot",
    "web_dashboard_tile",
    "web_dialog_size",
    "web_drop_target",
    # https://github.com/OCA/web/issues/1607
    # "web_edit_user_filter",
    "web_group_by_percentage",
    "web_listview_range_select",
    "web_no_bubble",
    "web_pwa_oca",
    "web_pivot_computed_measure",
    "web_responsive",
    "web_search_with_and",
    "web_searchbar_full_width",
    "web_favorite_company",
    "web_switch_context_warning",
    "web_widget_image_webcam",
    "web_widget_image_download",
    "web_widget_many2many_tags_multi_selection",
    "web_widget_numeric_step",
    "web_tree_resize_column",
    "web_refresher",

    # grap-odoo-incubator
    "mobile_kiosk_purchase",        # replace mobile_app_purchase
    "mobile_kiosk_inventory",        # replace mobile_app_inventory
    "stock_picking_quick_quantity_done",
    "product_default_code_res_company_code",  # code from product_fiscal_company

    # grap-odoo-business
    "account_move_change_number",    # reinstall module that was failing the migration...
    "product_food",
    "product_label",
    "product_origin",
    "product_origin_l10n_fr_department",
    "product_print_category_food_report",
    "recurring_consignment",
    "recurring_consignment_pos",
    "recurring_consignment_purchase",
    "recurring_consignment_sale",
    "stock_preparation_category",
    "technical_partner_access",             # replace users_partners_access

    # TODO, FIXME, this module raise error when installed,
    # because there are duplicates product in database
    # "product_barcode_constraint_per_company",

    # grap-odoo-custom-account
    "grap_custom_account_invoice_workflow",
    "grap_account_export_ebp",              # replace account_export_ebp

    # grap-odoo-custom
    "grap_change_base_product_mass_addition",
    "grap_theme",
    "grap_change_default",
    "grap_change_views_account",
    "grap_change_views_base",
    "grap_change_views_calendar",
    "grap_change_views_mail",
    "grap_change_views_partner",
    "grap_change_views_pos",
    "grap_change_views_product",
    "grap_change_views_purchase",
    "grap_change_views_sale",
    "grap_change_views_stock",

    # odoo-cae / odoo-addons-cae
    "fiscal_company_base",              # replace base_fiscal_company
    "fiscal_company_account",           # replace account_fiscal_company
    "fiscal_company_product",           # replace product_fiscal_company
    "fiscal_company_sales_team",        # replace sales_team_fiscal_company
    "fiscal_company_company_wizard_account",
    "fiscal_company_company_wizard_base",
    "fiscal_company_sale",

    # odoo-cae / odoo-addons-company-wizard (new modules)
    "company_wizard_account",
    "company_wizard_base",
    "company_wizard_product",

    # odoo-cae / odoo-addons-multi-company (new modules)
    "multi_company_account",
    # TODO, fix multi_company_base.
    # 1) code is still present in res_company_code module
    # 2) other modules should not be auto_install to True
    "multi_company_base",
    "multi_company_product",
    "multi_company_sale",
    "multi_company_sales_team",
    "multi_company_barcodes",
]


def main(self, step):
    self._install_modules(_INSTALL_MODULES)
