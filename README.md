# grap-odoo-upgrade

## First time

### Add you as a sudoers that can execute command without
As the script will try to execute sudo with various users (postgres, odooX, ...)
you should add the following line, editing the file ``sudo visudo``

```
$MY_USER ALL=(ALL) NOPASSWD: ALL
```

### Configure postgresql properly

in your postgresql.conf add a line in the end :

``max_locks_per_transaction = 1024``



### Installation

```
# Get Code
git clone https://gitlab.com/grap-rhone-alpes/grap-odoo-upgrade
cd grap-odoo-upgrade

# Install required libraries
./install.sh

# Get environments of each Odoo version
./env/bin/python ./install_or_update_environment.py
```

### Create config file
Create private configuration file based on a template and edit it

```
cp ./config_private.tmpl.py ./config_private.py
```

## Create / Restore Database

### Initialize a demo database

First you can create a database, based on a production configuration, to have
a light database with all the correct modules installed. For that purpose, you can run
the following command.
This command will:
* create a database named ``_DATABASE``
* connect to your production instance defined ``_PRODUCTION_URL``
* recover all the installed modules, and install them on your local database
```
./env/bin/python ./init_database.py
```

### OR Restore Database

Once the script is working with a demo database, you can run the update script
with a restored production database :

**Handle Database**
```
sudo su postgres -c "psql"
create database resto owner odoo8;
date +"%T" && sudo su odoo8 -c "pg_restore -d resto ./grap_production.dump --no-owner" && date +"%T"
```

**Handle Filestore**
```
mkdir filestore
tar xzf grap_production_filestore.tar.gz -C ./filestore
cd filestore/opt/odoo_filestore/filestore
mv grap_production/* /opt/grap_dev/grap-odoo-upgrade/filestore/filestore/migration_current/
cd /opt/grap_dev/grap-odoo-upgrade/filestore/filestore/
sudo chmod -R 777 ./
sudo chown -R odoo8 ./migration_current/

```

## Run migration

```
./env/bin/python ./update.py
```

## General RoadMap / Known Issues


## Specific RoadMap / Known Issues of the migration 8.0 -> 12.0



ALTER DATABASE test_sale OWNER TO odoo9;





def _populate_currency_rate_if_mono_currency(self, cr):
    # count distinct currency id on pos order
    cr.execute("""
        SELECT count(distinct(ppl.currency_id))
        FROM pos_order po
        INNER JOIN product_pricelist ppl
        ON ppl.id = po.pricelist_id;
    """)
    if cr.fetchone() == 1:
        # We are in a monocurrency context,
        # We can set 1.0 on new currency_rate field
        # TODO
        pass

## TODO

### ???
ALTER TABLE "res_partner_bank" ALTER COLUMN "partner_id" SET NOT NULL
ERROR: column "partner_id" contains null values

### ???
ALTER TABLE "product_packaging" ALTER COLUMN "name" SET NOT NULL
ERROR: column "name" contains null values


### ???
ALTER TABLE "stock_picking" ALTER COLUMN "location_dest_id" SET NOT NULL
ERROR: column "location_dest_id" contains null values

### ???
ALTER TABLE "stock_rule" ALTER COLUMN "picking_type_id" SET NOT NULL
ERROR: column "picking_type_id" contains null values

### ???
TODO : write a commit to disable non null values on amount_return,
amount_total, amount_tax, amount_paid

### ???
TODO, écrire un script pour créer des pos.move.reason, basé sur les produits.



# TODO, check au prochain passe
- check fonctionnement pos_multicompany
- sequence de pos.session
- journal de facturation de pos.config
- suppression des ir_act_windows. (check : )

```
select iaw.res_model, count(*)
from ir_act_window iaw
where res_model not in (select model from ir_model)
group by iaw.res_model
order by iaw.res_model;
      res_model      | count
---------------------+-------
 document.page       |     9
 mass.action.wizard  |     2
 mass.linking.wizard |     7
 mass.sort.wizard    |     6
 pos.cash.controls   |     2
 pos.receipt         |     1

 ```



## Merdouille en cours

### 20/05/2020
- web_widget_formula : ça a l'air de puer. TODO désinstaller
- grap_change_data : porté pour désactiver les datas incorrectes. A tester.
- Ces putains d'assets !

- [sale]
- faire un devis chez TOU. la description de TVA en bas indique
 "[old-2013] Base H.T. 7.0%", ce qui est bien, mais pas top.
- faire un devis chez VEV (ou ailleurs). le confirmer. pas de numérotation.

- [base]
- pas de external_report_layout_id sur les res.company

- [intercompany]
- aller dans 3PP, essayer de modifier le external_report_layout_id. ça tente de modifier le res.company, et ça échoue car ça tente de modifier d'autres partner, dans d'autres company. (il manque un sudo())



# Export


- export filestore


cd /opt/grap_dev/grap-odoo-upgrade/filestore/filestore
sudo tar -czf /tmp/backup_V12.tar.gz ./migration_current/


sudo su postgres -c "pg_dump --format=c migration_current --file=/tmp/backup.dump"


sudo su odoo12 -c "pg_restore -d grap_production_v12_2020_06_11 backup_V12.dump --no-owner"
mkdir filestore

tar xzf backup_V12.tar.gz -C ./filestore



sudo chmod 777 grap_production_v12_2020_06_11 -R
sudo chown odoo12 grap_production_v12_2020_06_11 -R
sudo chgrp odoo12 grap_production_v12_2020_06_11 -R











CHECK
je viens de retirer     "account_invoice_pricelist_stock_account",
de la désintall V8 (je pense que ça virait le module account_invoice_pricelist) voir si le module est encore installé en V12.




TODO V16
========

* do not dump all & vaccumm if migration failed.
* make the system more robust with a system of disabling script, if modules are not installed.
for exemple, something like :
``@module_installed('stock', 'purchase')``: check if the module are not uninstalled, to make the script.


* faire un script de comparaison SQL entre une base de production sur une version X
et une version de démo avec la même version X pour voir notamment si :
- des constraintes sont manquantes, ou en trop
- des trucs trainent, différents par rapport à une base classique

* ajouter un système de yeld par exemple :
- for company in self.yeld_company:
bien faire en sorte qu'à la fin du yeld, le script remette le user à la même company.

* mettre un paramètre COMPANY_UPGRADE qui est la company avec lequel le script va toujours s'exécuter.
