import time
from abstract_script import AbstractScript


class GoOdoo(AbstractScript):

    def main(self):
        if len(self.steps_to_execute) != 1:
            self.logger_error(
                "Invalid step argument. please provide a unique step.")
            exit()
        target_step = self.steps_to_execute[0]

        try:
            self._run_odoo(target_step)
            input("Odoo launched. Press Enter to finish...")
        except KeyboardInterrupt:
            time.sleep(0.1)
            self.logger_info("Interrupted by the user. Killing processes")
            time.sleep(0.1)
        finally:
            self._stop_odoo()


if __name__ == "__main__":
    GoOdoo().run()
