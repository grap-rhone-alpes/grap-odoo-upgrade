sudo apt-get install git -y
sudo apt-get install virtualenv -y
sudo apt-get install python-dev -y
sudo apt-get install python3-dev -y

virtualenv env --python=python3
./env/bin/python3 -m pip install -r ./requirements.txt
