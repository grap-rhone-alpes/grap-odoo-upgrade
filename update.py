from abstract_script import AbstractScript


class UpdateScript(AbstractScript):

    def main(self):
        for step in self.steps_to_execute:
            self.logger_title(
                "Executing step '%s'" % step["technical_name"], level=1)

            # Get the state of the modules, before the step
            pre_module_states = self._get_module_states()

            # Change Owner
            self._set_owner_if_required(step)

            if not self.only_update:
                # Run Pre-SQL request
                self._execute_sql_file_pre_migration(step)

            # Update all
            self._update_odoo(step)

            if not self.only_update:
                # Execute Post Python Script
                self._execute_python_files_post_migration(step)

            # Get the state of the modules, at the end of the state
            post_module_states = self._get_module_states()

            # Flush the result
            self.flush_module_states_differences(
                pre_module_states,
                post_module_states
            )

            # Backup Database
            self._backup_database_if_required(step)


if __name__ == "__main__":
    UpdateScript().run()
