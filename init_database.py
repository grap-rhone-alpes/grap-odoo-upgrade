import config
from abstract_script import AbstractScript


class InitDatabase(AbstractScript):

    _require_odoo_external = True

    def main(self):
        if len(self.steps_to_execute) != 1:
            self.logger_error(
                "Invalid step argument. please provide a unique step.")
            exit()
        target_step = self.steps_to_execute[0]

        # create database, if not exists
        self.logger_info("Try to create Local database ...")
        create_request = """
            CREATE DATABASE %s OWNER %s;
        """ % (self.database, target_step["user"])
        self._execute_sql_request(create_request, database="postgres")

        # Get modules list in the external instance
        self.logger_info(
            "Get modules installed on external Odoo instance ...")
        module_names = [
            x['name'] for x in self.odoo_external.IrModuleModule.search_read(
                [('state', '=', 'installed')], ['name'])
        ]
        self.logger_info("--> Found %d modules" % (len(module_names)))

        # Install modules on local database
        self._run_odoo(target_step, init="base")

        self._install_modules(module_names)

        self._stop_odoo()


if __name__ == "__main__":
    InitDatabase().run()
