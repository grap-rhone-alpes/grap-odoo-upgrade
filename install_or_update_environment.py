from pathlib import Path
from plumbum.cmd import mkdir, git
from plumbum import local
import subprocess

from abstract_script import AbstractScript


class InstallEnvironment(AbstractScript):

    def main(self):
        self.logger_title("Create Folders")
        # Create folders
        self.logger_info("Creating 'log' folder ...")
        mkdir(["--mode", "777", "--parents", "./log"])

        self.logger_info("Creating 'src' folder ...")
        mkdir(["--mode", "777", "--parents", "./src"])

        self.logger_info("Creating 'filestore' folder ...")
        mkdir(["--mode", "777", "--parents", "./filestore"])

        self.logger_info(
            "Creating in 'filestore' subdirectories"
            " 'addons' / 'filestore' / 'sessions' ...")
        mkdir(["--mode", "777", "--parents", "./filestore/addons"])
        mkdir(["--mode", "777", "--parents", "./filestore/filestore"])
        mkdir(["--mode", "777", "--parents", "./filestore/sessions"])

        # Clone or update each environment folder
        self.logger_title("Get Last environment")
        for step in self.steps_to_execute:
            if not step.get("do_not_rebuild", False):
                git_file = Path(step["local_path"], ".git")
                if git_file.is_dir():
                    with local.cwd(step["local_path"]):
                        self.logger_info(
                            "Pulling last environment in %s" % step["local_path"])
                        git(["pull", "origin", step["git_branch"]])
                else:
                    self.logger_info(
                        "Clone environement in %s" % step["local_path"])
                    git([
                        "clone",
                        step["git_url"],
                        "--branch",
                        step["git_branch"],
                        step["local_path"]
                    ])

        # Install each environment folder
        self.logger_title("Install each environment")
        for step in self.steps_to_execute:
            if not step.get("do_not_rebuild", False):
                with local.cwd(step["local_path"]):
                    self.logger_info(
                        "Installing environment in %s" % step["local_path"])
                    subprocess.check_output(["./install.sh"], shell=True)


if __name__ == "__main__":
    InstallEnvironment().run()
