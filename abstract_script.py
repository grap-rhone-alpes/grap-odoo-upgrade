import csv
import datetime
import erppeek
import importlib
import logging
import os
import psutil
import re
import socket
import stat
import sys
import subprocess
import tempfile
import time
import traceback
import urllib
import yaml
import psycopg2


import config
import config_private


from cli.app import CommandLineApp


class AbstractScript(CommandLineApp):

    # Set to True in your script, if you require a connexion
    # to an external Odoo Instance
    _require_odoo_external = False

    # ########################################################################
    # Overload Section
    # ########################################################################
    def __init__(self):
        # Call Super of Command Line
        CommandLineApp.__init__(self)

        self.logger_warning_list = []
        self.logger_error_list = []
        self.logger_critical_list = []

        # Initialize other object
        self.odoo = False
        self.admin_user = False
        self.module_list_updated = False
        self.prefetch = {}

        self.now_date = datetime.datetime.now().strftime('%Y-%m-%d')

        # Add parameters
        self.add_param(
            "-ll", "--log-level", default="INFO",
            help="Log level of the file.",
            choices=['DEBUG', 'INFO', 'WARNING', "", "ERROR", "CRITICAL"])
        self.add_param(
            "-d", "--database",
            help="Database. if not set, the default '_DATABASE' key"
            "of the config_private.py file will be used")
        self.add_param(
            "-s", "--steps",
            help="Coma separated steps list to execute."
            " Let empty to execute the script on all steps."
            " IE --steps 4,5,6")
        self.add_param(
            "-bs", "--backup-steps",
            help="Coma separated step list to backup (after)"
            " Let empty wil not backup any step."
            " IE --backup-steps 5,6")
        self.add_param(
            "-od", "--optimize-database", default=False, type=bool,
            help="Perform some postgresql command at the end of the script."
            " IE --optimize-database yes")
        self.add_param(
            "-ou", "--only-update", default=False, type=bool,
            help="Only make an --update all. Do not run pre and post custom"
            " migration script; IE --only-update yes"
            " (Usefull only for the 'update.py' command)")
        self.add_param(
            "-da", "--dump-all", default=False, type=bool,
            help="Dump database and filestore in a export folder."
            " IE --dump-all yes")
        self.add_param(
            "-sl", "--script-list",
            help="Coma separated step list of python script to execute"
            " (Usefull only for the 'execute_script.py' command)")

    def pre_run(self):
        CommandLineApp.pre_run(self)
        self.date_begin = datetime.datetime.now()
        self.script_name_date = "{script_name}__{str_date}".format(
            script_name=str(self.__class__).split(".")[1][:-2],
            str_date=self.date_begin.strftime("%Y_%m_%d__%H_%M_%S")
        )
        self._log_file = "./log/%s.log" % self.script_name_date

        # Initialize Logger
        logging.basicConfig(
            level=logging.DEBUG,
            format='%(asctime)s %(levelname)-8s %(message)s',
            datefmt='%Y-%m-%d %H:%M:%S',
            filename=self._log_file,
            filemode='a'
        )
        console = logging.StreamHandler()
        console.setLevel(getattr(logging, str(self.params.log_level)))
        formatter = logging.Formatter(
            "%(asctime)s %(levelname)-8s %(message)s")
        console.setFormatter(formatter)
        logging.getLogger().addHandler(console)

        self._init_settings()

        self._say_welcome()

        self._init_odoo_external()

    def post_run(self, returned):
        if self.current_odoo_process:
            self._stop_odoo()

        if self.optimize_database:
            self._optimize_database()

        if self.dump_all:
            self._dump_all()

        self.date_end = datetime.datetime.now()

        self._say_goodbye()
        if isinstance(returned, Exception):
            self.logger_error(returned)
        else:
            return CommandLineApp.post_run(self, returned)

    # ########################################################################
    # Custom Init Settings
    # ########################################################################
    def _init_settings(self):
        # Get database
        self.database = self.params.database or config_private._DATABASE

        # Initialize Odoo process
        self.current_odoo_process = False

        # Keep current directory
        self._current_directory = os.getcwd()

        # Get steps
        if not self.params.steps:
            self.steps_to_execute = config._ALL_STEPS
        else:
            self.steps_to_execute = []
            for step in config._ALL_STEPS:
                for step_number in self.params.steps.split(","):
                    if step["technical_name"].startswith(
                            "step_%s" % (step_number)):
                        self.steps_to_execute.append(step)
                        continue

        # Get Backup steps
        self.steps_to_backup = []
        if self.params.backup_steps:
            for step in config._ALL_STEPS:
                for step_number in self.params.backup_steps.split(","):
                    if step["technical_name"].startswith(
                            "step_%s" % (step_number)):
                        self.steps_to_backup.append(step)
                        continue

        self.optimize_database = self.params.optimize_database
        self.dump_all = self.params.dump_all
        self.only_update = self.params.only_update

        # Get script list to execute
        self.script_list = (
            self.params.script_list
            and self.params.script_list.split(",")
            or [])

        self.modules_to_check =\
            config_private._MODULES_TO_PREVENT_UNINSTALLATION

    def _init_odoo_external(self):
        if not self._require_odoo_external:
            return

        database = config_private._ODOO_EXTERNAL_DATABASE
        login = config_private._ODOO_EXTERNAL_LOGIN
        password = config_private._ODOO_EXTERNAL_PASSWORD

        # Connection to the server
        try:
            self.logger_info("Connecting on %s ..." % (
                config_private._ODOO_EXTERNAL_URL))
            self.odoo_external = erppeek.Client(
                config_private._ODOO_EXTERNAL_URL)
        except socket.gaierror:
            self.logger_critical("Incorrect url '%s'" % (
                config_private._ODOO_EXTERNAL_URL))
            exit()
        except socket.error:
            self.logger_critical(
                "Unable to connect to %s" % (
                    config_private._ODOO_EXTERNAL_URL))
            exit()

        # Authentication
        try:
            user_id =\
                self.odoo_external.login(
                    login, password=password, database=database)
            if user_id == 1:
                self.logger_warning(
                    "Warning, you are logged with admin user. ACL will"
                    " not be checked.")
            self.odoo_user_external = self.odoo_external.ResUsers.browse(
                user_id)
        except erppeek.Error:
            self.logger_critical(
                "Incorrect database or wrong credentials")
            exit()

    def _say_welcome(self):
        self.logger_title(
            "Begin of the script %s" % self.__class__, level=0)
        self.logger_info(
            "> script will be executed for the following steps: %s" % (
                ", ".join(x["technical_name"] for x in self.steps_to_execute)))
        if self.steps_to_backup:
            self.logger_info(
                "> backup will be done after the following steps: %s" % (
                    ", ".join(
                        x["technical_name"] for x in self.steps_to_backup)))
        self.logger_info(
            "> log will be written in the following file : %s" % (
                self._log_file))

    def _say_goodbye(self):
        self.logger_title(
            "End of the script %s" % self.__class__, level=0)
        duration = self.date_end - self.date_begin
        hours, remainder = divmod(duration.total_seconds(), 3600)
        minutes, seconds = divmod(remainder, 60)

        self.logger_info(
            "Total duration of the script:"
            " {:.0f} hours / {:.0f} minutes / {:f} seconds.".format(
                hours, minutes, seconds))
        self.flush_log()

# ############################################################################
# Basic
# ############################################################################
    def _run_command_popen(self, command, shell=True, sleep=0.0):
        self.logger_debug("[Popen] command %s ..." % (command))
        result = subprocess.Popen(command, shell=True)
        time.sleep(sleep)
        return result

    def _run_command_call(self, command, shell=True):
        self.logger_debug("[call] command %s ..." % (command))
        subprocess.call(command, shell=True)

# ############################################################################
# Filestore / OS Parts
# ############################################################################

    def _create_or_change_filestore_owner(self, step, database=False):
        if not database:
            database = self.database
        current_filestore_folder =\
            config_private._FILESTORE_FOLDER + 'filestore/' + database
        if not os.path.isdir(current_filestore_folder):
            # create database folder
            command = "mkdir %s --mode 777" % (current_filestore_folder)
            self._run_command_popen(command, shell=True, sleep=.2)

        # Change owner of database
        command = "sudo chown -R %s %s" % (
            step['user'], current_filestore_folder)
        self._run_command_popen(command, shell=True, sleep=.2)

    def _generator_from_csv(
            self, step, file_name, mapping, begin_line,
            max_qty=False, delimiter=',', quotechar='"'):

        class Object(object):
            pass

        path = os.path.join(
            "script",
            step["technical_name"],
            "data",
            file_name,
            )
        if not os.path.exists(path):
            self.logger_error("File %s not found." % path)
            raise Exception

        csvfile = open(path, 'r', encoding="utf-8")
        spamreader = csv.reader(
            csvfile, delimiter=delimiter, quotechar=quotechar)
        count = 0
        for row in spamreader:
            count += 1
            if count >= begin_line and\
                    (not max_qty or count - begin_line < max_qty):

                myObject = Object()
                setattr(
                    myObject, '_row_number_text',
                    '%s. ' % str(count).zfill(4))

                self.logger_debug(
                    "%sAnalyzing line ... data %s" % (
                        myObject._row_number_text, str(row)))

                for i in range(len(mapping)):
                    field = mapping[i]
                    box_value = row[i]
                    if field['type'] == 'char':
                        value = box_value
                    elif field['type'] == 'boolean':
                        value = bool(box_value)

                    setattr(myObject, field['name'], value)

                yield myObject

    # ########################################################################
    # Logger Section
    # ########################################################################

    def logger_title(self, title, level=2):
        if level == 0:
            self.logger_info("=" * 100)
            self.logger_info(title)
            self.logger_info("=" * 100)
        elif level == 1:
            self.logger_info("*" * 80)
            self.logger_info(title)
            self.logger_info("*" * 80)
        else:
            self.logger_info("-" * 60)
            self.logger_info(title)
            self.logger_info("-" * 60)

    def logger_debug(self, message):
        logging.debug(message)

    def logger_info(self, message):
        logging.info(message)

    def logger_warning(self, message):
        logging.warning(message)
        self.logger_warning_list.append(message)

    def logger_error(self, message):
        logging.error(message)
        self.logger_error_list.append(message)

    def logger_critical(self, message):
        logging.critical(message)
        self.logger_critical_list.append(message)

    def flush_log(self):
        if self.logger_warning_list:
            self.logger_info("==========================")
            self.logger_info("============== WARNING ===")
            self.logger_info("==========================")
            for message in self.logger_warning_list:
                logging.warning(message)

        if self.logger_error_list:
            self.logger_info("==========================")
            self.logger_info("============== ERROR =====")
            self.logger_info("==========================")
            for message in self.logger_error_list:
                logging.error(message)

        if self.logger_critical_list:
            self.logger_info("==========================")
            self.logger_info("============== CRITICAL ==")
            self.logger_info("==========================")
            for message in self.logger_critical_list:
                logging.critical(message)

    # ########################################################################
    # SQL / Postgresql Parts
    # ########################################################################

    def _execute_sql_request_by_psycopg2(self, sql_request):
        """To work, the user should have an account on postgres
        for exemple and access to the database : create user xxx SUPERUSER;"""

        connexion = psycopg2.connect(dbname=self.database)
        cursor = connexion.cursor()
        cursor.execute(sql_request)
        return cursor.fetchall()

    def _execute_sql_request(
            self, sql_request, output_file=False, database=False):
        sql_file = tempfile.NamedTemporaryFile()
        sql_file.seek(0)
        sql_file.write(sql_request.encode('utf-8'))
        sql_file.flush()
        os.chmod(sql_file.name, stat.S_IRWXO)
        self.logger_debug("Executing SQL Request '%s'" % (sql_request))
        self._execute_sql_file(
            sql_file.name,
            database=database,
            output_file=output_file,
            log=False)
        sql_file.close()

    def _execute_sql_file(
            self, sql_file, database=False, log=True,
            output_file=False):
        if not database:
            database = self.database
        output_file_option = ""
        if output_file:
            output_file_option = "--output %s" % output_file
            output_file_option += " --pset='tuples_only'"
        else:
            output_file_option = ""
        command = (
            "sudo su postgres -c \""
            "psql"
            " --dbname {database}"
            " --file {sql_file}"
            " {output_file_option}"
            "\"  >> {log_file} 2>&1"
           ).format(
            database=database, sql_file=sql_file, log_file=self._log_file,
            output_file_option=output_file_option)
        if log:
            self.logger_info(
                "Executing SQL File '%s'" % (sql_file))
        self._run_command_call(command)

    def _reassign_database(self, step, database=False):
        if not database:
            database = self.database
        sql_request = "ALTER DATABASE %s OWNER TO %s;" % (
            database, step["user"])
        self._execute_sql_request(sql_request, database=database)
        self._execute_sql_file(
            "./script/change_owner_to_%s.sql" % (step["user"]),
            database=database,
        )

    def _backup_database(self, step, database=False):
        self.logger_info("Waiting 1 minute before backuping")
        time.sleep(60)
        if not database:
            database = self.database
        new_db_name = "%s__backup_after__%s" % (
            database, step["technical_name"])
        sql_request = "CREATE DATABASE %s WITH TEMPLATE %s OWNER %s;" % (
            new_db_name, database, step["user"])
        self._execute_sql_request(sql_request, database=database)

    def _dump_all(self):
        backup_name = "database_%s" % (
            self.date_begin.strftime("%Y_%m_%d__%H_%M_%S")
        )
        # Create main dump all folder
        dump_folder = os.path.join(
            config_private._DUMP_FOLDER,
            self.date_begin.strftime("%Y_%m_%d__%H_%M_%S")
        )
        command = "mkdir %s --mode 777" % (dump_folder)
        self._run_command_popen(command, shell=True, sleep=.2)
        self.logger_info(
            "Dumping all the data into the folder '%s'" % dump_folder)

        # Dump Database
        database_file_path = os.path.join(
            dump_folder,
            "%s.dump" % (backup_name)
        )
        db_command = (
            "sudo su postgres -c \""
            "pg_dump {database}"
            " -f {database_file_path} --format=c"
            "\""
        ).format(
                database=self.database, database_file_path=database_file_path)

        self._run_command_call(db_command)

        # Dump filestore
        current_fs_folder = os.path.join(
            config_private._FILESTORE_FOLDER,
            "filestore",
            self.database,
        )
        dump_fs_path = os.path.join(
            dump_folder,
            "%s.tar.gz" % backup_name
        )
        fs_command = (
            "cd {current_fs_folder}"
            " && sudo tar czf {dump_fs_path} ./"
        ).format(
            current_fs_folder=current_fs_folder,
            dump_fs_path=dump_fs_path,
        )
        self._run_command_call(fs_command)

    # ########################################################################
    # Odoo Process & erppeek Parts (Generic Process)
    # ########################################################################

    def _generate_addons_path(self, step):
        folder = os.path.join(self._current_directory, step["local_path"])
        stream = open(os.path.join(folder, "repos.yml"), 'r')
        data = yaml.safe_load(stream)

        base_dir = {
            "8.0": "openerp",
            "9.0": "openerp",
            "10.0": "odoo",
            "11.0": "odoo",
            "12.0": "odoo",
        }[step["odoo_version"]]

        addons_path = []
        for key in data.keys():
            path = os.path.join(folder, key)
            if path.split('/')[-1] == "odoo":
                if step["role"] == "update":
                    # Add two addons path
                    addons_path.append(os.path.join(path, "addons"))
                    addons_path.append(os.path.join(path, base_dir, "addons"))
            elif path.split('/')[-1] == "openupgrade":
                if step["role"] == "upgrade":
                    # Add two addons path
                    addons_path.append(os.path.join(path, "addons"))
                    addons_path.append(os.path.join(path, base_dir, "addons"))
            else:
                addons_path.append(path)

        return ",".join(addons_path)

    def _generate_run_command(self, step):
        folder = os.path.join(self._current_directory, step["local_path"])
        python_env = {
            "8.0": "python2",
            "9.0": "python2",
            "10.0": "python2",
            "11.0": "python3",
            "12.0": "python3",

        }[step["odoo_version"]]
        odoo_folder = {
            "update": "./src/odoo",
            "upgrade": "./src/openupgrade",
        }[step["role"]]
        odoo_run = {
            "8.0": "odoo.py",
            "9.0": "odoo.py",
            "10.0": "odoo-bin",
            "11.0": "odoo-bin",
            "12.0": "odoo-bin",
        }[step["odoo_version"]]
        return "%s %s" % (
            os.path.join(folder, "env/bin", python_env),
            os.path.join(folder, odoo_folder, odoo_run),
        )

    def _generate_odoo_url(self):
        return "http://localhost:%s/" % config_private._XMLRPC_PORT

    def _generate_odoo_command(
            self, step, database=False, stop_after_init=False,
            update="", init="", no_workers=True):

        if not database:
            database = self.database

        total_memory = psutil.virtual_memory().total

        config_file = os.path.join(
            self._current_directory,
            "script",
            step["technical_name"],
            "odoo.cfg")
        if not os.path.isfile(config_file):
            config_file = ""
        conf = {
            "run_command": self._generate_run_command(step),
            "database": database,
            "db_user": step["user"],
            "db_password": config_private._DB_PASSWORD,
            "log_level": "info",
            "log_file": self._get_log_file(step),
            "data_dir": config_private._FILESTORE_FOLDER,
            "workers": no_workers and "0" or "8",
            "memory_soft": int(total_memory / 3),
            "memory_hard": int(total_memory / 2),
            "xmlrpc_port": config_private._XMLRPC_PORT,
            "longpolling_port": config_private._LONGPOLLING_PORT,
            "opt_stop_after_init":
            stop_after_init and "--stop-after-init" or "",
            "opt_update_list": update and "--update %s" % update or "",
            "opt_init_list": init and "--init %s" % init or "",
            "opt_config_file":
            config_file and "--config %s" % config_file or "",
            "addons_path": self._generate_addons_path(step),
        }

        odoo_command =\
            " {run_command}"\
            " --database {database} --data-dir {data_dir}"\
            " --db_user {db_user} --db_password {db_password}"\
            " --log-level {log_level} --logfile {log_file}"\
            " --xmlrpc-port {xmlrpc_port}"\
            " --longpolling-port {longpolling_port}"\
            " --workers {workers}"\
            " --limit-memory-soft {memory_soft}"\
            " --limit-memory-hard {memory_hard}"\
            " --limit-time-cpu 3600 --limit-time-real 3600"\
            " {opt_stop_after_init}"\
            " {opt_update_list}"\
            " {opt_init_list}"\
            " {opt_config_file}"\
            " --addons-path {addons_path}".format(**conf)

        return "sudo su {odoo_user} -c '{odoo_command}'".format(
            odoo_user=step["user"],
            odoo_command=odoo_command,
        )

    def _update_odoo(self, step, database=False):
        if not database:
            database = self.database
        initial_count = self._count_loaded_ok_in_log(step)
        command = self._generate_odoo_command(
            step, database=database, stop_after_init=True, update="all")

        self.logger_info(
            "Updating all, waiting for the end of the communication ...")
        current_process = self._run_command_popen(
            command, shell=True, sleep=1.0)

        current_process.communicate()
        self.logger_info("Updated.")

        final_count = self._count_loaded_ok_in_log(step)
        if final_count != initial_count + 1:
            self.logger_error("The Update all process failed. Check Logs.")
            raise Exception

    def _run_odoo(self, step, database=False, no_workers=True, init=False):
        if not database:
            database = self.database

        command = self._generate_odoo_command(
            step, database=database, no_workers=no_workers, init=init)

        self.logger_info("Running odoo to execute script ...")
        self.current_odoo_process = self._run_command_popen(
            command, shell=True, sleep=1.0)
        i = 0
        ok = False
        while not ok and i < config._MAX_WAIT_FOR_ODOO:
            i += 1
            url = self._generate_odoo_url()
            self.logger_info("%d / %d : Wait for Odoo on %s" % (
                i, config._MAX_WAIT_FOR_ODOO, url))
            try:
                urllib.request.urlopen(url)
                ok = True
            except urllib.error.URLError:
                time.sleep(1.0)
        if not ok:
            self.logger_error("Unable to connect to %s after %d seconds" % (
                url, config._MAX_WAIT_FOR_ODOO))
        time.sleep(5.0)
        try:
            self.odoo_local = erppeek.Client(url)
            user_id = self.odoo_local.login(
                config_private._LOGIN,
                password=config_private._PASSWORD,
                database=database)
            self.odoo_user = self.odoo_local.ResUsers.browse(user_id)
        except Exception as e:
            self.logger_error(
                "Unable to connect to %s with login %s and password %s" % (
                    url, config_private._LOGIN, config_private._PASSWORD))
            raise e

    def _stop_odoo(self):
        self.odoo_local = False
        process = psutil.Process(self.current_odoo_process.pid)
        self.logger_info(
            "Stopping Odoo. Process %d (and children)" % process.pid)
        self._kill_process(process)
        time.sleep(5.0)

    def _kill_process(self, process):
        for child in process.children():
            self._kill_process(child)
        self.logger_info("Killing process %d..." % process.pid)
        command = "sudo kill -9 %d" % process.pid
        self._run_command_popen(command, shell=True, sleep=0.5)

    # ########################################################################
    # Odoo Process & erppeek Parts (Business Process)
    # ########################################################################

    def _get_ref(self, xml_id):
        module, xml_name = xml_id.split('.')
        res = self.odoo_local.IrModelData.search_read(
            [('module', '=', module), ('name', '=', xml_name)],
            ["res_id", "model"])
        if len(res) != 1:
            self.logger_error("Unable to find the xml_id '%s'" % xml_id)
        res = res[0]
        return self.odoo_local.model(res['model']).browse([res['res_id']])[0]

    def browse_by_search(self, model, domain=[]):
        return model.browse(model.search(domain))

    def _restrict_group_to_users(self, xml_groups, emails=[], logins=[]):
        # Due to current design of the script,
        # we can not login in with __system__ user (with ACL disabled)
        # so the script will switch for all the commpanies
        company_ids = self.odoo_local.ResCompany.search([
            '|', ('active', '=', True), ('active', '=', False)])

        for company_id in company_ids:
            self.odoo_user.write({'company_id': company_id})
            if emails:
                domain = [("email", "in", emails)]
            elif logins:
                domain = [("login", "in", logins)]
            else:
                domain = [('id', 'in', self.odoo_local.ResUsers.search([]))]

            with_peoples = self.odoo_local.ResUsers.browse(domain)
            without_peoples = self.odoo_local.ResUsers.browse(
                [('id', 'not in', [x.id for x in with_peoples])])
            for xml_id in xml_groups:
                group = self._get_ref(xml_id)
                group_name = "%s / %s " % (
                    group.category_id and group.category_id.name or "ROOT",
                    group.name
                )
                current_people_ids = [x.id for x in group.users]
                to_add_peoples = []
                for user in with_peoples:
                    if user.id not in current_people_ids:
                        to_add_peoples.append(user)

                too_remove_peoples = []
                for user in without_peoples:
                    if user.id in current_people_ids:
                        too_remove_peoples.append(user)

                if to_add_peoples:
                    self.logger_info(
                        "[ADD] from group '%s':\n %s" % (
                            group_name, ", ".join(
                                [x.name for x in to_add_peoples])))
                    group.write({
                        "users": [(4, x.id) for x in to_add_peoples]
                        })

                if too_remove_peoples:
                    self.logger_info(
                        "[REMOVE] from group '%s':\n  %s" % (
                            group_name, ", ".join(
                                [x.name for x in too_remove_peoples])))
                    value = [(3, x.id) for x in too_remove_peoples]
                    group.write({
                        "users": value,
                        })

    def _backup_database_if_required(self, step):
        if step['technical_name'] in [
                x['technical_name'] for x in self.steps_to_backup]:
            self._backup_database(step)

    def _optimize_database(self):
        self.logger_info("Waiting 1 minute before optimizing...")
        # time.sleep(60)
        self.logger_info("VACUUM FULL ANALYZE: Running...")
        self._execute_sql_request("VACUUM FULL ANALYZE;")
        self.logger_info("VACUUM FULL ANALYZE: Done.")
        self.logger_info("REINDEX DATABASE: Running...")
        self._execute_sql_request("REINDEX DATABASE %s;" % (self.database))
        self.logger_info("REINDEX DATABASE: Done.")

    def _set_owner_if_required(self, step):
        if step.get('old_user', False):
            # Change owner of the database
            self._reassign_database(step)

            # Change owner of the file store
            self._create_or_change_filestore_owner(step)

    def _is_model_present(self, model_name):
        if self.odoo_local.IrModel.search([("model", "=", model_name)]):
            return True
        else:
            self.logger_warning(
                "Model '%s' not found."
                " Part of the script will be skipped." % (model_name))
            return False

    def _install_modules(self, module_names):
        # Install Modules
        i = 0
        for module_name in module_names:
            i += 1
            prefix = str(i) + "/" + str(len(module_names))
            modules = self.odoo_local.IrModuleModule.browse(
                [('name', '=', module_name)])
            if not len(modules):
                self.logger_warning(
                    "%s - Module '%s': Not found." % (prefix, module_name))
                continue

            module = modules[0]
            if module.state == 'installed':
                self.logger_info(
                    "%s - Module %s still installed."
                    " skipped." % (prefix, module_name))
            elif module.state == "uninstalled":
                try_qty = 0
                installed = False
                while installed is False:
                    try_qty += 1
                    self.logger_info(
                        "%s - Module '%s': Installing ... %s" % (
                            prefix, module_name,
                            "(try #%d)" % try_qty if try_qty != 1 else ""))
                    try:
                        module.button_immediate_install()
                        installed = True
                        time.sleep(5)
                    except Exception as e:
                        if try_qty <= config._MAX_TRY_QTY:
                            sleeping_time = 2 * try_qty * 60
                            self.logger_warning(
                                "Error. Retrying in %d seconds.\n %s" % (
                                    sleeping_time, e))
                            time.sleep(sleeping_time)
                        else:
                            self.logger_error(
                                "Error after %d try. Exiting.\n %s" % (
                                    try_qty, e))
                            raise e

            else:
                self.logger_warning(
                    "%s - Module '%s': In the %s state."
                    " (Unable to install)" % (
                        prefix, module_name, module.state))

    def _uninstall_modules(
            self, module_names, pre_sql_request=False
    ):
        i = 0
        for module_name in module_names:
            if pre_sql_request:
                self._execute_sql_request(pre_sql_request)

            i += 1
            prefix = str(i) + "/" + str(len(module_names))
            modules = self.odoo_local.IrModuleModule.browse(
                [('name', '=', module_name)])
            if not len(modules):
                self.logger_warning(
                    "%s - Module '%s': Not found." % (prefix, module_name))
                continue
            module = modules[0]
            if module.state in (
                    "installed", 'to upgrade', "to update", "to remove"):
                self.logger_info(
                    "%s - Module %s Uninstalling .." % (
                        prefix, module_name))
                module.button_upgrade_cancel()
                module.button_uninstall()
                wizard = self.odoo_local.BaseModuleUpgrade.create({})
                wizard.upgrade_module()

            else:
                self.logger_warning(
                    "%s - Module '%s': In the %s state."
                    " (Unable to uninstall)" % (
                        prefix, module_name, module.state))
            self._check_modules_uninstallation()

    def _execute_sql_file_pre_migration(self, step):
        pre_sql_file = "./script/%s/pre-migration.sql" % (
            step["technical_name"])

        if os.path.isfile(pre_sql_file):
            self._execute_sql_file(pre_sql_file)

    def _execute_python_files_post_migration(self, step):
        step_folder = os.path.join(
            self._current_directory, "script", step["technical_name"]
        )
        from os import listdir
        from os.path import isfile, join
        python_files = [
            f for f in listdir(step_folder)
            if isfile(join(step_folder, f))
            and f[-3:] == ".py"
        ]
        python_files = sorted(python_files)

        self._run_odoo(step)
        try:
            for python_file in python_files:
                script_name = python_file[:-3]
                if self.script_list:
                    if script_name not in self.script_list:
                        self.logger_info(
                            "Skipping script '%s' (not in list)" % (
                                script_name))
                        continue
                self.logger_info(
                    "Running Script Post (Python) %s" % python_file)

                package_name = "script.%s.%s" % (
                    step["technical_name"], script_name
                )
                package = importlib.import_module(package_name)
                main_function = getattr(package, "main")
                main_function(self, step=step)
            self._check_modules_uninstallation()
        except Exception as e:
            self.logger_error("An error occured. Exiting. %s\n%s" % (
                e, traceback.print_exception(*sys.exc_info())))
            raise e
        finally:
            self._stop_odoo()

    def _check_modules_uninstallation(self):
        for module_name_to_check in self.modules_to_check:
            module_to_check = self.odoo_local.IrModuleModule.browse(
                [('name', '=', module_name_to_check)])[0]
            if module_to_check.state not in ('installed', 'to upgrade'):
                self.logger_error(
                    "Module '%s' seems to have uninstalled module %s" % (
                        module_name, module_name_to_check))
                self.modules_to_check.remove(module_name_to_check)

    def _get_module_states(self):
        query = "SELECT name, state FROM ir_module_module;"
        temporary_file_path = "/tmp/migration_request.csv"
        self._execute_sql_request(
            query, output_file=temporary_file_path)
        result = {}
        with open(temporary_file_path, mode='r') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter='|')
            for row in csv_reader:
                # We skip empty lines
                if not len(row):
                    continue
                result[row[0].strip()] = row[1].strip()
        csv_file.close()
        # Remove file using sudo, because it belong to postgres
        self._run_command_call("sudo rm /tmp/migration_request.csv")
        return result

    def flush_module_states_differences(
        self, pre_module_states, post_module_states
    ):
        result = {}
        _module_recorded = []
        for pre_module, pre_state in pre_module_states.items():
            _module_recorded.append(pre_module)
            post_state_if_any = post_module_states.get(pre_module, False)
            key = (pre_state, post_state_if_any)
            if key in result:
                result[key].append(pre_module)
            else:
                result[key] = [pre_module]

        for post_module, post_state in post_module_states.items():
            if post_module in _module_recorded:
                continue
            key = (False, post_state)
            if key in result:
                result[key].append(post_module)
            else:
                result[key] = [post_module]

        for key, modules in result.items():
            # we check if there are differencies, skipping
            # new module key[0] = False or old module key[1] = False
            if key[0] != key[1] and key[0] and key[1]:
                self.logger_title(
                    "Moves from %s to %s :\n %s" % (
                        key[0], key[1], sorted(modules)),
                    level=2)

# ############################################################################
# Logging Parts
# ############################################################################

    def _get_log_file(self, step):
        return os.path.join(
            config_private._LOG_FOLDER,
            "{prefix}__{technical_name}.log".format(
                prefix=self.script_name_date,
                technical_name=step['technical_name'],
            )
        )

    def _count_loaded_ok_in_log(self, step):
        pattern = r"modules\.loading: Modules loaded"

        log_file = self._get_log_file(step)
        if not os.path.isfile(log_file):
            return 0

        textfile = open(log_file, 'r')
        filetext = textfile.read()
        textfile.close()
        return len(re.findall(pattern, filetext))
